const app = require('./app');
const config = require('./config');

if (!module.parent) {
    configure(app);
    app.listen( config.port, function() {
        console.log(
            'Server is running, listening on %s:%s, environment: %s',
            'http://localhost',
            config.port,
            process.env.ENV
        );
    });
}

async function configure(app){
    if(process.env.ENV && process.env.ENV.includes('test')){
        console.log('************** WARNING: ADDED DELETE METHODS **************')
        let models = await require('./models');
        const AppResponse = require('./helpers/AppResponse');

        app.get('/users', async (req, res)=>{
            let users = await models.User.findAllPromise();
            new AppResponse('Success', users, 200).sendResponse(res);
        });

        app.delete('/users', async (req, res)=>{
            let users = await models.User.removePromise();
            new AppResponse('Success', null, 200).sendResponse(res);
        });
    }
}