const config = require('../config');
const secretKey = config.stripe.secretKey;
let stripe = require("stripe")(secretKey);

var service = {
    changeSubscriptionPromise: function(customerId, subscriptionId, packageId, prorationStartTimeInSeconds){
        return new Promise((fulfill, reject)=>{
            stripe.subscriptions.retrieve(subscriptionId).then(subscription => {
                var item_id = subscription.items.data[0].id;
                stripe.subscriptions.update(subscriptionId, {
                    items: [{
                        id: item_id,
                        plan: packageId,
                    }],
                    proration_date: prorationStartTimeInSeconds,
                }, function(err, rawSubscriptionData) {
                    if(err){
                        return reject(err);
                    }else{
                        fulfill(rawSubscriptionData);
                    }
                });
            });
        });
    },
    /**
     *
     * @param customerId - is the stripe customer id
     * @param subscriptionId - is the subscription id from stripe
     * @param packageId - the package id we want to switch to
     * @param prorationStartTimeInSeconds
     * @returns {Promise<unknown>}
     */
    getProrationAmountPromise: async function(customerId, subscriptionId, packageId, prorationStartTimeInSeconds){
        let subscription = await stripe.subscriptions.retrieve(subscriptionId);
        var item_id = subscription.items.data[0].id;
        var items = [{
            id: item_id,
            plan: packageId,
        }];

        let args = {
            customer: customerId,
            subscription: subscriptionId,
            subscription_items: items, // Switch to new plan
            subscription_proration_date: prorationStartTimeInSeconds
        };

        let invoice = await stripe.invoices.retrieveUpcoming(args);
        let nextChargeDate = invoice.next_payment_attempt;
        var cost = 0;
        for (var i = 0; i < invoice.lines.data.length; i++) {
            var invoice_item = invoice.lines.data[i];
            if (invoice_item.period.start - prorationStartTimeInSeconds <= 1) {
                cost += invoice_item.amount;
            }
        }
        return {cost, nextChargeDate};
    },
    getAllCustomerCardsPromise: function(customerId){
        return new Promise((fulfill, reject)=>{
            stripe.customers.listCards(customerId, function(err, cards) {
                if(err){
                    return reject(err);
                }
                fulfill(cards.data);
            });
        });
    },
    createCustomerWithPaymentMethodPromise: function(user, paymentMethod){
        return new Promise((fulfill, reject)=>{
            stripe.customers.create({
                email: user.email,
                source: paymentMethod.tokenId,
                metadata: {userId: user.getIdString()}
            }, function(err, customer) {
                if(!err){
                    fulfill(customer);
                }else{
                    reject(err);
                }
            });
        });
    },
    /**
     * Deletes the user from stripe and cancels any subscriptions they may have.
     * @param customerId
     * @returns {Promise<unknown>}
     */
    deleteCustomerPromise: function(customerId){
        return new Promise((fulfill, reject)=>{
            stripe.customers.del(customerId, function(err, confirmation) {
                if(!err){
                    fulfill(confirmation);
                }else{
                    reject(err);
                }
            });
        });
    },
    deleteCardPromise: function(customerId, cardId){
        return new Promise((fulfill, reject)=>{
            stripe.customers.del(customerId, cardId, function(err, confirmation) {
                if(!err){
                    fulfill(confirmation);
                }else{
                    reject(err);
                }
            });
        });
    },
    /**
     * endOfTrialTime [seconds || 'now']
     */
    createAndStartSubscriptionForUserPromise: function(user, pm, endOfTrialTime, desiredPackageId){
        var trialEnd = endOfTrialTime;
        if(typeof trialEnd == 'number'){
            trialEnd = trialEnd/1000
        }
        return new Promise((fulfill, reject)=>{
            stripe.subscriptions.create({
                customer: user.paymentCustomerId,
                items: [{plan: desiredPackageId}],
                trial_end: trialEnd,
                // source: pm.tokenId // not adding source b/c the customer already has a CC associated to it.
            }, function(err, subscription) {
                if(!err){
                    fulfill(subscription);
                }else{
                    reject(err);
                }
            });
        })
    },

    cancelSubscriptionPromise: function(subscriptionId){
        new Promise((fulfill, reject)=>{
            stripe.subscriptions.del(
                subscriptionId,
                function(err, cancelledSubscription) {
                    if(err) return reject(err);
                    // the cancelledSubscription is the subscription object but w/ the status=canceled.
                    fulfill(cancelledSubscription);
                }
            );
        });
    },

    addCardToCustomerPromise: function(paymentCustomerId, paymentTokenId){
        return new Promise((fulfill, reject)=>{
            stripe.customers.createSource(
                paymentCustomerId,
                {source: paymentTokenId},
                function(err, card) {
                    if(err) return reject(err);
                    fulfill(card);
                }
            );
        });
    },
    /**
     *
     * @param customerId
     * @param cardId
     * @returns {Promise<unknown>}
     * {
        "id": "card_1GNrMv2eZvKYlo2CPIssrNn1",
        "object": "card",
        "deleted": true
        }
     */
    deleteCustomerCardPromise: function(customerId, cardId){
        return new Promise((fulfill, reject)=>{
            stripe.customers.deleteCard(
                customerId,
                cardId,
                function(err, confirmation) {
                    if(err) return reject(err);
                    if(!confirmation.deleted){
                        return reject('Error deleting card')
                    }
                    fulfill();
                }
            );
        });
    },

    updateDefaultPaymentPromise: function(paymentCustomerId, paymentCardId){
        return new Promise((fulfill, reject)=>{
            stripe.customers.update(
                paymentCustomerId,
                {default_source: paymentCardId},
                function(err, customer) {
                    if(err) return reject(err);
                    fulfill(customer);
                }
            );
        });
    },
    /**
     * THis was added just for integration testing purposes.
     * @param mockStripe
     * @private
     */
    _setStripe: function(mockStripe){
        stripe = mockStripe;
    }
};

module.exports = service;