var ms = require('mustache');

var resetPasswordTemplateHTML = require('../resources/email/resetPasswordTemplate.html');
var resetPasswordTemplateText = require('../resources/email/resetPasswordTemplate.txt');

var welcomeAndConfirmHTML = require('../resources/email/welcomeAndConfirmTemplate.html');
var welcomeAndConfirmText = require('../resources/email/welcomeAndConfirmTemplate.txt');

var subscriptionStartedHTML = require('../resources/email/subscriptionStarted.html');
var subscriptionStartedText = require('../resources/email/subscriptionStarted.txt');

var subscriptionCancelledHTML = require('../resources/email/subscriptionCancelled.html');
var subscriptionCancelledText = require('../resources/email/subscriptionCancelled.txt');

var subscriptionResumedHTML = require('../resources/email/subscriptionResumed.html');
var subscriptionResumedText = require('../resources/email/subscriptionResumed.txt');

var subscriptionSwitchedHTML = require('../resources/email/subscriptionSwitched.html');
var subscriptionSwitchedText = require('../resources/email/subscriptionSwitched.txt');

var service = {
    getSubscriptionResumedData: function(numConnections, price){
        var html = ms.render(subscriptionResumedHTML, {numConnections, price});
        var txt = ms.render(subscriptionResumedText, {numConnections, price});
        var subject = "Subscription Resumed";
        return {html: html, text: txt, subject: subject}
    },

    getSubscriptionSwitchedData: function(oldNumConnections, newNumConnections, newPrice){
        var html = ms.render(subscriptionSwitchedHTML, {oldNumConnections, newNumConnections, newPrice});
        var txt = ms.render(subscriptionSwitchedText, {oldNumConnections, newNumConnections, newPrice});
        var subject = "Subscription Updated";
        return {html: html, text: txt, subject: subject}
    },

    getResetPasswordData: function(tempPass, resetLink){
        var html = ms.render(resetPasswordTemplateHTML, {tempPass: tempPass, resetLink: resetLink});
        var txt = ms.render(resetPasswordTemplateText, {tempPass: tempPass, resetLink: resetLink});
        var subject = "Password Reset";
        return {html: html, text: txt, subject: subject}
    },

    getWelcomeAndConfirmData: function(confirmLink){
        var html = ms.render(welcomeAndConfirmHTML, {confirmLink});
        var txt = ms.render(welcomeAndConfirmText, {confirmLink});
        var subject = "Welcome to Buckets Of Money!";
        return {html: html, text: txt, subject: subject}
    },

    getSubscriptionStartedData: function(){
        var subject = "Upgrade Success!";
        return {html: subscriptionStartedHTML, text: subscriptionStartedText, subject: subject}
    },

    getSubscriptionCancelledData: function(){
        var subject = "Shorrent Subscription Cancelled";
        return {html: subscriptionCancelledHTML, text: subscriptionCancelledText, subject: subject}
    }
};

module.exports = service;
