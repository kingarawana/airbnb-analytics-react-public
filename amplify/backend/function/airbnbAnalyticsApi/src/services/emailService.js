const nodemailer = require('nodemailer');
const config = require('../config');
const emailTemplateService = require('./emailTemplateService');
const EMAIL_SENDER = config.email.username;
const EMAIL_PASS = config.email.pass;
const BASE_URL = 'DUMMY.com'; // this will be removed
const MG_API_KEY = config.email.mailGun.apiKey;
const MG_DOMAIN = 'bucketsofmoneyapp.com'; // TODO: create a new MG acct.
const logger = require('../helpers/logger');
const Packages = require('../classes/Packages')
// const MG_DOMAIN = 'sandbox29487ef01bfe4ae7bb6fd864d14a3c33.mailgun.org';

var mailgun = require('mailgun-js')({apiKey: MG_API_KEY, domain: MG_DOMAIN});

var transporter = nodemailer.createTransport({
    host: 'smtpout.secureserver.net',
    port: 465,
    secure: true, // secure:true for port 465, secure:false for port 587
    auth: {
        user: EMAIL_SENDER,
        pass: EMAIL_PASS
    }
});

var service = {
    sendSubscriptionSwitchedPromise: function(toEmail, oldPackageId, newPackageId){
        // TODO fix this. The email template are not up to date
        var emailData = emailTemplateService.getSubscriptionSwitchedData(
            Packages.getPriceForPackage(newPackageId)
        );
        return service._sendEmailHelper(toEmail, emailData);
    },
    sendSubscriptionResumedPromise: function(toEmail, packageId){
        // TODO fix this. The email template are not up to date
        var emailData = emailTemplateService.getSubscriptionResumedData();
        return service._sendEmailHelper(toEmail, emailData);
    },
    sendPasswordResetEmailPromise: function(toEmail, tempPass){
        return new Promise((fulfill, reject)=>{
            var emailData = emailTemplateService.getResetPasswordData(tempPass, `${BASE_URL}/users/password/update?email=${toEmail}&tp=${tempPass}`);
            let mailOptions = {
                from: `"Buckets Of Money" <${EMAIL_SENDER}>`, // sender address
                to: toEmail, // list of receivers
                subject: emailData.subject,
                text: emailData.text,
                html: emailData.html
            };

            sendMail(mailOptions, (err, info) => {
                if(err){
                    logger.error(err);
                    reject(err);
                }else{
                    logger.debug('Message %s sent: %s', info.messageId, info.response);
                    fulfill(info);
                }
            });
        });
    },
    sendWelcomeEmailWithEmailConfirmationLinkPromise: function(user){
        return new Promise((fulfill, reject)=>{
            var emailData = emailTemplateService.getWelcomeAndConfirmData(`${BASE_URL}/users/${user._id.toString()}/email/confirm`);
            let mailOptions = {
                from: `"Buckets Of Money" <${EMAIL_SENDER}>`, // sender address
                to: user.email, // list of receivers
                subject: emailData.subject,
                text: emailData.text,
                html: emailData.html
            };

            sendMail(mailOptions, (err, info) => {
                if(err){
                    logger.error(err);
                    reject(err);
                }else{
                    logger.debug('Message %s sent: %s', info.messageId, info.response);
                    fulfill(info);
                }
            });
        });
    },
    sendSubscriptionStartedPromise: function(user){
        return new Promise((fulfill, reject)=>{
            var emailData = emailTemplateService.getSubscriptionStartedData();
            let mailOptions = {
                from: `"Buckets Of Money" <${EMAIL_SENDER}>`, // sender address
                to: user.email, // list of receivers
                subject: emailData.subject,
                text: emailData.text,
                html: emailData.html
            };

            sendMail(mailOptions, (err, info) => {
                if(err){
                    logger.error(err);
                    reject(err);
                }else{
                    logger.debug('Message %s sent: %s', info.messageId, info.response);
                    fulfill(info);
                }
            });
        });
    },
    sendSubscriptionCancelledPromise: function(user){
        return new Promise((fulfill, reject)=>{
            var emailData = emailTemplateService.getSubscriptionCancelledData();
            let mailOptions = {
                from: `"Buckets Of Money" <${EMAIL_SENDER}>`, // sender address
                to: user.email, // list of receivers
                subject: emailData.subject,
                text: emailData.text,
                html: emailData.html
            };

            sendMail(mailOptions, (err, info) => {
                if(err){
                    logger.error(err);
                    reject(err);
                }else{
                    logger.debug('Message %s sent: %s', info.messageId, info.response);
                    fulfill(info);
                }
            });
        });
    },
    _sendEmailHelper: function(to, emailData, cb){
        return new Promise((fulfill, reject)=>{
            let mailOptions = {
                from: `"Buckets Of Money" <${EMAIL_SENDER}>`, // sender address
                to: to, // list of receivers
                subject: emailData.subject,
                text: emailData.text,
                html: emailData.html
            };

            sendMail(mailOptions, (err, info) => {
                if(err){
                    logger.error(err);
                    reject(err);
                }else{
                    logger.debug('Email sent. Subject: [%s] response: %s', emailData.subject, info.response);
                    fulfill(info);
                }
                if(cb){
                    cb(err, info)
                }
            });
        });
    }
};


var sendMail = function(options, cb){
    if(process.env.ENV === 'prod'){
        mailgun.messages().send(options, cb); // TODO: UNDO disabling for now while i'm testing.
        cb(null, {todo: 're enable mailgun. I\'ve disabled it while dev testing'});
    }else{
        transporter.sendMail(options, cb);
    }
};

if(process.env.ENV === 'test-it' || process.env.ENV === 'test'){
    transporter = {
        sendMail: ()=> Promise.resolve()
    };
    sendMail = function(options, cb){
        cb(null, "mail success!");
    };
}

module.exports = service;
