// let conn = require('./models/mongooseManager');
// let UserProfile = require('./models/UserProfile');
//
// let profile = new UserProfile({userId: 'testing'});
// profile.save().then(doc => {
//     console.log('the saved doc');
// }).catch(err => {
//     console.log('err', err);
// });
//
// console.log('the user profile', UserProfile);


// let config = require('./config');
// let mongoose = require('mongoose');
// let connection = null;
// let Schema = mongoose.Schema;
//
// (async function(){
//     try{
//         connection = await mongoose.createConnection(config.mongo.host, {
//             user: config.mongo.username,
//             pass: config.mongo.pass,
//             useNewUrlParser: true,
//             useUnifiedTopology: true,
//             bufferCommands: false,
//             bufferMaxEntries: 0
//         });
//
//         function errorHandler(){
//             console.log('closing')
//             connection.close(function () {
//                 console.log('closed');
//                 process.exit(0);
//             });
//         }
//
//         process.on('SIGINT', errorHandler);
//         process.on('SIGTERM', errorHandler);
//         process.on('SIGHUP', errorHandler);
//
//         connection.on('close', function(){
//             process.removeListener('SIGINT', errorHandler);
//             process.removeListener('SIGTERM', errorHandler);
//             process.removeListener('SIGHUP', errorHandler);
//         });
//
//         console.log('connection success!')
//     }catch(err){
//         console.log('the err', err);
//     }
//
//
//     const ModelSchema = new Schema({
//         userId: {
//             type: String,
//             required: true
//         },
//         email: String,
//         paymentCustomerId: String,
//         archived: {
//             type: Boolean,
//             default: false
//         },
//         creationDate: {
//             type: Number,
//             default: model => Math.floor(Date.now() / 86400000)
//         }
//     });
//
//     let UserProfile = mongoose.connection.model('UserProfile', ModelSchema);
//     console.log('the profile', UserProfile)
//     let profile = new UserProfile({userId: 'testing'});
//     profile.save().then(doc => {
//         console.log('the saved doc', doc);
//     }).catch(err => {
//         console.log('err', err);
//     });
// })();

(async function(){
    let models = await require('./models');
    let UserProfile = models.UserProfile;
    let profile = new UserProfile({userId: 'testing'});
    profile.save().then(doc => {
        console.log('the saved doc', doc);
    }).catch(err => {
        console.log('err', err);
    });
})();