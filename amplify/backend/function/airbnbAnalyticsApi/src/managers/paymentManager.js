let stripeService = require('../services/stripeService');
let AppError = require('../helpers/AppError');
let moment = require('moment');
let logger = require('../helpers/logger');
let PaymentMethod = null;
let Subscription = null;

const FREE_TRIAL_PERIOD = 30;

var paymentManager = {
    updatePaymentMethodWithCardIdPromise: async function(customerId, paymentMethod){
        if(paymentMethod.cardId) return;
        var cards = await stripeService.getAllCustomerCardsPromise(customerId);
        for(var i = 0; i < cards.length; i++){
            var card = cards[i];
            if(card.last4 === paymentMethod.last4){
                paymentMethod.cardId = card.id;
                break;
            }
        }
        if(paymentMethod.cardId){
            await paymentMethod.savePromise();
        }
    },

    setPaymentMethodAsPrimaryPromise: async function(paymentCustomerId, paymentMethodId){
        var pm = await PaymentMethod.findOneByIdPromise(paymentMethodId);
        pm.setAsDefaultPayment();
        await stripeService.updateDefaultPaymentPromise(paymentCustomerId, pm.cardId);
        await pm.savePromise();
        return pm;
    },

    updatePaymentMethodPromise: async function(userId, paymentMethodId, updates){
        var pm = await PaymentMethod.findOneByUserIdAndIdPromise(userId, paymentMethodId);
        if(!pm || !updates){
            throw 'Invalid call';
        }
        updates = ['name', 'expireMonth', 'expireYear'].reduce((prev, key) => {
            if(updates[key]){
                prev[key] = updates[key];
            }
            return prev;
        }, {});
        if(Object.keys(updates).length){
            pm.setPropertiesFromDict(updates);
            await pm.savePromise();
        }
        return pm;
    },

    addCreditCardAndSetAsPrimaryPromise: async function(user, paymentData){
        var paymentMethod = await PaymentMethod.findOneByUserIdAndTokenIdPromise(user.getIdString(), paymentData.tokenId);
        if(!paymentMethod){
            paymentMethod = new PaymentMethod(paymentData);
            paymentMethod.userId = user.getIdString();
            paymentMethod.email = user.email;
        }
        // console.log("YYY", card.cardId, card.tokenId, user.paymentCustomerId);
        if(user.paymentCustomerId){
            var rawCardData = await stripeService.addCardToCustomerPromise(user.paymentCustomerId, paymentMethod.tokenId);
            // had to add this b/c on android, the cardId is missing. It's a bug w/ the framework that the mobile app is using. tipsi-stripe
            if(!paymentMethod.cardId){
                paymentMethod.cardId = rawCardData.id;
            }
        }

        paymentMethod.setAsDefaultPayment();
        // why didn't i add this check to the above if statement?
        if(user.paymentCustomerId){
            await stripeService.updateDefaultPaymentPromise(user.paymentCustomerId, paymentMethod.cardId);
        }
        await paymentMethod.savePromise();
        return paymentMethod;
    },

    /**
     * Creates a customer in stripe and a Payment Method in stripeService
     * Also creates a PaymentMethod in our system and sets the paymentCustomerId
     * field on the user.
     *
     * if a customer already exits, it creates and adds the card as a default.
     * if the card already exists it just adds it as the default.
     *
     * NOTE: Wait, what happens if the customer and the card doesn't exit? I don't see
     * where the code adds the card
     */
    createCustomerWithPaymentDataPromise: async function(user, paymentData){
        // this can be refactored. I'll do it after i write some tests
        if(!user.paymentCustomerId){
            let paymentMethod = await paymentManager.addCreditCardAndSetAsPrimaryPromise(user, paymentData);
            let stripeCustomer = await stripeService.createCustomerWithPaymentMethodPromise(user, paymentMethod);
            user.paymentCustomerId = stripeCustomer.id;
            await paymentManager.updatePaymentMethodWithCardIdPromise(user.paymentCustomerId, paymentMethod);
            await user.savePromise();
            return paymentMethod;
        }else{
            return await paymentManager.addCreditCardAndSetAsPrimaryPromise(user, paymentData);
        }
    },
    deletePaymentMethodPromise: async function(user, paymentMethodId){
        // using findOneByUserIdAndIdPromise just as extra precaution
        let pm = await PaymentMethod.findOneByUserIdAndIdPromise(user.getIdString(), paymentMethodId);
        if(pm){
            try{
                await stripeService.deleteCustomerCardPromise(user.paymentCustomerId, pm.cardId);
                await pm.archiveAndSavePromise();
            }catch(err){
                logger.error(`Error deleting payment method for user: ${user.getIdString()}, pmId: ${paymentMethodId}`);
                throw new AppError('Error', 500, AppError.codes.APP_ERROR);
            }
        }else{
            throw new AppError('Not Found', 404, AppError.codes.PAYMENT_METHOD_NOT_FOUND);
        }
    },
    /**
     * Adds a new PaymentMethod and sets is as the new default payment source.
     * I don't see any reason why we would need to add a payment, but not want
     * to immediately set is as the default source.
     */
    addPaymentMethodAndSetAsDefault: async function(user, paymentData){
        var paymentMethod = new PaymentMethod({...paymentData, email: user.email, userId: user.getIdString()});
        paymentMethod.setAsDefaultPayment();
        var existingPM = await PaymentMethod.findOneByTokenIdPromise(paymentMethod.tokenId);
        console.log('xxx', 1);
        if(existingPM){
            console.log('xxx', 2);
            let id = existingPM._id;
            existingPM.set(paymentMethod);
            existingPM._id = id; // looks like mongoose now generates the _id before saving. Have to change back to original id.
            paymentMethod = existingPM;
            // if existing PM is true, there's no need to updateDefaultPaymentPromise since it's already default.
        }else{
            console.log('xxx', 3);
            // await stripeService.addCardToCustomerPromise(user.paymentCustomerId, paymentMethod.tokenId);
            if(!paymentMethod.cardId){
                console.log('xxx', 4);
                await stripeService.addCardToCustomerPromise(user.paymentCustomerId, paymentMethod.tokenId);
                // TODO: why not use the card returned from addCardToCustomerPromise and set it on the paymentMethod?
                // COME BACK TO THIS AND CHECK. I DON'T SEE WHY IT WOULDN'T WORK.
                console.log('xxx', 5);
                await paymentManager.updatePaymentMethodWithCardIdPromise(user.paymentCustomerId, paymentMethod);
                console.log('xxx', 6);
            }
            console.log('xxx', 7);
            await stripeService.updateDefaultPaymentPromise(user.paymentCustomerId, paymentMethod.cardId);
            console.log('xxx', 8);
        }
        console.log('xxx', 9);
        await paymentMethod.savePromise();
        console.log('xxx', 10);
    },

    getAllPaymentMethodsByUserIdPromise: function(userId){
        // for now making assumption that all of them are valid.
        return PaymentMethod.findAllByUserIdPromise(userId);
    },
    /**
     * Returns the beginning of end date as milliseconds
     * NOTE: i should probably rename this to _getEndDateInNDaysAsMilliseconds
     */
    _getEndDateInNDays: function(days, today){
        today = moment(today);
        today = today.add(days, 'days').startOf('day');
        return today.valueOf();
    },

    // ===================== ALL SUBSCRIPTION METHODS SHOULD BE REFACTORED INTO IT'S OWN MANAGER =====================
    getChangeSubscriptionCostPromise: async (user, desiredPackageId, prorationDate)=>{
        var sub = await Subscription.findOneByUserIdPromise(user.getIdString());
        // i don't think i need this
        if(!prorationDate) {
            prorationDate = Math.floor(Date.now() / 1000);
        }
        return await stripeService.getProrationAmountPromise(user.paymentCustomerId, sub.externalSubscriptionId, desiredPackageId, prorationDate);
    },

    /**
     * @return {oldPackageId, newPackageId}
     */
    changeSubscriptionPromise: async (user, desiredPackageId, prorationDate)=>{
        var result = {};
        var sub = await Subscription.findOneByUserIdPromise(user.getIdString());
        // we can assume sub exists, b/c there would be no reason to call this API if there didn't exist a subscription to change.
        result.oldPackageId = sub.subscriptionPackageId;
        result.newPackageId = desiredPackageId;
        await stripeService.changeSubscriptionPromise(user.paymentCustomerId, sub.externalSubscriptionId, desiredPackageId, prorationDate);
        sub.subscriptionPackageId = desiredPackageId;
        await sub.savePromise();
        return result;
    },

    /**
     * Starts subscription for given user.
     */
    startFreeTrialSubscriptionPromise: function(user, packageId){
        return paymentManager.startSubscriptionPromise(user, FREE_TRIAL_PERIOD, packageId);
    },
    startImmediateSubscriptionPromise: function(user, packageId){
        return paymentManager.startSubscriptionPromise(user, 1, packageId);
    },

    startSubscriptionPromise: async function(user, numberOfFreeDays, packageId){
        const userId = user._id.toString();
        var pm = await PaymentMethod.getDefaultPaymenetMethodForUserPromise(userId);
        if(!pm) throw 'User has no payment method: ' + userId;
        var today = moment(); // i'm leaving the current millisecond. not going to make it start-of-day.
        var trialEndDate = paymentManager._getEndDateInNDays(numberOfFreeDays, today);
        var subscriptionData = await stripeService.createAndStartSubscriptionForUserPromise(user, pm, trialEndDate, packageId);
        var subData = {
            userId: userId,
            externalSubscriptionId: subscriptionData.id,
            paymentMethodId: pm._id.toString(),
            trialStartDate: today.valueOf(),
            trialEndDate: trialEndDate,
            subscriptionPackageId: packageId
        };
        var sub = new Subscription(subData);
        sub.setPrice();
        await sub.savePromise();
    },

    /**
     * sub Last active subscription.
     * TODO: Test
     *  when testing ensure that trialEndDate is set correctly. I had this bug.
     */
    resumeSubscriptionPromise: async function(user, sub){
        const userId = user._id.toString();
        var pm = await PaymentMethod.getDefaultPaymenetMethodForUserPromise(userId);
        if(!pm) throw 'User has no payment method: ' + userId;
        var trialEndDate = 'now';
        if(sub.trialEndDate > Date.now()){
            trialEndDate = sub.trialEndDate;
        }
        // NOTE: createAndStartSubscriptionForUserPromise doesn't even need pm anymore.
        var subscriptionData = await stripeService.createAndStartSubscriptionForUserPromise(user, pm, trialEndDate, sub.subscriptionPackageId);
        sub.externalSubscriptionId = subscriptionData.id;
        sub.paymentMethodId = pm._id.toString();
        sub.archived = false;
        await sub.savePromise();
    },
    /**
     * Cancels a users subscription.
     */
    cancelSubscriptionPromise: async function(user){
        var userId = user._id.toString();
        var sub = await Subscription.findOneByUserIdPromise(userId);
        if(!sub) throw new AppError('User has no subscription to cancel: ' + userId, 404, AppError.codes.SUBSCRIPTION_NOT_FOUND);
        await stripeService.cancelSubscriptionPromise(sub.externalSubscriptionId);
        sub.cancel();
        await sub.savePromise();
    },
    init: async function(){
        if(!PaymentMethod){
            let models = await require('../models');
            PaymentMethod = models.PaymentMethod;
            Subscription = models.Subscription;
        }
    }
};

module.exports = paymentManager;
