const paymentManager = require('./paymentManager');
const logger = require('../helpers/logger');
const util = require('../helpers/util');
const AppError = require('../helpers/AppError');
const _ = require('lodash');

let PaymentMethod = null;
let Subscription = null;
let User = null;
let UserSettings = null;

let userAccountManager = {
    registerNewUserPromise: async userData => {
        let user = await new User(userData).savePromise();
        // I don't need this atm. Commenting this out.
        // return userAccountManager._updateUserWithSettingsPromise(user, true);
        return user;
    },
    getUserPromise: userId => {
        return User.findOneByIdPromise(userId);
    },
    existsPromise: q => {
        return User.existsPromise(q);
    },
    updateUserPromise: async (userId, attributes) => {
        const updateable = ['name', 'email'];
        let updates = updateable.reduce((prev, attr)=>{
            if(attributes[attr]){
                prev[attr] = attributes[attr];
            }
            return prev;
        }, {});
        if(_.isEmpty(updates)){
            return;
        }
        let result = await User.updateOnePromise({_id: userId}, updates);
        if(result.n === 0){
            throw new AppError('Not Found', 404, AppError.codes.USER_NOT_FOUND);
        }
    },
    getUserByCognitoIdPromise: async (cognitoId, fieldSelectString='') => {
        return User.findOneByCognitoIdPromise(cognitoId, fieldSelectString);
    },
    _updateUserWithSettingsPromise: async (user, isNew)=>{
        if(isNew){
            try{
                let us = await new UserSettings({userId: user.getIdString()}).savePromise();
                user.userSettings = us;
                return user;
            }catch(err){
                await user.removePromise();
                throw err;
            }
        }else{
            let us = await UserSettings.findOneByUserIdPromise(user.getIdString());
            if(!us){
                logger.error('_updateUserWithSettingsPromise: for some reason this fn was called saying the user is not new, but the user did not have UserSettings: ' + user.getIdString());
                throw Error('User is missing UserSettings')
            }
            user.userSettings = us;
            return us;
        }
    },
    /**
     *
     * @param user
     * @param desiredPackageId
     * @returns {Promise<null|{trialEndDate: number, prorationDate: number, cost: number, nextChargeDate: number, freeTrial: Boolean}>}
     */
    getChangeSubscriptionCostPromise: async (user, desiredPackageId) => {
        let sub = await Subscription.findOneByUserIdPromise(user.getIdString(), true);
        let cancelled = Subscription.existsPromise({userId: user.getIdString(), archived: true});
        let result = {freeTrial: false};
        // if they've cancelled and has no subs, then no freeTrial or proration
        if(cancelled && !sub){
            return result;
        }
        // if they a sub, then calculate the proration.
        if(sub){
            let prorationDate = Math.floor(Date.now() / 1000);
            let changeData = await paymentManager.getChangeSubscriptionCostPromise(user, desiredPackageId, prorationDate);
            let trialEndDate = sub.trialEndDate;
            if(trialEndDate < Date.now()){ // this should be fine, since trialEndDate is the beginning of the day.
                trialEndDate = 0;
            }
            return {...result, ...changeData, prorationDate, trialEndDate};
        }else{
            // if they haven't cancelled and has no sub, give them a free trial.
            return {freeTrial: true};
        }
    },
    getUserSubscriptionPromise: user => {
        return Subscription.findOneByUserIdPromise(user.getIdString());
    },

    /**
     * @return {oldPackageId, newPackageId}
     */
    changeSubscriptionPromise: (user, desiredPackageId, prorationDate)=>{
        return paymentManager.changeSubscriptionPromise(user, desiredPackageId, prorationDate);
    },

    isPaidAccountPromise: async userId => {
        let activeSubs = await Subscription.findSubscriptionsByUserIdPromise(userId, true);
        return activeSubs.length > 0;
    },

    /**
     * If a previous active subscription is found, it cancels it, and starts a new one immediately.
     * if a previous archived subscriptions is found, it starts immediately, since the user already had the free trail
     * If no archived subscriptions are found, it starts a free trial.
     */
    startSubscriptionSmartPromise: async (user, paymentMethodId, packageId)=>{
        if(!user.paymentCustomerId) throw 'Cannot start subscription for a non registered paid user: ' + user.getIdString();

        // hum, why would I have this? shouldn't a default payment method already be set when it was created?
        // TODO: Come back and check on this to see if I can remove it.
        if(paymentMethodId){
            await paymentManager.setPaymentMethodAsPrimaryPromise(user.paymentCustomerId, paymentMethodId)
        }
        // this would be more of an error case. The user shouldn't be able to call this if they already have an active subscription.
        let activeSubs = await Subscription.findSubscriptionsByUserIdPromise(user.getIdString(), true);
        if(activeSubs.length > 0){
            await userAccountManager.cancelSubscriptionPromise(user);
            await userAccountManager._startSubscriptionPromise(user, paymentManager.startImmediateSubscriptionPromise, packageId);
            logger.warn("POTENTIAL ERROR: User started subscription when they already had an existing subsription. " + user.getIdString());
            return;
        }
        let archivedSubs = await Subscription.findSubscriptionsByUserIdPromise(user.getIdString(), false);
        if(archivedSubs.length === 0){
            await userAccountManager._startSubscriptionPromise(user, paymentManager.startFreeTrialSubscriptionPromise, packageId);
        }else{
            /**
             * The reason for this else block is in the situation the user already had a free trial. Therefore
             * their new subscription should start immediately.
             */
            if(archivedSubs.length > 1){
                logger.error("WARNING/ERROR: There should only be one subscription per user: uId: " + user._id);
            }
            await userAccountManager._startSubscriptionPromise(user, paymentManager.startImmediateSubscriptionPromise, packageId);
        }
    },

    /**
     * packageId Not used for now. It will only be used in the case where users want to switch to another package. they don't already have.
     *
     * For now I'm making the assumption that users currently can't switch plans during resuming their subscription.
     */
    resumeSubscriptionSmartPromise: async (user, paymentMethodId, packageId)=>{
        if(!user.paymentCustomerId) throw 'Cannot start subscription for a non registgered paid user: ' + user.getIdString();

        if(paymentMethodId){
            await paymentManager.setPaymentMethodAsPrimaryPromise(user.paymentCustomerId, paymentMethodId)
        }

        let activeSubs = await Subscription.findSubscriptionsByUserIdPromise(user.getIdString(), true);
        if(activeSubs.length > 0){
            throw 'Cannot resume subscription, there is already an active subscription.'
        }

        let lastActiveSub = await Subscription.getLastActiveSubscriptionByUserId(user.getIdString());
        if(!lastActiveSub){
            throw 'User has no subscription to resume.'
        }

        await userAccountManager._startSubscriptionPromise(user, paymentManager.resumeSubscriptionPromise, lastActiveSub);
    },

    _startSubscriptionPromise: async (user, fn, fnArg)=>{
        await fn(user, fnArg);
        // other things can happen here
    },

    cancelSubscriptionPromise: async user => {
        await paymentManager.cancelSubscriptionPromise(user);
    },
    init: async function(){
        if(!PaymentMethod){
            let models = await require('../models');
            await paymentManager.init();
            PaymentMethod = models.PaymentMethod;
            Subscription = models.Subscription;
            User = models.User;
            UserSettings = models.UserSettings;
        }
    }
};

module.exports = userAccountManager;