const AppResponse = require('../helpers/AppResponse');
const scraperService = require('../scraperService');

module.exports = {
    /**
     * Gets the listing info by the listing id from airbnb.com
     *
     * @param req
     * @param res
     * @returns {Promise<void>}
     */
    getListingByListingId: async function (req, res) {
        let listingId = req.query.listingId;
        let listing = await scraperService.getListingDetailsByListingId(listingId);
        new AppResponse('Success', listing, 200).sendResponse(res);
    },
    /**
     * Gets the detailed listing info for a specific listing.
     *
     * @param req
     * @param res
     * @returns {Promise<void>}
     */
    getListingDataByListingId: async function(req, res){
        let listingId = req.params.listingId;
        let numMonths = req.query.months ? parseInt(req.query.months) : 3;
        let calendar = await scraperService.getCalendarForListingId(listingId, numMonths);
        new AppResponse('Success', {calendar}, 200).sendResponse(res);
    },
};