const AppResponse = require('../helpers/AppResponse');
const AppError = require('../helpers/AppError');
const logger = require('../helpers/logger');
const userAccountManager = require('../managers/userAccountManager');
const emailService = require('../services/emailService');
const Packages = require('../classes/Packages');

let Subscription = null;

module.exports = {
    startSubscription: async function(req, res){
        await init();
        var user = req.resources.user;
        // had to the the || b/c of old api from payment route.
        var paymentMethodId = req.query.pmId || req.params.paymentId;
        var pkgId = req.query.pkgId || req.params.packageId;
        console.log(user, paymentMethodId, pkgId);
        if(!Packages.isValidPackageId(pkgId)){
            return new AppError('Error starting subscription.', 500, AppError.codes.SUBSCRIPTION_ERROR).sendResponse(res);
        }

        userAccountManager.startSubscriptionSmartPromise(user, paymentMethodId, pkgId).then(()=>{
            // i might need to send a different email depending if it's a free trial or resume.
            emailService.sendSubscriptionStartedPromise(user).catch(err => {
                logger.error("Error sending confirm email: ", err);
            });
            new AppResponse('Success', null, 202).sendResponse(res);
        }).catch(err => {
            logger.error(err);
            new AppError('Error starting subscription.', 500, AppError.codes.SUBSCRIPTION_ERROR).sendResponse(res);
        });
    },
    getUserArchivedSubscriptions: async function(req, res){
        await init();
        var user = req.resources.user;
        var includeUpdatedPrice = req.query.iup; // this might not even be necessary since a stripe plan price can't be changed if there are any active users subscribed to it.
        Subscription.findSubscriptionsByUserIdPromise(user.getIdString(), false).then(subs => {
            if(includeUpdatedPrice){
                subs.forEach(s => s.setPrice());
            }
            new AppResponse('Success', _cleanSubscription(subs), 200).sendResponse(res);
        }).catch(err => {
            new AppError('Error getting subscription.', 500, AppError.codes.APP_ERROR).sendResponse(res);
        });
    },
    getUserSubscription: async function(req, res){
        await init();
        var user = req.resources.user;
        userAccountManager.getUserSubscriptionPromise(user).then((sub)=>{
            if(sub){
                new AppResponse('Success', sub.subscriptionPackageId, 200).sendResponse(res);
            }else{
                new AppResponse('Success', null, 200).sendResponse(res);
            }
        }).catch(err => {
            new AppError('Error getting subscription.', 500, AppError.codes.APP_ERROR).sendResponse(res);
        });
    },
    getProrationCost: async function(req, res){
        await init();
        var user = req.resources.user;
        var pkgId = req.params.packageId;
        // return new AppResponse('Success', {cost: 9.99, t: Date.now(), trialEndDate: Date.now()}, 200).sendResponse(res);
        userAccountManager.getChangeSubscriptionCostPromise(user, pkgId).then(results => {
            results = {...results};
            results.t = results.prorationDate;
            delete results.prorationDate;
            new AppResponse('Success', results, 200).sendResponse(res);
        }).catch(err => {
            logger.error(err);
            new AppError('Error changing subscription.', 500, AppError.codes.SUBSCRIPTION_ERROR).sendResponse(res);
        });
    },
    /**
     *
     * @param prorationDate must be the time remaining in seconds. For example 15 days remaining, so it'll be
     * today - 15 days in seconds.
     * @returns {Promise<void>}
     */
    changeSubscription: async function(req, res){
        await init();
        var user = req.resources.user;
        var pkgId = req.params.packageId;
        var prorationDate = req.query.t;

        if(!prorationDate){
            return new AppError('Error changing subscription.', 400, AppError.codes.SUBSCRIPTION_UPDATE_ERROR).sendResponse(res);
        }

        prorationDate = parseInt(prorationDate);

        var diff = Math.round((Date.now()/1000) - prorationDate);
        // what is this # 2764800? 32 days? How did I choose 32? Why not 31? or 30 + 1 millisecond?
        if(diff > 2764800 || diff < 0){
            return new AppError('Error changing subscription.', 400, AppError.codes.SUBSCRIPTION_UPDATE_ERROR).sendResponse(res);
        }
        if(!Packages.isValidPackageId(pkgId)){
            return new AppError('Error changing subscription.', 500, AppError.codes.SUBSCRIPTION_ERROR).sendResponse(res);
        }
        userAccountManager.changeSubscriptionPromise(user, pkgId, prorationDate).then((results)=>{
            emailService.sendSubscriptionSwitchedPromise(user.email, results.oldPackageId, results.newPackageId).catch(err => {
                logger.error("Error sending confirm email: ", err);
            });
            new AppResponse('Success', null, 202).sendResponse(res);
        }).catch(err => {
            logger.error(err);
            new AppError('Error changing subscription.', 500, AppError.codes.SUBSCRIPTION_ERROR).sendResponse(res);
        });
    },
    resumeSubscription: async function(req, res){
        await init();
        var user = req.resources.user;
        // had to the the || b/c of old api from payment route.
        var paymentMethodId = req.query.pmId;
        var pkgId = req.query.pkgId; // the frontend has this b/c it makes a request for last active sub to show info to user.

        if(pkgId && !Packages.isValidPackageId(pkgId)){
            return new AppError('Error starting subscription.', 500, AppError.codes.SUBSCRIPTION_ERROR).sendResponse(res);
        }

        userAccountManager.resumeSubscriptionSmartPromise(user, paymentMethodId, pkgId).then(()=>{
            // i might need to send a different email depending if it's a free trial or resume.

            emailService.sendSubscriptionResumedPromise(user.email, pkgId).catch(err => {
                logger.error("Error sending confirm email: ", err);
            });
            new AppResponse('Success', null, 202).sendResponse(res);
        }).catch(err => {
            logger.error(err);
            new AppError('Error starting subscription.', 500, AppError.codes.SUBSCRIPTION_ERROR).sendResponse(res);
        });
    },
    cancelSubscription: async function(req, res){
        await init();
        var user = req.resources.user;
        userAccountManager.cancelSubscriptionPromise(user).then(()=>{
            emailService.sendSubscriptionCancelledPromise(user).catch(err => {
                logger.error("Error sending confirm email: ", err);
            });
            new AppResponse('Success', null, 202).sendResponse(res);
        }).catch(err => {
            if(err.appCode === AppError.codes.SUBSCRIPTION_NOT_FOUND){
                // TODO: do logging so i get notified when this happens.
                logger.warn("ATTEMPTED TO CANCEL SUBSCRIPTION, BUT USER HAD NO SUBSCRIPTION.")
                new AppResponse('Success', null, 202).sendResponse(res); // pretending as if it was successful.
            }else{
                new AppError('Error canceling payment method.', 500, AppError.codes.APP_ERROR).sendResponse(res);
            }
        });
    },
    getPlans: async function(req, res){
        var plans = [
            {packageId: 'bom-basic-monthly-plan', price: 2.99, numberOfConnections: '1', description: 'Connect up to 1 bank and credit cards accounts.'},
            {packageId: 'bom-499-4-monthly-plan', price: 4.99, numberOfConnections: '4', description: 'Connect up to 4 bank and credit cards accounts.'},
            {packageId: 'bom-1999-20-monthly-plan', price: 9.99, numberOfConnections: 'unlimited', description: 'Connect Unlimited bank and credit cards accounts.'}
        ];
        new AppResponse('Success', plans, 200).sendResponse(res);
    },
};

async function init(){
    if(!Subscription){
        await userAccountManager.init();
        let models = await require('../models');
        Subscription = models.Subscription;
    }
}

function _cleanSubscription(sub){
    if(sub instanceof Array){
        sub.forEach(s => _cleanSubscription(s))
    }else{
        sub.paymentMethodId = null;
        sub.externalSubscriptionId = null;
    }
    return sub;
}