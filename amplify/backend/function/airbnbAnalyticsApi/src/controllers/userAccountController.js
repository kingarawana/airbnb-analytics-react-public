const AppResponse = require('../helpers/AppResponse');
const AppError = require('../helpers/AppError');
const userAccountManager = require('../managers/userAccountManager');
const _ = require('lodash');
module.exports = {
    registerUser: async function (req, res) {
        await userAccountManager.init();
        var userData = req.body;
        if(!('cognitoId' in userData) || !('email' in userData) || !('name' in userData)){
            return new AppError('Error starting subscription.', 400, AppError.codes.INVALID_DATA).sendResponse(res);
        }
        userAccountManager.registerNewUserPromise(userData).then(user => {
            new AppResponse('Success', user, 200).sendResponse(res);
        }).catch(err => {
            if(err.code === 11000){
                new AppError('Error starting subscription.', 400, AppError.codes.DUPLICATE_DATA).sendResponse(res);
            }else{
                new AppError('Error starting subscription.', 500, AppError.codes.APP_ERROR).sendResponse(res);
            }
        });
    },
    getUser: async function(req, res){
        await userAccountManager.init();
        let userId = req.params.userId;
        userAccountManager.getUserPromise(userId).then(user => {
            new AppResponse('Success', _cleanUser(user), 200).sendResponse(res);
        }).catch(err => {
            new AppError('Error starting subscription.', 500, AppError.codes.APP_ERROR).sendResponse(res);
        });
    },
    userExists: async function(req, res){
        await userAccountManager.init();
        let q = req.query;
        userAccountManager.existsPromise(q).then(exists => {
            new AppResponse('Success', exists, 200).sendResponse(res);
        }).catch(err => {
            new AppError('Error starting subscription.', 500, AppError.codes.APP_ERROR).sendResponse(res);
        });
    },
    updateUser: async function(req, res){
        await userAccountManager.init();
        if(_.isEmpty(req.body)){
            return new AppError('Invalid request.', 400, AppError.codes.INVALID_REQUEST).sendResponse(res);
        }
        userAccountManager.updateUserPromise(req.params.userId, req.body).then(() => {
            new AppResponse('Success', null, 200).sendResponse(res);
        }).catch(err => {
            if(AppError.isAppError(err)){
                err.sendResponse(res);
            }else{
                new AppError('Error starting subscription.', 500, AppError.codes.APP_ERROR).sendResponse(res);
            }
        });
    },
    getUserByCognitoId: async function(req, res){
        await userAccountManager.init();
        userAccountManager.getUserByCognitoIdPromise(req.params.cognitoId, '_id paymentCustomerId').then(user => {
            // user.paymentCustomerId = 'kdkdkdkkddkkddkdk'
            new AppResponse('Success', _cleanUser(user), 200).sendResponse(res);
        }).catch(err => {
            new AppError('Error starting subscription.', 500, AppError.codes.APP_ERROR).sendResponse(res);
        });
    }
};

function _cleanUser(user){
    return user ? user.cleanForResp() : null;
}