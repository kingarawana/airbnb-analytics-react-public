const paymentManager = require('../managers/paymentManager');
const AppResponse = require('../helpers/AppResponse');
const AppError = require('../helpers/AppError');
const logger = require('../helpers/logger');

module.exports = {
    /**
     * Request body should be an object with the form of {PaymentMethod}
     *
     * @param req
     * @param res
     * @returns {Promise<void>}
     */
    createPaymentMethod: async function(req, res){
        await paymentManager.init();
        let paymentMethodData = req.body;
        var user = req.resources.user;
        let pm = await paymentManager.createCustomerWithPaymentDataPromise(user, paymentMethodData);
        new AppResponse('Success', _cleanPaymentMethod(pm.clone()), 202).sendResponse(res);
    },
    /**
     * I should probably refactor `createPaymentMethod` and `addAndSetDefaultPaymentMethod` so that it's just one fn w/
     * an extra param to set as default or not.
     * @param req
     * @param res
     * @returns {Promise<void>}
     */
    addAndSetDefaultPaymentMethod: async function(req, res){
        await paymentManager.init();
        var paymentMethodData = req.body;
        var user = req.resources.user;
        paymentManager.addPaymentMethodAndSetAsDefault(user, paymentMethodData).then(()=>{
            new AppResponse('Success', null, 202).sendResponse(res);
        }).catch(err => {
            logger.error('addAndSetDefaultPaymentMethod: ', err);
            new AppError('Error adding payment method.', 500, AppError.codes.APP_ERROR).sendResponse(res);
        });
    },
    deletePaymentMethod: async function(req, res){
        await paymentManager.init();
        var user = req.resources.user;
        let paymentMethodId = req.params.modelId;
        try{
            await paymentManager.deletePaymentMethodPromise(user, paymentMethodId);
            new AppResponse('Success', null, 200).sendResponse(res);
        }catch(err){
            err.sendResponse(res);
        }
    },
    setAsPrimaryPaymentMethod: async function(req, res){
        await paymentManager.init();
        var user = req.resources.user;
        var paymentMethodId = req.params.modelId;
        paymentManager.setPaymentMethodAsPrimaryPromise(user.paymentCustomerId, paymentMethodId).then(pm => {
            new AppResponse('Success', _cleanPaymentMethod(pm.clone()), 202).sendResponse(res);
        }).catch(err => {
            logger.error('setAsPrimaryPaymentMethod', err);
            new AppError('Error adding payment method.', 500, AppError.codes.APP_ERROR).sendResponse(res);
        });
    },
    updatePaymentMethod: async function(req, res){
        await paymentManager.init();
        var user = req.resources.user;
        var paymentMethodId = req.params.modelId;
        let body = req.body;
        paymentManager.updatePaymentMethodPromise(user.getIdString(), paymentMethodId, body).then(pm => {
            new AppResponse('Success', _cleanPaymentMethod(pm.clone()), 202).sendResponse(res);
        }).catch(err => {
            logger.error('setAsPrimaryPaymentMethod', err);
            new AppError('Error updating payment method.', 500, AppError.codes.APP_ERROR).sendResponse(res);
        });
    },
    getPaymentMethods: async function(req, res){
        await paymentManager.init();
        var user = req.resources.user;
        paymentManager.getAllPaymentMethodsByUserIdPromise(user._id.toString()).then(pms => {
            pms = pms.map(pm => {
                return _cleanPaymentMethod(pm.clone());
            });
            new AppResponse('Success', pms, 200).sendResponse(res);
        }).catch(err => {
            new AppError('Error adding payment method.', 500, AppError.codes.APP_ERROR).sendResponse(res);
        });
    }
};

function _cleanPaymentMethod(pm){
    pm.email = null;
    pm.tokenId = null;
    pm.addressZip = null;
    pm.cardId = null;
    return pm;
}