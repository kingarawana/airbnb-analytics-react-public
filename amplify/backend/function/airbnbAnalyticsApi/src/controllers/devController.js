const AppResponse = require('../helpers/AppResponse');
const AWS = require('aws-sdk');
const stripeService = require('../services/stripeService');

let credentials = new AWS.SharedIniFileCredentials({profile: 'amplify'});
AWS.config.update({region:'us-west-2', credentials});
let cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

const _ = require('lodash');

let PaymentMethod = null;
let Subscription = null;
let User = null;
let UserSettings = null;

module.exports = {
    deleteUser: async function (req, res) {
        await init();
        let email = req.query.email;
        let user = await User.findOnePromise({email});

        console.log('found the user', user)
        if(user){
            var params = {
                UserPoolId: 'us-west-2_59ei1UOgF', /* required */
                Username: user.email /* required */
            };
            try{
                let resp = await new Promise((fulfill, reject) => {
                    cognitoidentityserviceprovider.adminDeleteUser(params, function(err, data) {
                        if (err){
                            console.log(err, err.stack);
                            reject(err);
                        } else {
                            fulfill(data)
                        }
                    });
                });
                console.log('the resp ****', resp);
            }catch(err){
                if(err.code === 'UserNotFoundException'){
                    console.log('User not found in Cognito')
                }else{
                    throw err;
                }
            }
            if(user.paymentCustomerId){
                await stripeService.deleteCustomerPromise(user.paymentCustomerId);
            }

            await PaymentMethod.deleteAllByUserIdPromise(user.getIdString());
            await Subscription.deleteAllByUserIdPromise(user.getIdString());
            await user.removePromise();
        }

        new AppResponse('Success', null, 200).sendResponse(res);
    },
};

async function init(){
    if(!PaymentMethod){
        let models = await require('../models');
        PaymentMethod = models.PaymentMethod;
        Subscription = models.Subscription;
        User = models.User;
        UserSettings = models.UserSettings;
    }
}