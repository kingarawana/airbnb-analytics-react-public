var genericSchemaMixin = require('./schemaMixinGeneric');

module.exports.mix = function(modelSchema){
    genericSchemaMixin.mix(modelSchema);
    /**
     * Finds all instances by user id.
     *
     * @param {String} userId
     * @return {Promise} List of instances. Empty if none.
     */
    modelSchema.statics.findAllByUserIdPromise = function(userId, archived=false) {
        return new Promise((fulfill, reject)=>{
            this.find({userId: userId, archived: archived}).exec(function(err, instances) {
                if(err){
                    return reject(err);
                }else{
                    fulfill(instances)
                }
            });
        });
    };

    modelSchema.statics.updateArchiveAllByUserIdPromise = function(userId, shouldArchive) {
        return this.updateAllPromise({userId: userId}, {$set: {archived: shouldArchive}});
    };

    /**
     * Deletes all by user id.
     *
     * @param {String} userId
     * @return {Promise} list of all instnaces deleted.
     */
    modelSchema.statics.deleteAllByUserIdPromise = function(userId) {
        return new Promise((fulfill, reject)=>{
            this.remove({userId: userId}, err => {
                if(err){
                    reject(err);
                }else{
                    fulfill();
                }
            });
        });
    };
};