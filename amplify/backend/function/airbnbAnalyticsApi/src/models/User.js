let mongoose = require('mongoose');
let Schema = mongoose.Schema;
var schemaMixin = require('./schemaMixin');

const ModelSchema = new Schema({
    // the default id that cognito/user pool provides.
    cognitoId: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    name: String,
    paymentCustomerId: String,
    archived: {
        type: Boolean,
        default: false
    },
    creationDate: {
        type: Number,
        default: model => Math.floor(Date.now() / 86400000)
    }
});

schemaMixin.mix(ModelSchema);

ModelSchema.statics.findOneOrCreatePromise = async function(userData){
    let user = this.findOneByIdPromise(userData.userId);
    if(!user){
        user = await (new this(userData)).savePromise();
    }
    return user;
};

ModelSchema.statics.findOneByCognitoIdPromise = async function(cognitoId, fieldSelectString=''){
    return this.findOnePromise({cognitoId}, fieldSelectString);
};

ModelSchema.methods.cleanForResp = function(){
    let doc = this.toObject();
    if(doc.paymentCustomerId){
        doc.paymentCustomerId = doc.paymentCustomerId.substr(0, 4) + '****';
    }
    return doc;
};

module.exports.init = function(conn){
    return conn.model('User', ModelSchema);
};