let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let schemaMixin = require('./schemaMixin');
let Package = require('../classes/Packages');
let moment = require('moment');

const SUB_PERIOD_IN_MILLI = 2592000000; // 30 DAYS

var ModelSchema = new Schema({
    paymentMethodId:  {
        type: String,
        required: true
    },
    userId:  {
        type: String,
        required: true
    },
    externalSubscriptionId:  { // this is the id from the 3rd party service.
        type: String,
        required: true
    },
    subscriptionPackageId:  {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        // required: true // not making it required for now.
    },
    trialStartDate: {
        type: Number,
        default: _ => Date.now()
    },
    /**
     * User trial end date to figure out when the next payment date is.
     */
    trialEndDate: {
        type: Number,
        default: _ => Date.now()
    },
    dateCancelled: { // date when customer cancels
        type: Number
    },
    updateDate: {
        type: Number,
        default: 0
    },
    archived: {
        type: Boolean,
        default: false
    },
    creationDate: { // i don't see myself needed createdAt in the near future. leaving it for now.
        type: Number,
        default: _ => Date.now()
    }
});

schemaMixin.mix(ModelSchema);

ModelSchema.statics.findOneByUserIdPromise = function(userId, active=true) {
    return new Promise((fulfill, reject)=>{
        let q = {userId: userId};
        if(active !== null){
            q.archived = !active;
        }
        this.findOne(q).exec(function(err, pm) {
            if (err) {
                return reject(err);
            }
            fulfill(pm);
        });
    });
};

ModelSchema.statics.findSubscriptionsByUserIdPromise = function(userId, active=true, sortField, isAscending) {
    return new Promise((fulfill, reject)=>{
        var q = this.find({userId: userId, archived: !active});
        if(sortField){
            var s = {}
            s[sortField] = isAscending ? 1 : -1;
            q.sort(s);
        }
        q.exec(function(err, pms) {
            if (err) {
                return reject(err);
            }
            fulfill(pms);
        });
    });
};

ModelSchema.statics.getNumberOfConnectionsForPackage = function(packageId) {
    return getNumberOfConnectionsForPackage(packageId)
};

ModelSchema.statics.getLastActiveSubscriptionByUserId = function(userId) {
    return new Promise((fulfill, reject)=>{
        this.findSubscriptionsByUserIdPromise(userId, false, 'dateCancelled', false).then(subs => {
            if(subs.length == 0){
                fulfill(null);
            }else{
                fulfill(subs[0]);
            }
        });
    });
}

ModelSchema.statics.getLastActiveSubscription = function(subs) {
    subs.sort((a, b)=>{
        var val1 = a.dateCancelled;
        var val2 = b.dateCancelled;
        if(val1 > val2){
            return 1;
        }else if(val1 < val2){
            return -1
        }else{
            return 0;
        }
    });
};

/**
 * Transitions the subscription into a cancelled state.
 */
ModelSchema.methods.cancel = function() {
    this.dateCancelled = Date.now();
    this.archived = true;
};

// ModelSchema.methods.getNextPaymentDate = function(now = Date.now()) {
//     if(now <= this.trialEndDate){
//         return this.trialEndDate;
//     }
//     let passedTime = now - this.trialEndDate;
//     let passedIntervals = Math.floor(passedTime/SUB_PERIOD_IN_MILLI);
//     let lastPaymentTime = this.trialEndDate + (passedIntervals * SUB_PERIOD_IN_MILLI);
//     // let nextPaymentTime = lastPaymentTime + SUB_PERIOD_IN_MILLI;
//     // return Math.floor(nextPaymentTime / 86400000)*86400000
//     return moment(lastPaymentTime).add(30, 'days').startOf('day').valueOf();
// };

ModelSchema.methods.getNextPaymentDate = function(now = Date.now()) {
    if(now <= this.trialEndDate){
        return this.trialEndDate;
    }
    let trial = moment(this.trialEndDate);
    now = moment(now);

    let trialDate = trial.get('date');
    let nowDate = now.get('date');

    let trialMonth = trial.get('month');
    let nowMonth = now.get('month');

    let result = null;
    if(trial.isLeapYear() && !now.isLeapYear() && trialMonth === 1 && trialDate === 29){
        trialDate = 28
    }
    if(nowDate < trialDate){
        result = now.set('date', trialDate);
    }else if(nowDate > trialDate){
        result = now.set('date', trialDate).add(1, 'month');
    }else if(nowDate === trialDate && trialMonth === nowMonth){
        result = now;
    } else{
        result = now.set('date', trialDate)
    }
    return result.valueOf();
};


ModelSchema.methods.setPrice = function(packageId) {
    if(packageId){
        this.price = Package.getPriceForPackage(packageId);
    }else{
        this.price = Package.getPriceForPackage(this.subscriptionPackageId);
    }
};

module.exports.init = function(conn){
    return conn.model('Subscription', ModelSchema);
};
