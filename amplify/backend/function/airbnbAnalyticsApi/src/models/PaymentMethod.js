let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let schemaMixin = require('./schemaMixin');

const ModelSchema = new Schema({
    userId: {
        type: String,
        required: true
    },
    /**
     * this should also be required. it'll need to update tests when making this change.
     */
    cardId: String,
    tokenId: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    name: { // name on card
        type: String,
    },
    cardBrand: String,
    cardBrandId: Number, // this is my internal id Visa = 0, Amex = 1, Master Card = 2, Discover = 3
    last4: String,
    addressZip: String,
    expireMonth: {
        type: Number,
        default: 0
    },
    expireYear: {
        type: Number,
        default: 0
    },
    fundType: String, // credit, etc..
    lastTimeWasDefault: {
        type: Number,
        default: 0
    },
    archived: {
        type: Boolean,
        default: false
    },
    creationDate: {
        type: Number,
        default: model => Date.now()
    }
});

schemaMixin.mix(ModelSchema);

ModelSchema.statics.findOneByUserIdPromise = function(userId) {
    return new Promise((fulfill, reject)=>{
        this.findOne({userId: userId}).exec(function(err, pm) {
            if (err) {
                return reject(err);
            }
            fulfill(pm);
        });
    });
};

ModelSchema.statics.findOneByUserIdAndTokenIdPromise = function(userId, tokenId) {
    return new Promise((fulfill, reject)=>{
        this.findOne({userId: userId, tokenId: tokenId}).exec(function(err, pm) {
            if (err) {
                return reject(err);
            }
            fulfill(pm);
        });
    });
};

ModelSchema.statics.findOneByUserIdAndIdPromise = function(userId, pmId) {
    return new Promise((fulfill, reject)=>{
        this.findOne({userId: userId, _id: pmId}).exec(function(err, pm) {
            if (err) {
                return reject(err);
            }
            fulfill(pm);
        });
    });
};

ModelSchema.statics.findOneByTokenIdPromise = function(tokenId) {
    return new Promise((fulfill, reject)=>{
        this.findOne({tokenId: tokenId}).exec(function(err, pm) {
            if (err) {
                return reject(err);
            }
            fulfill(pm);
        });
    });
};

ModelSchema.statics.existsByTokenIdPromise = function(tokenId) {
    return new Promise((fulfill, reject)=>{
        this.count({tokenId: tokenId}).exec(function(err, count) {
            if (err) {
                return reject(err);
            }
            fulfill(count > 0);
        });
    });
};

// NOT used. will likely delete. If i do end up useing it. delete this comment.
ModelSchema.statics.setAsDefaultPaymentById = function(paymentMethodId){
    this.updateAllPromise({_id: paymentMethodId}, {lastTimeWasDefault: Date.now()});
};

ModelSchema.statics.getDefaultPaymenetMethodForUserPromise = function(userId){
    return new Promise((fulfill, reject)=>{
        this.find({userId: userId, archived: false}).sort([['lastTimeWasDefault', -1]]).limit(1).exec(function(err, instances) {
            if(err) return reject(err);
            if(instances.length > 0){
                return fulfill(instances[0]);
            }
            fulfill(null);
        });
    });
};

ModelSchema.methods.setAsDefaultPayment = function(){
    this.lastTimeWasDefault = Date.now();
};

ModelSchema.methods.clone = function(){
    var obj = this.toObject();
    return new Model(obj);
};

let Model;
module.exports.init = function(conn){
    Model = conn.model('PaymentMethod', ModelSchema);
    return Model;
};