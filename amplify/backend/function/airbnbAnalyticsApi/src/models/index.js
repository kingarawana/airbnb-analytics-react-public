let config = require('../config');
let mongoose = require('mongoose');
let fs = require('fs');
let connection = null;
let models = {};

mongoose.set('useCreateIndex', true);

module.exports = (async function(){
    if(connection) return models;
    try{
        connection = await mongoose.createConnection(config.mongo.host, {
            user: config.mongo.username,
            pass: config.mongo.pass,
            useNewUrlParser: true,
            useUnifiedTopology: true,
            bufferCommands: false,
            bufferMaxEntries: 0
        });

        function errorHandler(){
            connection.close(function () {
                process.exit(0);
            });
        }

        process.on('SIGINT', errorHandler);
        process.on('SIGTERM', errorHandler);
        process.on('SIGHUP', errorHandler);

        connection.on('close', function(){
            process.removeListener('SIGINT', errorHandler);
            process.removeListener('SIGTERM', errorHandler);
            process.removeListener('SIGHUP', errorHandler);
        });

        let fileNames = await getModelNames();
        fileNames.forEach(fileName => {
            models[fileName] = require(`./${fileName}`).init(connection);
        });
        return models;
    }catch(err){
        console.log('the err', err);
    }
})();

async function getModelNames(){
    let files = await getFilesInDir(__dirname);
    return files.map(fileName => {
        let letter = fileName[0];
        if(letter === letter.toLowerCase()) return null;
        return fileName.slice(0, -3);
    }).filter(f => f);
}

async function getFilesInDir(dir){
    return new Promise((fulfill, reject)=>{
        fs.readdir(dir, function (err, files) {
            if (err) {
                return reject(err);
            }
            fulfill(files);
        });
    });
}