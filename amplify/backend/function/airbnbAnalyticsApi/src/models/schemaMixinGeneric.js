var util = require('../helpers/util');
var AppError = require('../helpers/AppError');
var _ = require('lodash');
var ObjectId = require('mongoose').Types.ObjectId;

module.exports.mix = function(modelSchema){
    modelSchema.statics.updateAllPromise = function(q, update) {
        var u;
        if(update.$set){
            u = update;
        }else{
            u = {$set: update};
        }
        u.$set.updateDate = Date.now();
        return new Promise((fulfill, reject)=>{
            this.update(q, u, {multi: true}, err => {
                if(err){
                    reject(err);
                }else{
                    fulfill();
                }
            })
        });
    };

    modelSchema.statics.updateAllObjsPromise = async function(objs, uniqueKey, multi=false){
        if(!uniqueKey) throw new Error(`uniqueKey param required`);
        for(let i = 0, len = objs.length; i < len; i++){
            let obj = objs[i];
            let uniqueValue = obj[uniqueKey];
            if(!uniqueValue) throw new Error(`${uniqueKey} had no value. a value is required for updates`);
            delete obj[uniqueKey]; // deleting since there's no point in updating the key I'm searching by.
            let q = {};
            q[uniqueKey] = uniqueValue;
            let handler = err => {
                throw err;
            };
            if(multi){
                await this.updateAllPromise(q, obj).catch(handler);
            }else{
                await this.updateOnePromise(q, obj).catch(handler);
            }
        }
    };

    modelSchema.statics.updateAllObjsWithQueryPromise = async function(objs, uniqueKey, extraQuery, multi=false){
        if(!uniqueKey) throw new Error(`uniqueKey param required`);
        for(let i = 0, len = objs.length; i < len; i++){
            let obj = objs[i];
            let uniqueValue = obj[uniqueKey];
            if(!uniqueValue) throw new Error(`${uniqueKey} had no value. a value is required for updates`);
            delete obj[uniqueKey]; // deleting since there's no point in updating the key I'm searching by.
            let q = _.cloneDeep(extraQuery);
            q[uniqueKey] = uniqueValue;
            let handler = err => {
                throw err;
            };
            if(multi){
                await this.updateAllPromise(q, obj).catch(handler);
            }else{
                await this.updateOnePromise(q, obj).catch(handler);
            }
        }
    };

    modelSchema.statics.updateOnePromise = function(q, update) {
        var u;
        if(update.$set){
            u = update;
        }else{
            u = {$set: update};
        }
        u.$set.updateDate = Date.now();
        return new Promise((fulfill, reject)=>{
            this.updateOne(q, u, (err, doc) => {
                if(err){
                    reject(err);
                }else{
                    fulfill(doc);
                }
            })
        });
    };

    /**
     * Eacb object in objs must contain the `_id` field. This method will
     * only update the values passed in. Any fields not mentioend will not be
     * updated.
     */
    modelSchema.statics.updateEachByIdPromise = async function(objs) {
        for(let i = 0; i < objs.length; i++){
            let obj = objs[i];
            if(obj._id){
                let q = {_id: new ObjectId(obj._id)};
                delete obj._id;
                await this.updateOnePromise(q, {$set: obj});
            }
        }
    };

    /**
     * Creates and saves the objects passed in (literals or instances).
     *
     * @param {Object} List of objects representing model.
     * @return {Promise} List of instances that have been saved.
     */
    modelSchema.statics.createPromise = async function(instances) {
        instances = util.convertLiteralsToInstances(instances, this);
        for(var i = 0; i < instances.length; i++){
            try{
                await instances[i].savePromise();
            }catch(err){
                if(err.code === 11000){
                    throw new AppError('Already exists', 400, AppError.codes.DUPLICATE_DATA);
                }else{
                    throw new AppError('Server error', 400, AppError.codes.APP_ERROR);
                }
            }
        }
        return instances;
    };

    /**
     * Updates and saves the objects passed in (literals or instances).
     *
     * @param {Object} List of objects representing model.
     */
    modelSchema.statics.updatePromise = async function(objs) {
        for(var i = 0; i < objs.length; i++){
            await new Promise((fulfill, reject)=>{
                let instance = _.cloneDeep(objs[i]);
                var _id = instance._id;
                delete instance._id;
                instance.updateDate = Date.now();
                this.update({_id: _id}, instance, err =>{
                    if(err){
                        reject(err);
                    }else{
                        fulfill();
                    }
                });
            }).catch(err => {
                throw err;
            });
        }
    };

    /**
     * Similar to @updatePromise but more efficient.
     *
     * It executes a single command.
     *
     * @param {Object} List of objects representing model.
     */
    modelSchema.statics.updateBulkPromise = async function(q, update) {
        // var self = this;
        await new Promise((fulfill, reject)=>{
            this.updateMany(q, update, function(err, docs){
                if(err){
                    return reject(err);
                }else{
                    return fulfill(docs);
                }
            })
        })
    };

    /**
     * In the future, this should replace the current implementation of `updatePromise`.
     * Not sure why I didn't create it as an upsert to begin with. Leaving it as is
     * for now, so I don't accidentally break something.
     *
     * @param {Object} List of objects representing model.
     */
    modelSchema.statics.upsertPromise = async function(objs) {
        for(let i = 0; i < objs.length; i++){
            await new Promise((fulfill, reject)=>{
                let obj = _.cloneDeep(objs[i]);
                let _id = obj._id;
                delete obj._id;
                obj.updateDate = Date.now();
                this.update({_id: _id}, {'$set': obj}, (err, numAffected)=>{
                    if(err){
                        reject(err);
                    }else{
                        fulfill();
                    }
                });
            }).catch(err => {
                throw err;
            });
        }
    };

    /**
     * Deletes stored instance by mongo id.
     *
     * @param {String} mongo id of instnace to delete.
     * @return {Promise}
     */
    modelSchema.statics.deleteByIdPromise = function(mId) {
        return new Promise((fulfill, reject)=>{
            this.remove({_id: util.toObjectId(mId)}, err => {
                if(err){
                    reject(err);
                }else{
                    fulfill();
                }
            });
        });
    };

    /**
     * Deletes all instances that match a given query.
     *
     * Use with caution!
     */
    modelSchema.statics.deleteAllByQueryPromise = function(q={}) {
        if(q){ // just to be safe
            return new Promise((fulfill, reject)=>{
                this.remove(q, err => {
                    if(err){
                        reject(err);
                    }else{
                        fulfill();
                    }
                });
            });
        }
    };

    /**
     * Finds an instance by id.
     *
     * @param {String} id
     * @param {String} fieldSelectString - a string specifying what fields to return.
     *        e.g. '+field1 +field2'
     * @return {Promise} list of all instnaces deleted.
     */
    modelSchema.statics.findOneByIdPromise = function(id, fieldSelectString = '') {
        return new Promise((fulfill, reject)=>{
            this.findOne({_id: id}).select(fieldSelectString).exec(function(err, acct) {
                if (err) {
                    return reject(err);
                }
                fulfill(acct);
            });
        });
    };

    modelSchema.statics.findOnePromise = function(q={}, fieldSelectString = '') {
        return new Promise((fulfill, reject)=>{
            this.findOne(q).select(fieldSelectString).exec(function(err, acct) {
                if (err) {
                    return reject(err);
                }
                fulfill(acct);
            });
        });
    };

    modelSchema.statics.findAllPromise = function(q={}, fieldSelectString=''){
        return new Promise((fulfill, reject)=>{
            this.find(q).select(fieldSelectString).exec((err, all) => {
                if(err){
                    reject(err);
                }else{
                    fulfill(all);
                }
            });
        });
    };

    modelSchema.statics.findAllSortLimitPromise = function(query, sort={}, limit=1){
        return new Promise((fulfill, reject)=>{
            this.find(query).sort(sort).limit(limit).exec((err, all) => {
                if(err){
                    reject(err);
                }else{
                    fulfill(all);
                }
            });
        });
    };

    modelSchema.statics.findOneSortLimitPromise = function(query, sort={}, limit=1){
        return new Promise((fulfill, reject)=>{
            this.findOne(query).sort(sort).limit(limit).exec((err, one) => {
                if(err){
                    reject(err);
                }else{
                    fulfill(one);
                }
            });
        });
    };

    modelSchema.statics.findOneAndUpdateSortLimitPromise = function(query, update, sort={}, fieldSelectString=''){
        return new Promise((fulfill, reject)=>{
            this.findOneAndUpdate(query, update, {projection: fieldSelectString}).sort(sort).exec((err, one) => {
                if(err){
                    reject(err);
                }else{
                    fulfill(one);
                }
            });
        });
    };

    /**
     * Returns the matched instances while maintaining the sort order of the original query.
     */
    modelSchema.statics.findAllByIdsSortedPromise = async function(ids){
        let instances = await this.findAllByIdsPromise(ids);
        let mapping = {};
        instances.forEach(i => {
            mapping[i.getIdString()] = i;
        });
        let ordered = [];
        ids.forEach(id => {
            ordered.push(mapping[id]);
        });
        return ordered;
    };

    modelSchema.statics.findAllByIdsPromise = function(ids){
        ids = ids.map(id => util.toObjectId(id));
        return this.findAllPromise({_id: {$in: ids}});
    };

    modelSchema.statics.deleteAllByIds = function(ids){
        if(!ids) return;
        ids = ids.map(id => util.toObjectId(id));
        return this.remove({_id: {$in: ids}});
    };

    modelSchema.statics.removePromise = function(q={}){
        return new Promise((fulfill, reject)=>{
            this.deleteMany(q).exec(function(err) {
                if(err){
                    reject(err);
                }else{
                    fulfill();
                }
            });
        });
    };

    /**
     * Saves the current instance and returns Promise.
     *
     * @return {Promise} Empty promise.
     */
    modelSchema.methods.savePromise = function() {
        this.updateDate = Date.now();
        if(this._id == null){
            this._id = undefined; // new mongoose doesn't set _id if it's null, but i want it to.
        }
        return new Promise((fulfill, reject)=>{
            this.save((err, instance) => {
                if(err){
                    reject(err);
                }else{
                    fulfill(instance);
                }
            });
        });
    };

    modelSchema.methods.setPropertiesFromDict = function(obj) {
        // any keys that aren't defined, won't get saved.
        for(let key in obj){
            this[key] = obj[key];
        }
    };

    /**
     * Saves the current instance and returns Promise.
     * NOTE: doesn't seem to work.
     *
     * @return {Promise} Empty promise.
     */
    modelSchema.methods.updatePromise = function() {
        return new Promise((fulfill, reject)=>{
            this.update((err, doc) => {
                if(err){
                    reject(err);
                }else{
                    fulfill(doc);
                }
            });
        });
    };

    modelSchema.methods.removePromise = function(){
        // didn't know about this method back then
        return this.deleteOne();
    };

    modelSchema.methods.getIdString = function() {
        if(this._id){
            return this._id.toString();
        }else{
            return null;
        }
    };

    /**
     * @returns newly cloned instances in the same order as the given ids.
     */
    modelSchema.statics.cloneByIdsPromise = async function(ids, overwriteProps={}) {
        let instances = await this.findAllByIdsPromise(ids);

        let mapping = {};
        instances.forEach(i => {
            let newId = ObjectId();
            mapping[i._id.toString()] = newId;
            i._id = newId;
            for(let key in overwriteProps){
                i[key] = overwriteProps[key];
            }
        })
        let newInstances = await this.insertPromise(instances);
        let newInstanceMapping = {};
        newInstances.forEach(i => {
            newInstanceMapping[i._id.toString()] = i;

        });

        let newInstancesOrdered = [];
        // console.log('newInstanceMapping', newInstanceMapping);

        ids.forEach(id => {
            newInstancesOrdered.push(newInstanceMapping[mapping[id]]);
        });
        return newInstancesOrdered;
    };

    modelSchema.statics.insertPromise = function(objs){
        return new Promise((fulfill, reject)=>{
            this.insertMany(objs, function(err, instances) {
                if(err){
                    reject(err);
                }else{
                    fulfill(instances);
                }
            });
        });
    };

    modelSchema.methods.deletePromise = function() {
        return new Promise((fulfill, reject)=>{
            this.remove(err => {
                if(err){
                    reject(err);
                }else{
                    fulfill();
                }
            });
        });
    };

    modelSchema.methods.toObj = function() {
        let obj = this.toObject();

        return obj;
    };

    modelSchema.methods.archiveAndSavePromise = function() {
        this.archived = true;
        return this.savePromise();
    };

    modelSchema.statics.archiveByIdPromise = async function(id) {
        return await this.updateAllPromise({_id: util.toObjectId(id)}, {archived: true});
    };

    modelSchema.statics.unArchiveByIdPromise = async function(id) {
        return await this.updateAllPromise({_id: util.toObjectId(id)}, {archived: false});
    };

    modelSchema.statics.existsPromise = function(q){
        return new Promise((fulfill, reject)=>{
            this.exists(q, function(err, exists){
                if(err){
                    reject(err);
                }else{
                    fulfill(exists);
                }
            });
        });
    };

    modelSchema.statics.countPromise = function(q){
        return new Promise((fulfill, reject)=>{
            this.countDocuments(q, function(err, count){
                if(err){
                    reject(err);
                }else{
                    fulfill(count);
                }
            });
        });
    }
};
