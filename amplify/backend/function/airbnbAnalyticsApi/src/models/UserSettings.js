let mongoose = require('mongoose');
let Schema = mongoose.Schema;
var schemaMixin = require('./schemaMixin');

const ModelSchema = new Schema({
    userId: {
        type: String,
        required: true,
    },
    updateDate: {
        type: Number,
        default: 0
    },
    archived: { // do i really need to archive this?
        type: Boolean,
        default: false
    },
    creationDate: { // i don't see myself needed createdAt in the near future. leaving it for now.
        type: Number,
        default: _ => Date.now()
    }
});

schemaMixin.mix(ModelSchema);

module.exports.init = function(conn){
    return conn.model('UserSettings', ModelSchema);
};