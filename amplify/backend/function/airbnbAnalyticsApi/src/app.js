const express = require('express');
const bodyParser = require('body-parser');
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');
const paymentController = require('./controllers/paymentController');
const subscriptionController = require('./controllers/subscriptionController');
const userAccountController = require('./controllers/userAccountController');
const devController = require('./controllers/devController');
const listingController = require('./controllers/listingController');
const authMiddleware = require('./middleware/authentication');
const config = require('./config');

var app = express();
app.use(bodyParser.json());
app.use(awsServerlessExpressMiddleware.eventContext());

// Enable CORS for all methods
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'PUT,POST,GET,DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-apigateway-context, x-apigateway-event");
  next();
});

app.use(function(req, res, next){
  req.resources = req.resources || {};
  res.locals = res.locals || {};
  next();
});

/**********************
 * Payment *
 **********************/

app.post('/users/:userId/payment/method', authMiddleware.ensureUserExists, paymentController.createPaymentMethod);
app.put('/users/:userId/payment/method', authMiddleware.ensureUserExists, paymentController.addAndSetDefaultPaymentMethod);
app.get('/users/:userId/payment/method', authMiddleware.ensureUserExists, paymentController.getPaymentMethods);
app.put('/users/:userId/payment/method/:modelId/primary', authMiddleware.ensureUserExists, paymentController.setAsPrimaryPaymentMethod);
app.put('/users/:userId/payment/method/:modelId', authMiddleware.ensureUserExists, paymentController.updatePaymentMethod);
app.delete('/users/:userId/payment/method/:modelId', authMiddleware.ensureUserExists, paymentController.deletePaymentMethod);

/**********************
 * Subscription *
 **********************/

app.post('/users/:userId/subscription/:packageId/payment/:paymentId', authMiddleware.ensureUserExists, subscriptionController.startSubscription);
app.post('/users/:userId/subscription/:packageId', authMiddleware.ensureUserExists, subscriptionController.startSubscription);
app.put('/users/:userId/subscription/resume', authMiddleware.ensureUserExists, subscriptionController.resumeSubscription);
app.delete('/users/:userId/subscription', authMiddleware.ensureUserExists, subscriptionController.cancelSubscription); // in the future i will need a pause subscription
app.put('/users/:userId/subscription/:packageId', authMiddleware.ensureUserExists, subscriptionController.changeSubscription);
app.get('/users/:userId/subscription/:packageId/proration', authMiddleware.ensureUserExists, subscriptionController.getProrationCost);
app.get('/users/:userId/subscription', authMiddleware.ensureUserExists, subscriptionController.getUserSubscription);
app.get('/users/:userId/subscription/archived', authMiddleware.ensureUserExists, subscriptionController.getUserArchivedSubscriptions);

/**********************
 * User *
 **********************/

app.post('/users', userAccountController.registerUser);
app.get('/users/exists', userAccountController.userExists);
app.get('/users/:userId', userAccountController.getUser);
app.put('/users/:userId', userAccountController.updateUser);
app.get('/users/cognito/:cognitoId', userAccountController.getUserByCognitoId);

/**********************
 * Listing Data *
 **********************/

app.get('/listings/:listingId', listingController.getListingByListingId);
app.get('/listings/:listingId/data', listingController.getListingDataByListingId);

if(process.env.ANB_IS_LOCAL){
  console.log('is local:', process.env.ANB_IS_LOCAL);
  app.delete('/users', devController.deleteUser);
}


if(process.env.ENV !== 'test' && !process.env.ANB_IS_LOCAL){
  app.listen(config.port, function() {
    console.log("App started")
  });
}

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app;
