module.exports = `
We're sorry to see you go, but we've honored your wishes and cancelled your full-feature subscription. You will still be able to use the app as usual. The only difference
is that the app will no longer be able to pull transactions from your bank account. You'll have to enter each transactions manually.\n\n
Please contact us anytime if you have a question or want to restart your subscription.\n\n
- Shorrent Team
`;
