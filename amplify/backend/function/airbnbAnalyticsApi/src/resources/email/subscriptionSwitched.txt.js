module.exports = `
You have succesfully switched your subscription package from {{oldNumConnections}} connection(s) to {{newNumConnections}} connection(s) for \${{newPrice}}.\n\n
Contact us any time with any questions.\n\n
- Shorrent Team
`;
