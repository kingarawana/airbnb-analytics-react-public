module.exports = `
Here is your temporary password. Please login with the temporary password to reset your password\n\n
{{tempPass}}\n\n
Or copy and paste this link into your browser to reset your password.\n\n
{{resetLink}}\n\n
- Shorrent Team
`;
