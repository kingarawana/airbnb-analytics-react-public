module.exports = `
Congratulations your full-featured subscription has started! Once you link your bank accounts, your transactions will be automatically pulled into the app! Categorizing your transactions will be easier than ever!\n\n
TIP: you can use the Rules feature to tell the app how to automatically categorize transactions for you. For example, you can create a "rule" that says, if the transaction name contains the word "gas", categorize it under the "necessities" bucket. Once you create that rule, the app will automatically categorize it for you!\n\n
Contact us any time with any questions. We’re here to help.\n\n
- Shorrent Team
`;
