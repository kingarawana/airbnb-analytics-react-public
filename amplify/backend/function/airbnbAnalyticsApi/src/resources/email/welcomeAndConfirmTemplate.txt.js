module.exports = `
Congratulations on taking control of your finances! We’re here to help support you along the way.\n\n
The entire purpose of this app is to help make managing your finances simple. This app is designed to take advantage of our natural mental bias to get us to automatically do the right thing.\n\n
Feel free to email us any time. We’re here to help.\n\n
Copy and paste this link into your browser to verify your account.\n\n
{{confirmLink}}\n\n
- Shorrent Team
`;
