module.exports = `
Your subscription for {{numConnections}} connection(s) for \${{price}} per month has been successfully resumed.\n\n
Contact us any time with any questions.\n\n
- Shorrent Team
`;
