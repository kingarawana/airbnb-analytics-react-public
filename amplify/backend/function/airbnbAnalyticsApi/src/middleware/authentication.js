const AppError = require('../helpers/AppError');
const util = require("../helpers/util");
const logger = require('../helpers/logger');

module.exports = {
    ensureUserExists: async function(req, res, next) {
        if (req.params.userId != null && !util.isObjectId(req.params.userId) && _getCognitoIdFromReq(req)) {
            return new AppError('Not authorized', 404, AppError.codes.UNAUTHORIZED).sendResponse(res);
        }
        const User = (await require('../models')).User;

        try{
            let user = await User.findOneByIdPromise(req.params.userId);
            if(user){
                req.resources.user = user;
                return next()
            }else{
                return new AppError('Not authorized', 404, AppError.codes.UNAUTHORIZED).sendResponse(res);
            }
        }catch(err){
            logger.error('ensureUserExists failed: ', err);
            return new AppError('App Error', 500, AppError.codes.APP_ERROR).sendResponse(res);
        }
    }
};

/**
 * This should work. Haven't tested it yet though.
 * @param req
 * @returns {null|*}
 * @private
 */
function _getCognitoIdFromReq(req){
    try{
        if(req.headers['x-apigateway-context'] && JSON.parse(req.headers['x-apigateway-context']).context.identity.cognitoIdentityId){
            return JSON.parse(req.headers['x-apigateway-context']).context.identity.cognitoIdentityId;
        }
    }catch(err){
        return null;
    }

}

