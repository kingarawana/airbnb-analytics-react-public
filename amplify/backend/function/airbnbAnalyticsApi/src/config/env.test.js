const PORT = 2999;
module.exports = {
    baseUrl: 'http://localhost:'+PORT,
    port: PORT,
    mongo:{
        host: 'mongodb://localhost:27017/airbnbanalytics_test',
        username: 'airbnbanalytics_test',
        pass: 'test_pw'
    },
    stripe: {
        publishableKey: process.env.ANB_STRIPE_PUBLISHABLE_KEY,
        secretKey: process.env.ANB_STRIPE_SECRETE_KEY,
    },
    logger: {
        logLevel: 'error' // comma separated list, debug,info,warn,error
    },
    email: {
        username: "support@teekr.com",
        pass: process.env.ANB_E_P,
        mailGun: {
            apiKey: process.env.ANB_MG_API_KEY
        }
    },
};