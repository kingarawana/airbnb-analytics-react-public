let env = process.env.ENV || 'dev';
console.log('The ENV', env);
if(env === 'test-it'){
    env = 'test';
}
const config = require(`./env.${env}`);

// I don't think I even need these
// config.email.pass = process.env.ANB_E_P;
// config.email.mailGun.apiKey = process.env.ANB_MG_API_KEY;

module.exports = config;