module.exports = {
    port: 3001,
    mongo:{
        host: 'mongodb://52.11.140.29:27017/airbnbanalytics_dev',
        username: 'airbnbanalytics_dev',
        pass: process.env.ANB_M_P
    },
    stripe: {
        publishableKey: process.env.ANB_STRIPE_PUBLISHABLE_KEY,
        secretKey: process.env.ANB_STRIPE_SECRETE_KEY,
    },
    logger: {
        logLevel: 'error' // comma separated list, debug,info,warn,error
    },
    email: {
        username: "support@bucketsofmoneyapp.com",
        pass: process.env.ANB_E_P,
        mailGun: {
            apiKey: process.env.ANB_MG_API_KEY
        }
    },
};