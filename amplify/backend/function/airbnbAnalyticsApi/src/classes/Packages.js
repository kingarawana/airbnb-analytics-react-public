const _  = require('lodash');
const packages = [
    {
        id: 'anb-free-monthly-plan', // this free one is not really used since free is teh default. Just here for completeness. F
        monthlyPrice: 0,
    },
    {
        id: 'plan_GtMD6yZoMpd63J',
        monthlyPrice: 19.99,
    },
    {
        id: 'plan_GtMDu92B8dUuKm',
        monthlyPrice: 99.98,
    },
];

const mPackages = packages.reduce((prev, curr) => {
    prev[curr.id] = curr;
    return prev;
}, {});

module.exports = {
    getPackages: function(){
        return _.cloneDeep(packages);
    },
    isValidPackageId: function (packageId) {
        return packageId in mPackages;
    },
    getPackageById: function(packageId){
        return mPackages[packageId];
    },
    getBasicPackage: function(){
        return mPackages['plan_GtMD6yZoMpd63J'];
    },
    getPremiumPackage: function(){
        return mPackages['plan_GtMDu92B8dUuKm'];
    },
    getPriceForPackage: function(packageId) {
        return mPackages[packageId].price;
    }
};