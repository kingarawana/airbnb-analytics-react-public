const airbnb = require('airbnbapijs-khon');
airbnb.setCurrency('USD');

const service = {
    getListingsByLocation: async function(location, offset=0, limit=15){
        return airbnb.listingSearch2({location: location, offset, limit}).then(resp => {
            return service._convertRawListingResults(resp);
        });
    },
    getListingDetailsByListingId: async function(rentalId) {
        let res = await airbnb.getListingInfo(rentalId);
        return res.listing;
    },
    getCalendarForListingId: function(rentalId, months=2){
        let d = new Date();
        return airbnb.getPublicListingCalendar({
            id: rentalId,
            month: d.getMonth() + 1,
            year: d.getFullYear(),
            count: months
        }).then(res => {
            if(!res) return [];
            res = res.calendar_months.reduce((arr, item) => {
                return arr.concat(item.days);
            }, []);
            let dates = new Set();
            res = res.filter(cal => {
                if(dates.has(cal.date)) return false;
                dates.add(cal.date);
                return true;
            });
            return res;
        });
    },
    /**
     * Converts raw listing results into just raw listings.
     * @param rawResults
     * @private
     */
    _convertRawListingResults: function(rawResp){
        let rawListings = rawResp.explore_tabs[0].sections[0].listings;
        if(!rawListings) return [];
        rawListings = rawListings.map(li => li.listing);
        return rawListings
    }
};

module.exports = service;