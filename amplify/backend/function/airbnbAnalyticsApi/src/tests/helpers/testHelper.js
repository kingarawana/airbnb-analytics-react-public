class Mock {
    constructor(originalFn){
        this.numCalls = 0;
        this.responseVal = null;
        this.args = [];
        this.fn = originalFn;
    }

    returnPromise(val){
        return this.return(Promise.resolve(val));
    }

    return(val){
        this.responseVal = val;
        var self = this;
        var fn = function(){

            self.numCalls++;
            var theArgs = [].slice.call(arguments);
            self.args = self.args.concat(theArgs);

            return self.responseVal;
        };
        fn.mock = this;
        return fn;
    }

    /**
     *
     * @param val = [err, val]
     * @param cb
     * @returns {fn}
     */
    returnCb(val){
        this.responseVal = val;
        var self = this;
        var fn = function(){
            self.numCalls++;
            var theArgs = [].slice.call(arguments);
            let cb = theArgs.pop();
            self.args = self.args.concat(theArgs);
            cb(...val);
        };
        fn.mock = this;
        return fn;
    }

    getNumCalls(){
        return this.numCalls;
    }
    getArgs(index = 0){
        return this.args[index];
    }
    getAllArgs(){
        return this.args;
    }
    resetArgsAndCalls(){
        this.args = [];
        this.numCalls = 0;
    }
}

var testHelper = {
    createMock: function(){
        return new Mock();
    },
    mock: function(obj, fnName, returnValue){
        var m = new Mock(obj[fnName]);
        obj[fnName] = m.mockReturn(returnValue);
    }
};

module.exports = testHelper;
