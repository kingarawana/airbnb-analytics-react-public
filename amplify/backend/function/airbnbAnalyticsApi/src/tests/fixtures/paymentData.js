module.exports = {
    tokenId: 'tok_1BTrnXAEOGPkGCAIzUpr6lJp',
    addressZip: '90026',
    cardId: 'card_1BTrnXAEOGPkGCAIs16tnMYH',
    expireMonth: 4,
    expireYear: 2024,
    last4: '1234',
    cardBrand: 'Visa',
    fundType: 'credit'
};
