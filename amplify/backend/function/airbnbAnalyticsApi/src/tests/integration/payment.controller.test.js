process.env.ENV = 'test';

const chai = require('chai');
const exp = chai.expect;
const axios = require('axios');
const http = require('http');
const config = require('../../config');
const testHelper = require('../helpers/testHelper');
const stripeService = require('../../services/stripeService');
const paymentDataFixture = require('../fixtures/paymentData');
const stripeCardFixture = require('../fixtures/stripeCard');
const stripeCustomer = require('../fixtures/stripeCustomer');

let ax = axios.create({
   baseURL: config.baseUrl,
   headers: {
      'x-apigateway-event': '{}',
      'x-apigateway-context': '{}'
   },
   validateStatus: false
});

describe('Stripe Payment Controller Tests', function(){
   let appServer;
   let models;
   let User;
   let PaymentMethod;
   let user;

   before(async function(){
      app = require('../../app');
      appServer = http.createServer(app);
      await new Promise(fulfill => {
         appServer.listen(config.port, fulfill);
      });
   });

   after(function(done){
      appServer.on('close', async function() {
         done();
      });
      appServer.close();
   });

   beforeEach(async function() {
      models = await require('../../models');
      User = models.User;
      PaymentMethod = models.PaymentMethod;
      user = await new User({cognitoId: '1111', name: 'Test User', email: 'test@email.com'}).savePromise();
   });

   afterEach(async function(){
      await User.removePromise();
      await PaymentMethod.removePromise();
   });

   describe('POST: /users/:userId/payment/method - createPaymentMethod', function(){
      describe("When user does not exist in stripe", function(){
         it('should create user in stripe and add payment method as the default', async function(){
            let start = Date.now();
            let mockStripe = {
               customers: {
                  create: testHelper.createMock().returnCb([null, stripeCustomer]),
                  createSource: testHelper.createMock().returnCb([null, null]),
                  update: testHelper.createMock().returnCb([null, null]),
                  listCards: testHelper.createMock().returnCb([])
                }
            };
            stripeService._setStripe(mockStripe);
            let resp = await ax.post(`/users/${user.getIdString()}/payment/method`, paymentDataFixture);
            let data = resp.data.data;
            exp(data).to.exist;
            exp(data._id).to.exist;
            exp(data.email).to.not.exist;
            exp(data.addressZip).to.not.exist;
            exp(data.cardId).to.not.exist;
            exp(data.tokenId).to.not.exist;
            exp(data.expireMonth).to.equal(paymentDataFixture.expireMonth);
            exp(data.expireYear).to.equal(paymentDataFixture.expireYear);
            exp(data.last4).to.equal(paymentDataFixture.last4+'');
            exp(data.cardBrand).to.equal(paymentDataFixture.cardBrand);
            exp(data.userId).to.equal(user.getIdString());
            exp(data.archived).to.be.false;
            exp(data.lastTimeWasDefault).to.be.greaterThan(start);

            let createMock = mockStripe.customers.create.mock;
            exp(createMock.getNumCalls()).to.equal(1);
            let createStripeCustomer = createMock.getArgs(0);
            exp(createStripeCustomer.email).to.equal(user.email);
            exp(createStripeCustomer.source).to.equal(paymentDataFixture.tokenId);
            exp(createStripeCustomer.metadata.userId).to.equal(user.getIdString());

            exp(mockStripe.customers.createSource.mock.getNumCalls()).to.equal(0);
            exp(mockStripe.customers.update.mock.getNumCalls()).to.equal(0);
            exp(mockStripe.customers.listCards.mock.getNumCalls()).to.equal(0);

            let pm = await PaymentMethod.findOneByUserIdPromise(user.getIdString());
            exp(pm.tokenId).to.equal(paymentDataFixture.tokenId);
            exp(pm.email).to.equal(user.email);
            exp(pm.cardId).to.equal(paymentDataFixture.cardId);
            exp(pm.addressZip).to.equal(paymentDataFixture.addressZip);
            exp(pm.expireMonth).to.equal(paymentDataFixture.expireMonth);
            exp(pm.expireYear).to.equal(paymentDataFixture.expireYear);
            exp(pm.last4).to.equal(paymentDataFixture.last4+'');
            exp(pm.cardBrand).to.equal(paymentDataFixture.cardBrand);
            exp(pm.userId).to.equal(user.getIdString());
            exp(pm.archived).to.be.false;
         });
      });
      describe("When user exist in stripe", function(){
         it('should create user in stripe and add payment method as the default', async function(){
            user.paymentCustomerId = 'stripeCustomerId123';
            await user.savePromise();
            let start = Date.now();
            let mockStripe = {
               customers: {
                  create: testHelper.createMock().returnCb([null, stripeCustomer]),
                  createSource: testHelper.createMock().returnCb([null, stripeCardFixture]),
                  update: testHelper.createMock().returnCb([null, stripeCustomer]),
                  listCards: testHelper.createMock().returnCb([])
               }
            };
            stripeService._setStripe(mockStripe);
            let resp = await ax.post(`/users/${user.getIdString()}/payment/method`, paymentDataFixture);
            let data = resp.data.data;
            exp(data).to.exist;
            exp(data.email).to.not.exist;
            exp(data.addressZip).to.not.exist;
            exp(data.cardId).to.not.exist;
            exp(data.tokenId).to.not.exist;
            exp(data.expireMonth).to.equal(paymentDataFixture.expireMonth);
            exp(data.expireYear).to.equal(paymentDataFixture.expireYear);
            exp(data.last4).to.equal(paymentDataFixture.last4+'');
            exp(data.cardBrand).to.equal(paymentDataFixture.cardBrand);
            exp(data.userId).to.equal(user.getIdString());
            exp(data.archived).to.be.false;
            exp(data.lastTimeWasDefault).to.be.greaterThan(start);

            exp(mockStripe.customers.create.mock.getNumCalls()).to.equal(0);
            exp(mockStripe.customers.listCards.mock.getNumCalls()).to.equal(0);

            let updateMock = mockStripe.customers.update.mock;
            exp(updateMock.getNumCalls()).to.equal(1);
            exp(updateMock.getArgs(0)).to.equal(user.paymentCustomerId);
            exp(updateMock.getArgs(1)).to.deep.equal({default_source: stripeCardFixture.id});

            let createSourceMock = mockStripe.customers.createSource.mock;
            exp(createSourceMock.getNumCalls()).to.equal(1);
            exp(createSourceMock.getArgs(0)).to.equal(user.paymentCustomerId);
            exp(createSourceMock.getArgs(1)).to.deep.equal({source: paymentDataFixture.tokenId});

            let pm = await PaymentMethod.findOneByUserIdPromise(user.getIdString());
            exp(pm.tokenId).to.equal(paymentDataFixture.tokenId);
            exp(pm.email).to.equal(user.email);
            exp(pm.cardId).to.equal(paymentDataFixture.cardId);
            exp(pm.addressZip).to.equal(paymentDataFixture.addressZip);
            exp(pm.expireMonth).to.equal(paymentDataFixture.expireMonth);
            exp(pm.expireYear).to.equal(paymentDataFixture.expireYear);
            exp(pm.last4).to.equal(paymentDataFixture.last4+'');
            exp(pm.cardBrand).to.equal(paymentDataFixture.cardBrand);
            exp(pm.userId).to.equal(user.getIdString());
            exp(pm.archived).to.be.false;
         });
      })
   });

   describe('DELETE: /users/:userId/payment/method/:modelId - deletePaymentMethod', function() {
      describe('When card exists', function () {
         it('should delete existing payment method', async function () {
            user.paymentCustomerId = 'stripeCustomerId123';
            await user.savePromise();
            let mockStripe = {
               customers: {
                  deleteCard: testHelper.createMock().returnCb([null, {deleted: true}])
               }
            };
            stripeService._setStripe(mockStripe);
            let pm = new PaymentMethod();
            pm.userId = user.getIdString();
            pm.cardId = 'card_1234';
            pm.email = user.email;
            pm.tokenId = 'tok_1234';
            await pm.savePromise();
            let resp = await ax.delete(`/users/${user.getIdString()}/payment/method/${pm.getIdString()}`);
            exp(resp.status).to.equal(200);
            let data = resp.data;
            exp(data).to.exist;
            exp(data.httpCode).to.equal(200);
            let pms = await PaymentMethod.findAllPromise();
            exp(pms.length).to.equal(1);
            pm = pms[0];
            exp(pm.archived).to.be.true;

            let delMock = mockStripe.customers.deleteCard.mock;
            exp(delMock.getNumCalls()).to.equal(1);
            exp(delMock.getArgs()).to.equal(user.paymentCustomerId);
            exp(delMock.getArgs(1)).to.equal(pm.cardId);
         })
      });
      describe('When card does not exist', function () {
         it('should return error', async function () {
            user.paymentCustomerId = 'stripeCustomerId123';
            await user.savePromise();
            let mockStripe = {
               customers: {
                  deleteCard: testHelper.createMock().returnCb([null, {deleted: true}])
               }
            };
            stripeService._setStripe(mockStripe);
            let randomId = '5e7231eb7a81557e714f3025';
            let resp = await ax.delete(`/users/${user.getIdString()}/payment/method/${randomId}`);
            exp(resp.status).to.equal(404);
            let data = resp.data;
            exp(data).to.exist;
            exp(data.httpCode).to.equal(404);

            let delMock = mockStripe.customers.deleteCard.mock;
            exp(delMock.getNumCalls()).to.equal(0);
         })
      });
      describe('When stripe call fails', function () {
         it('should delete existing payment method', async function () {
            user.paymentCustomerId = 'stripeCustomerId123';
            await user.savePromise();
            let mockStripe = {
               customers: {
                  deleteCard: testHelper.createMock().returnCb([null, {deleted: false}])
               }
            };
            stripeService._setStripe(mockStripe);
            let pm = new PaymentMethod();
            pm.userId = user.getIdString();
            pm.cardId = 'card_1234';
            pm.email = user.email;
            pm.tokenId = 'tok_1234';
            await pm.savePromise();
            let resp = await ax.delete(`/users/${user.getIdString()}/payment/method/${pm.getIdString()}`);
            exp(resp.status).to.equal(500);
            let data = resp.data;
            exp(data).to.exist;
            exp(data.httpCode).to.equal(500);
            let pms = await PaymentMethod.findAllPromise();
            exp(pms.length).to.equal(1);
            pm = pms[0];
            exp(pm.archived).to.be.false;

            let delMock = mockStripe.customers.deleteCard.mock;
            exp(delMock.getNumCalls()).to.equal(1);
            exp(delMock.getArgs()).to.equal(user.paymentCustomerId);
            exp(delMock.getArgs(1)).to.equal(pm.cardId);
         })
      });
   });

   describe('PUT: /users/:userId/payment/method - addAndSetDefaultPaymentMethod', function(){
      describe('When card exists', function(){
         it('should update existing payment method', async function(){
            user.paymentCustomerId = 'stripeCustomerId123';
            await user.savePromise();
            let start = Date.now();
            let updatedYear = 2030;
            let mockStripe = {
               customers: {
                  create: testHelper.createMock().returnCb([null, stripeCustomer]),
                  createSource: testHelper.createMock().returnCb([null, stripeCardFixture]),
                  update: testHelper.createMock().returnCb([null, null]),
                  listCards: testHelper.createMock().returnCb([])
               }
            };
            stripeService._setStripe(mockStripe);
            let existingPaymentMethod = new PaymentMethod({...paymentDataFixture, email: user.email, userId: user.getIdString()});
            await existingPaymentMethod.savePromise();
            let resp = await ax.put(`/users/${user.getIdString()}/payment/method`, {...paymentDataFixture, expireYear: updatedYear});
            let data = resp.data.data;
            exp(data).to.be.null;

            exp(mockStripe.customers.listCards.mock.getNumCalls()).to.equal(0);
            exp(mockStripe.customers.createSource.mock.getNumCalls()).to.equal(0);
            exp(mockStripe.customers.update.mock.getNumCalls()).to.equal(0);

            let pm = await PaymentMethod.findOneByUserIdPromise(user.getIdString());
            exp(pm.tokenId).to.equal(paymentDataFixture.tokenId);
            exp(pm.email).to.equal(user.email);
            exp(pm.cardId).to.equal(paymentDataFixture.cardId);
            exp(pm.addressZip).to.equal(paymentDataFixture.addressZip);
            exp(pm.expireMonth).to.equal(paymentDataFixture.expireMonth);
            exp(pm.expireYear).to.equal(updatedYear);
            exp(pm.last4).to.equal(paymentDataFixture.last4+'');
            exp(pm.cardBrand).to.equal(paymentDataFixture.cardBrand);
            exp(pm.userId).to.equal(user.getIdString());
            exp(pm.archived).to.be.false;
         });
      });
      describe('When card does not exist', function(){
         it('should add the card to stripe and create a payment method', async function(){
            user.paymentCustomerId = 'stripeCustomerId123';
            await user.savePromise();
            let start = Date.now();
            let updatedYear = 2030;
            let mockStripe = {
               customers: {
                  create: testHelper.createMock().returnCb([null, stripeCustomer]),
                  createSource: testHelper.createMock().returnCb([null, stripeCardFixture]),
                  update: testHelper.createMock().returnCb([null, null]),
                  listCards: testHelper.createMock().returnCb([null, {data: [{...paymentDataFixture, ...stripeCardFixture}]}])
               }
            };
            stripeService._setStripe(mockStripe);
            let resp = await ax.put(`/users/${user.getIdString()}/payment/method`, {...paymentDataFixture, cardId: null, expireYear: updatedYear});
            let data = resp.data.data;
            exp(data).to.be.null;


            let listCardsMock = mockStripe.customers.listCards.mock;
            exp(listCardsMock.getNumCalls()).to.equal(1);
            exp(listCardsMock.getArgs(0)).to.equal(user.paymentCustomerId);

            let createSourceMock = mockStripe.customers.createSource.mock;
            exp(createSourceMock.getNumCalls()).to.equal(1);
            exp(createSourceMock.getArgs(0)).to.equal(user.paymentCustomerId);
            exp(createSourceMock.getArgs(1)).to.deep.equal({ source:  paymentDataFixture.tokenId});

            let updateMock = mockStripe.customers.update.mock;
            exp(updateMock.getNumCalls()).to.equal(1);
            exp(updateMock.getArgs(0)).to.equal(user.paymentCustomerId);
            exp(updateMock.getArgs(1)).to.deep.equal({ default_source: stripeCardFixture.id});

            let pm = await PaymentMethod.findOneByUserIdPromise(user.getIdString());
            exp(pm.tokenId).to.equal(paymentDataFixture.tokenId);
            exp(pm.email).to.equal(user.email);
            exp(pm.cardId).to.equal(paymentDataFixture.cardId);
            exp(pm.addressZip).to.equal(paymentDataFixture.addressZip);
            exp(pm.expireMonth).to.equal(paymentDataFixture.expireMonth);
            exp(pm.expireYear).to.equal(updatedYear);
            exp(pm.last4).to.equal(paymentDataFixture.last4+'');
            exp(pm.cardBrand).to.equal(paymentDataFixture.cardBrand);
            exp(pm.userId).to.equal(user.getIdString());
            exp(pm.lastTimeWasDefault).to.greaterThan(start);
            exp(pm.archived).to.be.false;
         });
      });
   });
   describe('PUT: /users/:userId/payment/method/:modelId/primary - setAsPrimaryPaymentMethod', function(){
      it('should update db and update in stripe api', async function(){
         user.paymentCustomerId = 'stripeCustomerId123';
         await user.savePromise();
         let start = Date.now();
         let updatedYear = 2030;
         let mockStripe = {
            customers: {
               create: testHelper.createMock().returnCb([null, null]),
               createSource: testHelper.createMock().returnCb([null, null]),
               update: testHelper.createMock().returnCb([null, stripeCustomer]),
               listCards: testHelper.createMock().returnCb([null, []])
            }
         };
         stripeService._setStripe(mockStripe);
         let existingPaymentMethod = new PaymentMethod({...paymentDataFixture, email: user.email, userId: user.getIdString()});
         await existingPaymentMethod.savePromise();
         let resp = await ax.put(`/users/${user.getIdString()}/payment/method/${existingPaymentMethod.getIdString()}/primary`);
         let data = resp.data.data;
         exp(data).to.exist;
         exp(data.email).to.not.exist;
         exp(data.addressZip).to.not.exist;
         exp(data.cardId).to.not.exist;
         exp(data.tokenId).to.not.exist;
         exp(data.expireMonth).to.equal(paymentDataFixture.expireMonth);
         exp(data.expireYear).to.equal(paymentDataFixture.expireYear);
         exp(data.last4).to.equal(paymentDataFixture.last4+'');
         exp(data.cardBrand).to.equal(paymentDataFixture.cardBrand);
         exp(data.userId).to.equal(user.getIdString());
         exp(data.archived).to.be.false;
         exp(data.lastTimeWasDefault).to.be.greaterThan(start);

         let updateMock = mockStripe.customers.update.mock;
         exp(updateMock.getNumCalls()).to.equal(1);
         exp(updateMock.getArgs(0)).to.equal(user.paymentCustomerId);
         exp(updateMock.getArgs(1)).to.deep.equal({ default_source: existingPaymentMethod.cardId });

         let pm = await PaymentMethod.findOneByIdPromise(existingPaymentMethod._id);
         exp(pm.lastTimeWasDefault).to.be.greaterThan(existingPaymentMethod.lastTimeWasDefault);

         exp(mockStripe.customers.listCards.mock.getNumCalls()).to.equal(0);
         exp(mockStripe.customers.createSource.mock.getNumCalls()).to.equal(0);
         exp(mockStripe.customers.create.mock.getNumCalls()).to.equal(0);
      });
   });

   describe('PUT: /users/:userId/payment/method/:modelId - updatePaymentMethod', function(){
      describe('updating all possible fields', function(){
         it('should update db', async function(){
            user.paymentCustomerId = 'stripeCustomerId123';
            await user.savePromise();
            let start = Date.now();
            let updatedYear = 2030;
            let existingPaymentMethod = new PaymentMethod({...paymentDataFixture, email: user.email, name: user.name, userId: user.getIdString()});
            await existingPaymentMethod.savePromise();
            let pm = await PaymentMethod.findOneByIdPromise(existingPaymentMethod._id);

            exp(pm).to.exist;
            exp(pm.email).to.equal(user.email);
            exp(pm.name).to.equal(user.name);
            exp(pm.addressZip).to.equal(paymentDataFixture.addressZip);
            exp(pm.cardId).to.equal(paymentDataFixture.cardId);
            exp(pm.tokenId).to.equal(paymentDataFixture.tokenId);
            exp(pm.expireMonth).to.equal(paymentDataFixture.expireMonth);
            exp(pm.expireYear).to.equal(paymentDataFixture.expireYear);
            exp(pm.last4).to.equal(paymentDataFixture.last4+'');
            exp(pm.cardBrand).to.equal(paymentDataFixture.cardBrand);
            exp(pm.userId).to.equal(user.getIdString());
            exp(pm.archived).to.be.false;

            let updates = {name: 'New Name', expireMonth: 11, expireYear: 99, test: 345};


            let resp = await ax.put(`/users/${user.getIdString()}/payment/method/${existingPaymentMethod.getIdString()}`, updates);
            exp(resp.data.httpCode).to.equal(202);

            pm = await PaymentMethod.findOneByIdPromise(existingPaymentMethod._id);
            exp(pm).to.exist;
            exp(pm.email).to.equal(user.email);
            exp(pm.name).to.equal(updates.name);
            exp(pm.addressZip).to.equal(paymentDataFixture.addressZip);
            exp(pm.cardId).to.equal(paymentDataFixture.cardId);
            exp(pm.tokenId).to.equal(paymentDataFixture.tokenId);
            exp(pm.expireMonth).to.equal(updates.expireMonth);
            exp(pm.expireYear).to.equal(updates.expireYear);
            exp(pm.last4).to.equal(paymentDataFixture.last4+'');
            exp(pm.cardBrand).to.equal(paymentDataFixture.cardBrand);
            exp(pm.userId).to.equal(user.getIdString());
            exp(pm.archived).to.be.false;
            exp(pm.test).to.not.exist;
         });
      });
      describe('updating only some fields', function(){
         it('should update db', async function(){
            user.paymentCustomerId = 'stripeCustomerId123';
            await user.savePromise();
            let start = Date.now();
            let updatedYear = 2030;
            let existingPaymentMethod = new PaymentMethod({...paymentDataFixture, email: user.email, name: user.name, userId: user.getIdString()});
            await existingPaymentMethod.savePromise();
            let pm = await PaymentMethod.findOneByIdPromise(existingPaymentMethod._id);

            exp(pm).to.exist;
            exp(pm.email).to.equal(user.email);
            exp(pm.name).to.equal(user.name);
            exp(pm.addressZip).to.equal(paymentDataFixture.addressZip);
            exp(pm.cardId).to.equal(paymentDataFixture.cardId);
            exp(pm.tokenId).to.equal(paymentDataFixture.tokenId);
            exp(pm.expireMonth).to.equal(paymentDataFixture.expireMonth);
            exp(pm.expireYear).to.equal(paymentDataFixture.expireYear);
            exp(pm.last4).to.equal(paymentDataFixture.last4+'');
            exp(pm.cardBrand).to.equal(paymentDataFixture.cardBrand);
            exp(pm.userId).to.equal(user.getIdString());
            exp(pm.archived).to.be.false;

            let updates = {expireMonth: 11, expireYear: 99};


            let resp = await ax.put(`/users/${user.getIdString()}/payment/method/${existingPaymentMethod.getIdString()}`, updates);
            exp(resp.data.httpCode).to.equal(202);

            pm = await PaymentMethod.findOneByIdPromise(existingPaymentMethod._id);
            exp(pm).to.exist;
            exp(pm.email).to.equal(user.email);
            exp(pm.name).to.equal(user.name);
            exp(pm.addressZip).to.equal(paymentDataFixture.addressZip);
            exp(pm.cardId).to.equal(paymentDataFixture.cardId);
            exp(pm.tokenId).to.equal(paymentDataFixture.tokenId);
            exp(pm.expireMonth).to.equal(updates.expireMonth);
            exp(pm.expireYear).to.equal(updates.expireYear);
            exp(pm.last4).to.equal(paymentDataFixture.last4+'');
            exp(pm.cardBrand).to.equal(paymentDataFixture.cardBrand);
            exp(pm.userId).to.equal(user.getIdString());
            exp(pm.archived).to.be.false;
            exp(pm.test).to.not.exist;
         });
      });
      describe('when no updates', function(){
         it('should make no changes', async function(){
            user.paymentCustomerId = 'stripeCustomerId123';
            await user.savePromise();
            let start = Date.now();
            let updatedYear = 2030;
            let existingPaymentMethod = new PaymentMethod({...paymentDataFixture, email: user.email, name: user.name, userId: user.getIdString()});
            await existingPaymentMethod.savePromise();
            let pm = await PaymentMethod.findOneByIdPromise(existingPaymentMethod._id);

            exp(pm).to.exist;
            exp(pm.email).to.equal(user.email);
            exp(pm.name).to.equal(user.name);
            exp(pm.addressZip).to.equal(paymentDataFixture.addressZip);
            exp(pm.cardId).to.equal(paymentDataFixture.cardId);
            exp(pm.tokenId).to.equal(paymentDataFixture.tokenId);
            exp(pm.expireMonth).to.equal(paymentDataFixture.expireMonth);
            exp(pm.expireYear).to.equal(paymentDataFixture.expireYear);
            exp(pm.last4).to.equal(paymentDataFixture.last4+'');
            exp(pm.cardBrand).to.equal(paymentDataFixture.cardBrand);
            exp(pm.userId).to.equal(user.getIdString());
            exp(pm.archived).to.be.false;

            let updates = {};


            let resp = await ax.put(`/users/${user.getIdString()}/payment/method/${existingPaymentMethod.getIdString()}`, updates);
            exp(resp.data.httpCode).to.equal(202);

            pm = await PaymentMethod.findOneByIdPromise(existingPaymentMethod._id);
            exp(pm).to.exist;
            exp(pm.email).to.equal(user.email);
            exp(pm.name).to.equal(user.name);
            exp(pm.addressZip).to.equal(paymentDataFixture.addressZip);
            exp(pm.cardId).to.equal(paymentDataFixture.cardId);
            exp(pm.tokenId).to.equal(paymentDataFixture.tokenId);
            exp(pm.expireMonth).to.equal(paymentDataFixture.expireMonth);
            exp(pm.expireYear).to.equal(paymentDataFixture.expireYear);
            exp(pm.last4).to.equal(paymentDataFixture.last4+'');
            exp(pm.cardBrand).to.equal(paymentDataFixture.cardBrand);
            exp(pm.userId).to.equal(user.getIdString());
            exp(pm.archived).to.be.false;
            exp(pm.test).to.not.exist;
         });
      });
   });

   describe('GET: /users/:userId/payment/method - getPaymentMethods', function(){
      describe('when user has no payment methods', function(){
         it('should return an empty list', async function(){
            let mockStripe = {
               customers: {
                  create: testHelper.createMock().returnCb([null, null]),
                  createSource: testHelper.createMock().returnCb([null, null]),
                  update: testHelper.createMock().returnCb([null, null]),
                  listCards: testHelper.createMock().returnCb([null, []])
               }
            };
            stripeService._setStripe(mockStripe);
            let resp = await ax.get(`/users/${user.getIdString()}/payment/method`);
            let data = resp.data.data;
            exp(data).to.exist;
            exp(data).to.be.empty;
         });
      });
      describe('when user has payment methods', function(){
         it('should return payment methods', async function(){
            let mockStripe = {
               customers: {
                  create: testHelper.createMock().returnCb([null, null]),
                  createSource: testHelper.createMock().returnCb([null, null]),
                  update: testHelper.createMock().returnCb([null, null]),
                  listCards: testHelper.createMock().returnCb([null, []])
               }
            };
            stripeService._setStripe(mockStripe);

            let pm1 = new PaymentMethod({
               ...paymentDataFixture,
               token: 'tok1', cardId: 'cardId1', last4: '1111',
               email: user.email, userId: user.getIdString()});

            let pm2 = new PaymentMethod({
               ...paymentDataFixture,
               token: 'tok2', cardId: 'cardId2', last4: '2222',
               email: user.email, userId: user.getIdString()});

            let pm3 = new PaymentMethod({
               ...paymentDataFixture,
               token: 'tok3', cardId: 'cardId3', last4: '3333',
               email: user.email, userId: user.getIdString()});

            await pm1.savePromise();
            await pm2.savePromise();
            await pm3.savePromise();

            let resp = await ax.get(`/users/${user.getIdString()}/payment/method`);
            let data = resp.data.data;
            exp(data).to.exist;
            exp(data.length).to.equal(3);
         });
      });
   });
});