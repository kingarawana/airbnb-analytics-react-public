process.env.ENV = 'test';

const chai = require('chai');
const exp = chai.expect;
const axios = require('axios');
const http = require('http');
const config = require('../../config');
const testHelper = require('../helpers/testHelper');
const stripeSubscription = require('../fixtures/stripeSubscription');
const stripeCustomer = require('../fixtures/stripeCustomer');
const stripeService = require('../../services/stripeService');
const Packages = require('../../classes/Packages');
const moment = require('moment');
const _ = require('lodash');
const AppError = require('../../helpers/AppError');

let ax = axios.create({
    baseURL: config.baseUrl,
    headers: {
        'x-apigateway-event': '{}',
        'x-apigateway-context': '{}'
    },
    validateStatus: false
});

describe('Stripe Payment Controller Tests', function(){
    let appServer;
    let models;
    let User;
    let PaymentMethod;
    let Subscription;
    let user;

    before(async function(){
        let app = require('../../app');
        appServer = http.createServer(app);
        await new Promise(fulfill => {
            appServer.listen(config.port, fulfill);
        });
    });

    after(function(done){
        appServer.on('close', async function() {
            done();
        });
        appServer.close();
    });

    beforeEach(async function() {
        models = await require('../../models');
        User = models.User;
        PaymentMethod = models.PaymentMethod;
        Subscription = models.Subscription;
        user = await new User({cognitoId: '1111', name: 'Test User', email: 'test@email.com',
            paymentCustomerId: 'cust_pmt_id_1234'
        }).savePromise();
    });

    afterEach(async function(){
        await User.removePromise();
        await PaymentMethod.removePromise();
        await Subscription.removePromise();
    });

    describe('POST: /users/:userId/subscription/:packageId/payment/:paymentId - startSubscription', function(){
        describe('when user never had a subscription', function(){
            it('should create a new subscription for given user', async function(){
                let mockStripe = {
                    subscriptions: {
                        create: testHelper.createMock().returnCb([null, stripeSubscription]),
                    },
                    customers: {
                        update: testHelper.createMock().returnCb([null, stripeCustomer]),
                    }
                };
                stripeService._setStripe(mockStripe);
                let pm = await new PaymentMethod({tokenId: 'tok_123', userId: user.getIdString(), email:user.email, cardId: 'some_card_id'}).savePromise();
                let packageId = Packages.getBasicPackage().id;
                let paymentMethodId = pm.getIdString();
                let count = await Subscription.countPromise();
                exp(count).to.equal(0);
                let resp = await ax.post(`/users/${user.getIdString()}/subscription/${packageId}/payment/${paymentMethodId}`);
                let data = resp.data.data;
                exp(resp.data.httpCode).to.equal(202);
                exp(data).to.be.null;
                let sub = await Subscription.findOneByUserIdPromise(user.getIdString());
                exp(sub).to.exist;
                exp(sub.subscriptionPackageId).to.equal(packageId);
                exp(sub.archived).to.be.false;
                exp(sub.trialEndDate).to.be.at.most(moment().add(30, 'days').valueOf());

                let createSubMock = mockStripe.subscriptions.create.mock;
                exp(createSubMock.getNumCalls()).to.be.equal(1);
                let arg = createSubMock.getArgs(0);
                exp(arg.customer).to.equal(user.paymentCustomerId);
                exp(arg.trial_end).to.equal(sub.trialEndDate/1000); // in seconds
                exp(arg.items.length).to.equal(1);
                exp(arg.items[0]).to.deep.equal({plan: packageId});

                let customerUpdateMock = mockStripe.customers.update.mock;
                exp(customerUpdateMock.getNumCalls()).to.equal(1);
                exp(customerUpdateMock.getArgs(0)).to.equal(user.paymentCustomerId);
                exp(customerUpdateMock.getArgs(1)).to.deep.equal({default_source: pm.cardId});
            });
        });
        describe('when user has a cancelled subscription from before', function(){
            it('should start their subscription immediately', async function(){
                let mockStripe = {
                    subscriptions: {
                        create: testHelper.createMock().returnCb([null, stripeSubscription]),
                    },
                    customers: {
                        update: testHelper.createMock().returnCb([null, stripeCustomer]),
                    }
                };
                stripeService._setStripe(mockStripe);
                let pm = await new PaymentMethod({tokenId: 'tok_123', userId: user.getIdString(), email:user.email, cardId: 'some_card_id'}).savePromise();
                let packageId = Packages.getBasicPackage().id;
                let paymentMethodId = pm.getIdString();
                let count = await Subscription.countPromise();
                exp(count).to.equal(0);
                let archivedSub = await new Subscription({paymentMethodId: 'pmt_id', userId: user.getIdString(), externalSubscriptionId: 'stripe_id', archived: true, subscriptionPackageId: Packages.getPremiumPackage().id}).savePromise();
                archivedSub = await Subscription.findOneByUserIdPromise(user.getIdString(), false);
                exp(archivedSub).to.exist;
                exp(archivedSub.archived).to.be.true;
                let resp = await ax.post(`/users/${user.getIdString()}/subscription/${packageId}/payment/${paymentMethodId}`);
                let data = resp.data.data;
                exp(resp.data.httpCode).to.equal(202);
                exp(data).to.be.null;
                let subs = await Subscription.findAllPromise({userId: user.getIdString(), archived: false});
                exp(subs.length).to.equal(1);
                sub = subs[0];
                exp(sub.subscriptionPackageId).to.equal(packageId);
                exp(sub.archived).to.be.false;
                exp(sub.trialEndDate).to.be.at.most(moment().add(1, 'days').valueOf());
                exp(sub.trialEndDate).to.be.at.least(moment().add(0, 'days').valueOf());

                let createSubMock = mockStripe.subscriptions.create.mock;
                exp(createSubMock.getNumCalls()).to.be.equal(1);
                let arg = createSubMock.getArgs(0);
                exp(arg.customer).to.equal(user.paymentCustomerId);
                exp(arg.trial_end).to.equal(sub.trialEndDate/1000); // in seconds
                exp(arg.items.length).to.equal(1);
                exp(arg.items[0]).to.deep.equal({plan: packageId});

                let customerUpdateMock = mockStripe.customers.update.mock;
                exp(customerUpdateMock.getNumCalls()).to.equal(1);
                exp(customerUpdateMock.getArgs(0)).to.equal(user.paymentCustomerId);
                exp(customerUpdateMock.getArgs(1)).to.deep.equal({default_source: pm.cardId});
            });
        });
        describe('when user has a current active subscription from before', function(){
            it('should start their subscription immediately', async function(){
                delStripeSubscription = _.cloneDeep(stripeSubscription);
                delStripeSubscription.id = delStripeSubscription.id + '_del';
                let mockStripe = {
                    subscriptions: {
                        create: testHelper.createMock().returnCb([null, stripeSubscription]),
                        del: testHelper.createMock().returnCb([null, delStripeSubscription]),
                    },
                    customers: {
                        update: testHelper.createMock().returnCb([null, stripeCustomer]),
                    },
                };
                stripeService._setStripe(mockStripe);
                let pm = await new PaymentMethod({tokenId: 'tok_123', userId: user.getIdString(), email:user.email, cardId: 'some_card_id'}).savePromise();
                let packageId = Packages.getBasicPackage().id;
                let paymentMethodId = pm.getIdString();
                let count = await Subscription.countPromise();
                exp(count).to.equal(0);
                let archivedSub = await new Subscription({paymentMethodId: 'pmt_id', userId: user.getIdString(), externalSubscriptionId: 'stripe_id', subscriptionPackageId: Packages.getBasicPackage().id}).savePromise();
                archivedSub = await Subscription.findOneByUserIdPromise(user.getIdString());
                exp(archivedSub).to.exist;
                exp(archivedSub.archived).to.be.false;
                let resp = await ax.post(`/users/${user.getIdString()}/subscription/${packageId}/payment/${paymentMethodId}`);
                let data = resp.data.data;
                exp(resp.data.httpCode).to.equal(202);
                exp(data).to.be.null;
                let subs = await Subscription.findAllPromise({userId: user.getIdString(), archived: false});
                exp(subs.length).to.equal(1);
                sub = subs[0];
                exp(sub.subscriptionPackageId).to.equal(packageId);
                exp(sub.archived).to.be.false;
                exp(sub.trialEndDate).to.be.at.most(moment().add(1, 'days').valueOf());
                exp(sub.trialEndDate).to.be.at.least(moment().add(0, 'days').valueOf());

                let createSubMock = mockStripe.subscriptions.create.mock;
                exp(createSubMock.getNumCalls()).to.be.equal(1);
                let arg = createSubMock.getArgs(0);
                exp(arg.customer).to.equal(user.paymentCustomerId);
                exp(arg.trial_end).to.equal(sub.trialEndDate/1000); // in seconds
                exp(arg.items.length).to.equal(1);
                exp(arg.items[0]).to.deep.equal({plan: packageId});

                let customerUpdateMock = mockStripe.customers.update.mock;
                exp(customerUpdateMock.getNumCalls()).to.equal(1);
                exp(customerUpdateMock.getArgs(0)).to.equal(user.paymentCustomerId);
                exp(customerUpdateMock.getArgs(1)).to.deep.equal({default_source: pm.cardId});

                let delSubMock = mockStripe.subscriptions.del.mock;
                exp(delSubMock.getNumCalls()).to.be.equal(1);
                arg = delSubMock.getArgs(0);
                exp(arg).to.equal(archivedSub.externalSubscriptionId);
            });
        });
    });

    describe('PUT: /users/:userId/subscription/:packageId - changeSubscription', function(){
        describe('when all params are valid', function(){
            it('should update the subscription', async function(){
                let updatedSub = _.cloneDeep(stripeSubscription);
                updatedSub.id = updatedSub.id + '_updated';
                let mockStripe = {
                    subscriptions: {
                        retrieve: testHelper.createMock().returnPromise({items: {data:[stripeSubscription]}}),
                        update: testHelper.createMock().returnCb([null, updatedSub]),
                    },
                };

                stripeService._setStripe(mockStripe);

                let currentSub = await new Subscription({
                    paymentMethodId: 'pmt_id', userId: user.getIdString(),
                    externalSubscriptionId: 'stripe_id', subscriptionPackageId: Packages.getBasicPackage().id}).savePromise();
                let desiredPackageId = Packages.getPremiumPackage().id;
                let prorationDate = Math.round(moment().subtract(10, 'days').valueOf() / 1000);
                let resp = await ax.put(`/users/${user.getIdString()}/subscription/${desiredPackageId}?t=${prorationDate}`);
                let data = resp.data.data;
                exp(resp.data.httpCode).to.equal(202);
                exp(data).to.be.null;

                // should change subscription to new package id.
                let subs = await Subscription.findAllPromise();
                exp(subs.length).to.equal(1);
                let sub = subs[0];
                exp(sub.userId).to.equal(user.getIdString());
                exp(sub.subscriptionPackageId).to.equal(desiredPackageId);

                // should get the existing sub
                let retrieveMock = mockStripe.subscriptions.retrieve.mock;
                exp(retrieveMock.getNumCalls()).to.equal(1);
                exp(retrieveMock.getArgs(0)).to.equal(currentSub.externalSubscriptionId);

                // should update existing sub
                let updateMock = mockStripe.subscriptions.update.mock;
                exp(updateMock.getNumCalls()).to.equal(1);
                exp(updateMock.getAllArgs().length).to.equal(2);
                exp(updateMock.getArgs(0)).to.equal(sub.externalSubscriptionId);
                exp(updateMock.getArgs(1).proration_date).to.equal(prorationDate);
                let item = updateMock.getArgs(1).items[0];
                exp(item.id).to.equal(stripeSubscription.id);
                exp(item.plan).to.equal(desiredPackageId);
            })
        });
        describe('when time is greater than 30 days', function(){
            it('should return http error', async function(){
                let desiredPackageId = Packages.getPremiumPackage().id;
                let prorationDate = Math.round(moment().subtract(33, 'days').valueOf() / 1000);
                let resp = await ax.put(`/users/${user.getIdString()}/subscription/${desiredPackageId}?t=${prorationDate}`);
                exp(resp.status).to.equal(400);
                let err = resp.data;
                exp(err.httpCode).to.equal(400);
                exp(err.appCode).to.equal(AppError.codes.SUBSCRIPTION_UPDATE_ERROR);
            });
        });
        describe('when time is less than now', function(){
            it('should return http error', async function(){
                let desiredPackageId = Packages.getPremiumPackage().id;
                let prorationDate = Math.round(moment().add(3, 'days').valueOf() / 1000);
                let resp = await ax.put(`/users/${user.getIdString()}/subscription/${desiredPackageId}?t=${prorationDate}`);
                exp(resp.status).to.equal(400);
                let err = resp.data;
                exp(err.httpCode).to.equal(400);
                exp(err.appCode).to.equal(AppError.codes.SUBSCRIPTION_UPDATE_ERROR);
            });
        });
        describe('when invalid time', function(){
            it('should return http error', async function(){
                let desiredPackageId = Packages.getPremiumPackage().id + 'x';
                let prorationDate = Math.round(moment().subtract(23, 'days').valueOf() / 1000);
                let resp = await ax.put(`/users/${user.getIdString()}/subscription/${desiredPackageId}?t=${prorationDate}`);
                exp(resp.status).to.equal(500);
                let err = resp.data;
                exp(err.httpCode).to.equal(500);
                exp(err.appCode).to.equal(AppError.codes.SUBSCRIPTION_ERROR);
            });
        });
    });

    describe('DELETE: /users/:userId/subscription - cancelSubscription', function(){
        describe('when a subscription exists', function(){
            it('should cancel the subscription internally and in stripe', async function(){
                let now = Date.now();
                let mockStripe = {
                    subscriptions: {
                        del: testHelper.createMock().returnCb([null, stripeSubscription]),
                    },
                };

                stripeService._setStripe(mockStripe);

                await new Subscription({
                    paymentMethodId: 'pmt_id', userId: user.getIdString(),
                    externalSubscriptionId: 'stripe_id', subscriptionPackageId: Packages.getBasicPackage().id}).savePromise();
                let resp = await ax.delete(`/users/${user.getIdString()}/subscription`);
                let data = resp.data.data;
                exp(resp.data.httpCode).to.equal(202);
                exp(data).to.be.null;

                // should change subscription to new package id.
                let subs = await Subscription.findAllPromise();
                exp(subs.length).to.equal(1);
                let sub = subs[0];
                exp(sub.userId).to.equal(user.getIdString());
                exp(sub.archived).to.be.true;
                exp(sub.dateCancelled).to.be.greaterThan(now);

                // should get the existing sub
                let delMock = mockStripe.subscriptions.del.mock;
                exp(delMock.getNumCalls()).to.equal(1);
                exp(delMock.getArgs()).to.equal(sub.externalSubscriptionId);
            })
        });
        describe('when user has no subscription', function(){
            it('should return http error', async function(){
                let now = Date.now();
                let mockStripe = {
                    subscriptions: {
                        del: testHelper.createMock().returnCb([null, stripeSubscription]),
                    },
                };

                stripeService._setStripe(mockStripe);

                await new Subscription({
                    paymentMethodId: 'pmt_id', userId: user.getIdString() + 'a',
                    externalSubscriptionId: 'stripe_id', subscriptionPackageId: Packages.getBasicPackage().id}).savePromise();
                let resp = await ax.delete(`/users/${user.getIdString()}/subscription`);
                let data = resp.data.data;
                exp(resp.data.httpCode).to.equal(202);
                exp(data).to.be.null;

                // should change subscription to new package id.
                let subs = await Subscription.findAllPromise();
                exp(subs.length).to.equal(1);
                let sub = subs[0];
                exp(sub.archived).to.be.false;
                exp(sub.dateCancelled).to.be.undefined;

                // should get the existing sub
                let delMock = mockStripe.subscriptions.del.mock;
                exp(delMock.getNumCalls()).to.equal(0);
            })
        });
    });

    describe('PUT: /users/:userId/subscription/resume - resumeSubscription', function(){
        describe('when an old subscription exists', function(){
            it('should re-enable it', async function(){
                let mockStripe = {
                    subscriptions: {
                        create: testHelper.createMock().returnCb([null, stripeSubscription]),
                    },
                };

                stripeService._setStripe(mockStripe);

                let sub = await new Subscription({
                    paymentMethodId: 'pmt_id', userId: user.getIdString(),
                    externalSubscriptionId: 'stripe_id', subscriptionPackageId: Packages.getBasicPackage().id,
                    archived: true, dateCancelled: Date.now()
                }).savePromise();

                let pm = await new PaymentMethod({tokenId: 'tok_123', userId: user.getIdString(), email:user.email, cardId: 'some_card_id'}).savePromise();

                let resp = await ax.put(`/users/${user.getIdString()}/subscription/resume`);
                let data = resp.data.data;
                exp(resp.data.httpCode).to.equal(202);
                exp(data).to.be.null;

                let createMock = mockStripe.subscriptions.create.mock;
                exp(createMock.getNumCalls()).to.equal(1);
                exp(createMock.getAllArgs().length).to.equal(1);
                let arg = createMock.getArgs();
                exp(arg.customer).to.equal(user.paymentCustomerId);
                exp(arg.trial_end).to.equal('now');
                exp(arg.items[0]).to.deep.equal({plan: sub.subscriptionPackageId})

                let updatedSub = await Subscription.findOneByIdPromise(sub.getIdString());
                exp(updatedSub).to.exist;
                exp(updatedSub.archived).to.be.false;
            });
        });
        describe('when subscription already exists and is active', function(){
            it('should throw an error', async function(){
                let mockStripe = {
                    subscriptions: {
                        create: testHelper.createMock().returnCb([null, stripeSubscription]),
                    },
                };

                stripeService._setStripe(mockStripe);

                await new Subscription({
                    paymentMethodId: 'pmt_id', userId: user.getIdString(),
                    externalSubscriptionId: 'stripe_id', subscriptionPackageId: Packages.getBasicPackage().id, dateCancelled: Date.now()
                }).savePromise();

                await new PaymentMethod({tokenId: 'tok_123', userId: user.getIdString(), email:user.email, cardId: 'some_card_id'}).savePromise();

                let resp = await ax.put(`/users/${user.getIdString()}/subscription/resume`);
                exp(resp.status).to.equal(500);
                let data = resp.data;
                exp(data.httpCode).to.equal(500);
                exp(data.appCode).to.equal(AppError.codes.SUBSCRIPTION_ERROR);

                let createMock = mockStripe.subscriptions.create.mock;
                exp(createMock.getNumCalls()).to.equal(0);
            });
        });
        describe('when no archived subscriptions exists', function(){
            it('should throw an error', async function(){
                let mockStripe = {
                    subscriptions: {
                        create: testHelper.createMock().returnCb([null, stripeSubscription]),
                    },
                };

                stripeService._setStripe(mockStripe);

                let resp = await ax.put(`/users/${user.getIdString()}/subscription/resume`);
                exp(resp.status).to.equal(500);
                let data = resp.data;
                exp(data.httpCode).to.equal(500);
                exp(data.appCode).to.equal(AppError.codes.SUBSCRIPTION_ERROR);

                let createMock = mockStripe.subscriptions.create.mock;
                exp(createMock.getNumCalls()).to.equal(0);
            });
        });
    });

    describe('GET: /users/:userId/subscription/:packageId/proration - getProrationCost', function(){
        describe('happy case - when existing subscription is present', function(){
           it('should return the proration amount', async function(){
               let now = Math.round(Date.now() / 1000);
               let invoice = {
                   lines: {
                       data: [
                           {period: {start: now - 10000}, amount: 1.99},
                           {period: {start: now - 10000}, amount: 2.99},
                           {period: {start: now + 10000}, amount: 12.99}
                       ]
                   }
               };
               let mockStripe = {
                   subscriptions: {
                       retrieve: testHelper.createMock().returnPromise({items: {data:[stripeSubscription]}})
                   },
                   invoices: {
                       retrieveUpcoming: testHelper.createMock().returnPromise(invoice)
                   }
               };

               stripeService._setStripe(mockStripe);
               let nextPackageId = Packages.getPremiumPackage().id;
               let sub = await new Subscription({
                   paymentMethodId: 'pmt_id', userId: user.getIdString(),
                   externalSubscriptionId: 'stripe_id', subscriptionPackageId: Packages.getBasicPackage().id, dateCancelled: Date.now()
               }).savePromise();

               await new PaymentMethod({tokenId: 'tok_123', userId: user.getIdString(), email:user.email, cardId: 'some_card_id'}).savePromise();

               let resp = await ax.get(`/users/${user.getIdString()}/subscription/${nextPackageId}/proration`);
               exp(resp.status).to.equal(200);
               let data = resp.data;
               exp(data).to.exist;
               exp(data.httpCode).to.equal(200);
               exp(data.data.cost).to.equal(4.98);
               exp(data.data.t).to.be.at.most(now);

               let retrieveMock = mockStripe.subscriptions.retrieve.mock;
               exp(retrieveMock.getNumCalls()).to.equal(1);
               exp(retrieveMock.getArgs()).to.equal(sub.externalSubscriptionId);

               let retrieveUpcomingMock = mockStripe.invoices.retrieveUpcoming.mock;
               exp(retrieveUpcomingMock.getNumCalls()).to.equal(1);
               let retrieveArg = retrieveUpcomingMock.getArgs();
               exp(retrieveArg.customer).to.equal(user.paymentCustomerId);
               exp(retrieveArg.subscription).to.equal(sub.externalSubscriptionId);
               exp(retrieveArg.subscription_proration_date).to.be.at.most(now);
               exp(retrieveArg.subscription_items).to.deep.equal([{id: stripeSubscription.id, plan: nextPackageId}]);
           });
        });
        describe('when existing no subscription is present', function(){
            it('should return http error', async function(){
                let mockStripe = {
                    subscriptions: {
                        retrieve: testHelper.createMock().returnPromise({items: {data:[stripeSubscription]}})
                    },
                    invoices: {
                        retrieveUpcoming: testHelper.createMock().returnCb([null, null])
                    }
                };

                stripeService._setStripe(mockStripe);
                let nextPackageId = Packages.getPremiumPackage().id;
                await new Subscription({
                    paymentMethodId: 'pmt_id', userId: user.getIdString(),
                    externalSubscriptionId: 'stripe_id', subscriptionPackageId: Packages.getBasicPackage().id,
                    dateCancelled: Date.now(), archived: true
                }).savePromise();

                await new PaymentMethod({tokenId: 'tok_123', userId: user.getIdString(), email:user.email, cardId: 'some_card_id'}).savePromise();

                let resp = await ax.get(`/users/${user.getIdString()}/subscription/${nextPackageId}/proration`);
                exp(resp.status).to.equal(500);
                let data = resp.data;
                exp(data).to.exist;
                exp(data.httpCode).to.equal(500);

                let retrieveMock = mockStripe.subscriptions.retrieve.mock;
                exp(retrieveMock.getNumCalls()).to.equal(0);

                let retrieveUpcomingMock = mockStripe.invoices.retrieveUpcoming.mock;
                exp(retrieveUpcomingMock.getNumCalls()).to.equal(0);
            });
        });
    });

    describe('GET: /users/:userId/subscription - getUserSubscription', function(){
        describe('when subscription exists', function(){
            it('should return the subscription', async function(){
                let sub = await new Subscription({
                    paymentMethodId: 'pmt_id', userId: user.getIdString(),
                    externalSubscriptionId: 'stripe_id', subscriptionPackageId: Packages.getBasicPackage().id,
                    dateCancelled: Date.now()
                }).savePromise();

                let resp = await ax.get(`/users/${user.getIdString()}/subscription`);
                exp(resp.status).to.equal(200);
                let data = resp.data;
                exp(data).to.exist;
                exp(data.data).to.equal(sub.subscriptionPackageId);
            });
        });
        describe('when subscription does not exist', function(){
            it('should not return the subscription', async function(){
                let sub = await new Subscription({
                    paymentMethodId: 'pmt_id', userId: user.getIdString(),
                    externalSubscriptionId: 'stripe_id', subscriptionPackageId: Packages.getBasicPackage().id,
                    dateCancelled: Date.now(), archived: true
                }).savePromise();

                await new PaymentMethod({tokenId: 'tok_123', userId: user.getIdString(), email:user.email, cardId: 'some_card_id'}).savePromise();

                let resp = await ax.get(`/users/${user.getIdString()}/subscription`);
                exp(resp.status).to.equal(200);
                let data = resp.data;
                exp(data).to.exist;
                exp(data.data).to.be.null;
            });
        })
    });

    describe('GET: /users/:userId/subscription/archived - getUserArchivedSubscriptions', function(){
        describe('when archived subscriptions exists', function(){
            it('should return the subscription', async function(){
                let sub = await new Subscription({
                    paymentMethodId: 'pmt_id', userId: user.getIdString(),
                    externalSubscriptionId: 'stripe_id', subscriptionPackageId: Packages.getBasicPackage().id,
                    dateCancelled: Date.now(), archived: true
                }).savePromise();

                let sub2 = await new Subscription({
                    paymentMethodId: 'pmt_id', userId: user.getIdString(),
                    externalSubscriptionId: 'stripe_id', subscriptionPackageId: Packages.getPremiumPackage().id,
                    dateCancelled: Date.now(), archived: true
                }).savePromise();

                await new Subscription({
                    paymentMethodId: 'pmt_id', userId: user.getIdString(),
                    externalSubscriptionId: 'stripe_id', subscriptionPackageId: Packages.getPremiumPackage().id,
                    dateCancelled: Date.now(), archived: false
                }).savePromise();

                let resp = await ax.get(`/users/${user.getIdString()}/subscription/archived`);
                exp(resp.status).to.equal(200);
                let data = resp.data;
                exp(data).to.exist;
                exp(data.data.length).to.equal(2);
                let expectedPackages = [sub.subscriptionPackageId, sub2.subscriptionPackageId];
                data.data.forEach(sub => {
                    exp(expectedPackages).to.include(sub.subscriptionPackageId);
                });
            });
        });
        describe('when active subscriptions exists', function(){
            it('should return the subscription', async function(){
                await new Subscription({
                    paymentMethodId: 'pmt_id', userId: user.getIdString(),
                    externalSubscriptionId: 'stripe_id', subscriptionPackageId: Packages.getBasicPackage().id,
                    dateCancelled: Date.now(), archived: false
                }).savePromise();

                await new Subscription({
                    paymentMethodId: 'pmt_id', userId: user.getIdString(),
                    externalSubscriptionId: 'stripe_id', subscriptionPackageId: Packages.getPremiumPackage().id,
                    dateCancelled: Date.now(), archived: false
                }).savePromise();

                await new Subscription({
                    paymentMethodId: 'pmt_id', userId: user.getIdString(),
                    externalSubscriptionId: 'stripe_id', subscriptionPackageId: Packages.getPremiumPackage().id,
                    dateCancelled: Date.now(), archived: false
                }).savePromise();

                let resp = await ax.get(`/users/${user.getIdString()}/subscription/archived`);
                exp(resp.status).to.equal(200);
                let data = resp.data;
                exp(data).to.exist;
                exp(data.data.length).to.equal(0);
            });
        });
    });
});