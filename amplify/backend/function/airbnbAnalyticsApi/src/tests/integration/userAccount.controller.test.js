process.env.ENV = 'test';

const chai = require('chai');
const exp = chai.expect;
const axios = require('axios');
const http = require('http');
const config = require('../../config');
const AppError = require('../../helpers/AppError');

let ax = axios.create({
    baseURL: config.baseUrl,
    headers: {
        'x-apigateway-event': '{}',
        'x-apigateway-context': '{}'
    },
    validateStatus: false
});

describe('User Controller Tests', function(){
    let appServer;
    let models;
    let User;

    before(async function(){
        app = require('../../app');
        appServer = http.createServer(app);
        await new Promise(fulfill => {
            appServer.listen(config.port, fulfill);
        });
    });

    after(function(done){
        appServer.on('close', async function() {
            done();
        });
        appServer.close();
    });

    beforeEach(async function() {
        models = await require('../../models');
        User = models.User;
    });

    afterEach(async function(){
        await User.removePromise();
    });

    describe('POST: /users - registerUser', function(){
        describe('when user does not exist', function() {
            it('should create user and return user', async function(){
                let users = await User.findAllPromise();
                exp(users.length).to.equal(0);
                let userData = {cognitoId: '1111-2222-3333', email: 'joe@mail.com', name: 'Joe Smoe'};
                let res = await ax.post('/users', userData);
                users = await User.findAllPromise();
                exp(users.length).to.equal(1);
                let user = users[0];
                exp(res.data).to.exist;
                let result = res.data.data;
                exp(result._id).to.equal(user.getIdString());
                exp(result.email).to.equal(user.email);
            });
        });
        describe('when user already exists', function() {
            it('should return existing user', async function(){
                let users = await User.findAllPromise();
                exp(users.length).to.equal(0);
                let userData = {cognitoId: '1111-2222-3333', email: 'joe@mail.com', name: 'Joe Smoe'};
                let res = await ax.post('/users', userData);
                users = await User.findAllPromise();
                exp(users.length).to.equal(1);
                res = await ax.post('/users', userData);
                exp(res.status).to.equal(400);
                let result = res.data;
                exp(result).to.exist;
                exp(result.httpCode).to.equal(400);
                exp(result.appCode).to.equal(AppError.codes.DUPLICATE_DATA);
            });
        });
        describe('when missing data', function() {
            it('should create user and return user', async function(){
                let userData = {cognitoId: '1111-2222-3333'};
                let res = await ax.post('/users', userData);
                exp(res.status).to.equal(400);
                exp(res.data.appCode).to.equal(AppError.codes.INVALID_DATA);
            });
        });
    });

    describe('GET: /users/:userId - getUser', function(){
        describe('when user exists', function() {
            it('should return user', async function(){
                let user = await new User({cognitoId: '1111-2222-3333', email: 'joe@mail.com', name: 'Joe Smoe'}).savePromise();
                await new User({cognitoId: '1111-2222-4444', email: 'joe2@mail.com', name: 'Joe Smoe 2'}).savePromise();
                let res = await ax.get(`/users/${user.getIdString()}`);
                exp(res.status).to.equal(200);
                let userResp = res.data.data;
                exp(userResp._id).to.equal(user.getIdString());
                exp(userResp.email).to.equal(user.email);
                exp(userResp.cognitoId).to.equal(user.cognitoId);
            });
        });
    });

    describe('GET: /users/exists - userExists', function(){
        describe('when user exists', function() {
            it('should return true', async function(){
                let user = await new User({cognitoId: '1111-2222-3333', email: 'joe@mail.com', name: 'Joe Smoe'}).savePromise();
                await new User({cognitoId: '1111-2222-4444', email: 'joe2@mail.com', name: 'Joe Smoe 2'}).savePromise();
                let res = await ax.get(`/users/exists?email=${user.email}`);
                exp(res.status).to.equal(200);
                let exists = res.data.data;
                exp(exists).to.be.true;
            });
        });
        describe('when user deos not exist', function() {
            it('should return false', async function(){
                let user = await new User({cognitoId: '1111-2222-3333', email: 'joe@mail.com', name: 'Joe Smoe'}).savePromise();
                await new User({cognitoId: '1111-2222-4444', email: 'joe2@mail.com', name: 'Joe Smoe 2'}).savePromise();
                let res = await ax.get(`/users/exists?email=${user.email}`);
                exp(res.status).to.equal(200);
                let exists = res.data.data;
                exp(exists).to.be.true;
            });
        });
    });

    describe('PUT: /users/:userId - updateUser', function(){
        describe('when user exists', function() {
            it('should update only the given fields - name', async function(){
                let user = await new User({cognitoId: '1111-2222-3333', email: 'joe@mail.com', name: 'Joe Smoe'}).savePromise();
                let user2 = await new User({cognitoId: '1111-2222-4444', email: 'joe2@mail.com', name: 'Joe Smoe 2'}).savePromise();
                let updates = {name: 'new name'};
                let res = await ax.put(`/users/${user.getIdString()}`, updates);
                exp(res.status).to.equal(200);
                let updatedUser = await User.findOneByIdPromise(user._id);
                exp(updatedUser._id.toString()).to.equal(user.getIdString());
                exp(updatedUser.email).to.equal(user.email);
                exp(updatedUser.name).to.equal(updates.name);
                exp(updatedUser.cognitoId).to.equal(user.cognitoId);
                let user2Update = await User.findOneByIdPromise(user2._id);
                exp(user2Update.name).to.equal(user2.name);
            });
            it('should update only the given fields - name', async function(){
                let user = await new User({cognitoId: '1111-2222-3333', email: 'joe@mail.com', name: 'Joe Smoe'}).savePromise();
                let updates = {email: 'new@email.com'};
                let res = await ax.put(`/users/${user.getIdString()}`, updates);
                exp(res.status).to.equal(200);
                let updatedUser = await User.findOneByIdPromise(user._id);
                exp(updatedUser._id.toString()).to.equal(user.getIdString());
                exp(updatedUser.email).to.equal(updates.email);
                exp(updatedUser.name).to.equal(user.name);
                exp(updatedUser.cognitoId).to.equal(user.cognitoId);
            });
            it('should update only the given fields - name', async function(){
                let user = await new User({cognitoId: '1111-2222-3333', email: 'joe@mail.com', name: 'Joe Smoe'}).savePromise();
                let updates = {email: 'new@email.com', name: 'some name'};
                let res = await ax.put(`/users/${user.getIdString()}`, updates);
                exp(res.status).to.equal(200);
                let updatedUser = await User.findOneByIdPromise(user._id);
                exp(updatedUser._id.toString()).to.equal(user.getIdString());
                exp(updatedUser.email).to.equal(updates.email);
                exp(updatedUser.name).to.equal(updates.name);
                exp(updatedUser.cognitoId).to.equal(user.cognitoId);
            });
        });
        describe('when updating non update-able fields', function() {
            it('should return as normal but make no changes', async function () {
                let user = await new User({cognitoId: '1111-2222-3333', email: 'joe@mail.com', name: 'Joe Smoe', paymentCustomerId: '111122'}).savePromise();
                let updates = {paymentCustomerId: 'someid'};
                let res = await ax.put(`/users/${user.getIdString()}`, updates);
                exp(res.status).to.equal(200);
                let updatedUser = await User.findOneByIdPromise(user._id);
                exp(updatedUser._id.toString()).to.equal(user.getIdString());
                exp(updatedUser.email).to.equal(user.email);
                exp(updatedUser.name).to.equal(user.name);
                exp(updatedUser.paymentCustomerId).to.equal(user.paymentCustomerId);
                exp(updatedUser.cognitoId).to.equal(user.cognitoId);
            });
        });
        describe('when user does not exist', function() {
            it('should update only the given fields - name', async function () {
                let updates = {name: 'new name'};
                let res = await ax.put(`/users/5e6ce3644222d074db92ca61`, updates);
                exp(res.status).to.equal(404);
                let data = res.data;
                exp(data.httpCode).to.equal(404);
                exp(data.appCode).to.equal(AppError.codes.USER_NOT_FOUND);
            });
        });
    });

    describe('GET: /users/cognito/:cognitoId - getUser', function(){
        describe('when user exists', function() {
            it('should return user', async function(){
                let user = await new User({cognitoId: '1111-2222-3333', email: 'joe@mail.com', name: 'Joe Smoe'}).savePromise();
                await new User({cognitoId: '1111-2222-4444', email: 'joe2@mail.com', name: 'Joe Smoe 2'}).savePromise();
                let res = await ax.get(`/users/cognito/${user.cognitoId}`);
                exp(res.status).to.equal(200);
                let userResp = res.data.data;
                exp(userResp._id).to.equal(user.getIdString());
                exp(userResp.email).to.be.undefined;
                exp(userResp.cognitoId).to.be.undefined;
            });
        });
        describe('when user does not exist', function() {
            it('should return null', async function(){
                let user = await new User({cognitoId: '1111-2222-3333', email: 'joe@mail.com', name: 'Joe Smoe'}).savePromise();
                await new User({cognitoId: '1111-2222-4444', email: 'joe2@mail.com', name: 'Joe Smoe 2'}).savePromise();
                let res = await ax.get(`/users/cognito/1111-2222-44445`);
                exp(res.status).to.equal(200);
                let userResp = res.data.data;
                exp(userResp).to.be.null;
            });
        });
    });
});