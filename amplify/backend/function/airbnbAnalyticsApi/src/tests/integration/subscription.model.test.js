process.env.ENV = 'test';

const chai = require('chai');
const exp = chai.expect;
const moment = require('moment');


describe('Subscription Model Tests', function(){
    let models;
    let Subscription;

    beforeEach(async function() {
        models = await require('../../models');
        Subscription = models.Subscription;
    });

    afterEach(async function(){
        await Subscription.removePromise();
    });

    describe('getNextPaymentDate', function() {
        describe('trial has not ended yet', function () {
            it('calculate the next payment date', async function () {
                let trialEndDate = moment().set('year', 2032).set('month', 3).set('date', 10);
                let now = moment().set('year', 2032).set('month', 3).set('date', 5);

                let sub = new Subscription({trialEndDate: trialEndDate.valueOf()});
                let nextPayment = sub.getNextPaymentDate(now.valueOf());
                isDateEqual(nextPayment, trialEndDate.valueOf());
            });
        });
        describe('same month', function () {
            it('calculate the next payment date', async function () {
                let trialEndDate = moment().set('year', 2032).set('month', 3).set('date', 10);
                let now = moment().set('year', 2032).set('month', 3).set('date', 18);

                let sub = new Subscription({trialEndDate: trialEndDate.valueOf()});
                let nextPayment = sub.getNextPaymentDate(now.valueOf());
                let expectedNextPayment = moment().set('year', 2032).set('month', 4).set('date', 10);
                isDateEqual(nextPayment, expectedNextPayment.valueOf());
            });
        });
        describe('next month', function () {
            it('but before date', async function () {
                let trialEndDate = moment().set('year', 2032).set('month', 3).set('date', 10);
                let now = moment().set('year', 2032).set('month', 4).set('date', 7);

                let sub = new Subscription({trialEndDate: trialEndDate.valueOf()});
                let nextPayment = sub.getNextPaymentDate(now.valueOf());
                let expectedNextPayment = moment().set('year', 2032).set('month', 4).set('date', 10);
                isDateEqual(nextPayment, expectedNextPayment.valueOf());
            });
            it('but past date', async function () {
                let trialEndDate = moment().set('year', 2032).set('month', 5).set('date', 10);
                let now = moment().set('year', 2032).set('month', 4).set('date', 18);

                let sub = new Subscription({trialEndDate: trialEndDate.valueOf()});
                let nextPayment = sub.getNextPaymentDate(now.valueOf());
                let expectedNextPayment = moment().set('year', 2032).set('month', 5).set('date', 10);
                isDateEqual(nextPayment, expectedNextPayment.valueOf());
            });
        });
        describe('a few months', function () {
            it('but before date', async function () {
                let trialEndDate = moment().set('year', 2032).set('month', 3).set('date', 10);
                let now = moment().set('year', 2032).set('month', 8).set('date', 7);

                let sub = new Subscription({trialEndDate: trialEndDate.valueOf()});
                let nextPayment = sub.getNextPaymentDate(now.valueOf());
                let expectedNextPayment = moment().set('year', 2032).set('month', 8).set('date', 10);
                isDateEqual(nextPayment, expectedNextPayment.valueOf());
            });
            it('but past date', async function () {
                let trialEndDate = moment().set('year', 2032).set('month', 5).set('date', 10);
                let now = moment().set('year', 2032).set('month', 10).set('date', 18);

                let sub = new Subscription({trialEndDate: trialEndDate.valueOf()});
                let nextPayment = sub.getNextPaymentDate(now.valueOf());
                let expectedNextPayment = moment().set('year', 2032).set('month', 11).set('date', 10);
                isDateEqual(nextPayment, expectedNextPayment.valueOf());
            });
        });
        describe('leap year', function () {
            it('before date', async function () {
                let trialEndDate = moment().set('year', 2020).set('month', 1).set('date', 29);
                let now = moment().set('year', 2021).set('month', 1).set('date', 7);

                let sub = new Subscription({trialEndDate: trialEndDate.valueOf()});
                let nextPayment = sub.getNextPaymentDate(now.valueOf());
                let expectedNextPayment = moment().set('year', 2021).set('month', 1).set('date', 28);
                isDateEqual(nextPayment, expectedNextPayment.valueOf());
            });
            it('same date', async function () {
                let trialEndDate = moment().set('year', 2020).set('month', 1).set('date', 29);
                let now = moment().set('year', 2021).set('month', 1).set('date', 28);

                let sub = new Subscription({trialEndDate: trialEndDate.valueOf()});
                let nextPayment = sub.getNextPaymentDate(now.valueOf());
                let expectedNextPayment = moment().set('year', 2021).set('month', 1).set('date', 28);
                isDateEqual(nextPayment, expectedNextPayment.valueOf());
            });
        });
        describe('two leap years', function(){
            it('same date', async function () {
                let trialEndDate = moment().set('year', 2016).set('month', 1).set('date', 29);
                let now = moment().set('year', 2020).set('month', 1).set('date', 29);

                let sub = new Subscription({trialEndDate: trialEndDate.valueOf()});
                let nextPayment = sub.getNextPaymentDate(now.valueOf());
                let expectedNextPayment = moment().set('year', 2020).set('month', 1).set('date', 29);
                isDateEqual(nextPayment, expectedNextPayment.valueOf());
            });
            it('before date', async function () {
                let trialEndDate = moment().set('year', 2016).set('month', 1).set('date', 29);
                let now = moment().set('year', 2020).set('month', 1).set('date', 27);

                let sub = new Subscription({trialEndDate: trialEndDate.valueOf()});
                let nextPayment = sub.getNextPaymentDate(now.valueOf());
                let expectedNextPayment = moment().set('year', 2020).set('month', 1).set('date', 29);
                isDateEqual(nextPayment, expectedNextPayment.valueOf());
            });
        });
    });
});

function isDateEqual(time1, time2){
    let m1 = moment(time1);
    let m2 = moment(time2);
    exp(m1.toISOString().split('T')[0]).to.equal(m2.toISOString().split('T')[0]);
}