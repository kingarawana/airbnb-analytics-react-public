const ObjectId = require('mongoose').Types.ObjectId;

const util = {};

util.isObjectLiteral = function(obj){
    if (typeof obj == 'object' && obj !== null) {
        // If Object.getPrototypeOf supported, use it
        if (typeof Object.getPrototypeOf == 'function') {
            var proto = Object.getPrototypeOf(obj);
            return proto === Object.prototype || proto === null;
        }
        // Otherwise, use internal class
        // This should be reliable as if getPrototypeOf not supported, is pre-ES5
        return Object.prototype.toString.call(obj) == '[object Object]';
    }
    return false;
};

util.convertLiteralsToInstances = function(objs, clazz){
    var results = [];
    if(objs.length > 0 && (objs[0] instanceof clazz)){
        results = objs;
    }else{
        for(var i = 0; i < objs.length; i++){
            results.push(new clazz(objs[i]));
        }
    }
    return results;
};

util.toObjectId = function(id){
    if(id instanceof ObjectId){
        return id;
    }
    return new ObjectId(id);
};

util.isObjectId = function(id){
    if(id instanceof ObjectId){
        return true;
    }
    return ObjectId.isValid(id);
};


util.getNewObjectId = function(id){
    return new ObjectId();
};

module.exports = util;
