class AppError {
    constructor(message, httpCode = 500, appCode, devMessage){
        if(appCode && appCode < 1000) throw 'Invalid values passed to AppError constructor.'
        if(message instanceof AppError){
            this.httpCode = message.httpCode;
            this.appCode = message.appCode;
            this.message = message.message;
            this.developerMessage = message.devMessage;
        }else{
            this.httpCode = httpCode;
            this.appCode = appCode;
            this.message = message;
            this.developerMessage = devMessage;
        }
    }

    toObject(){
        return JSON.parse(JSON.stringify(this));
    }

    sendResponse(res){
        res.status(this.httpCode).json(this.toObject());
    }

    static isAppError(err){
        return err instanceof AppError;
    }
}

AppError.codes = {
    // server/app errors
    APP_ERROR: 1000,
    INVALID_UPDATE: 1001,
    UNAUTHORIZED: 1002,
    INVALID_DATA: 1003,
    DUPLICATE_DATA: 1004,
    INVALID_REQUEST: 1005,

    // user errors
    USER_NOT_FOUND: 2000,
    USER_ALREADY_EXISTS: 2001,
    INVALID_EMAIL_ADDRESS: 2002,
    USER_ALREADY_REGISTERED_WITH_FB: 2003,
    USER_ALREADY_REGISTERED_WITH_EMAIL: 2004,

    // subscription errors
    SUBSCRIPTION_ERROR: 3000,
    SUBSCRIPTION_NOT_FOUND: 3001,
    SUBSCRIPTION_UPDATE_ERROR: 3002,

    // payment method errors
    PAYMENT_METHOD_NOT_FOUND: 3000,
};

module.exports = AppError;
