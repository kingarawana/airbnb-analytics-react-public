class AppResponse {
    constructor(message, data={}, httpCode = 200, appCode){
        this.message = message;
        this.data = data;
        this.httpCode = httpCode;
        this.appCode = appCode;
    }

    toObject(){
        return JSON.parse(JSON.stringify(this));
    }

    sendResponse(res){
        res.status(this.httpCode).json(this.toObject());
    }
}

module.exports = AppResponse;
