module.exports = {
    "channel": "auth",
    "payload": {
        "event": "signUp",
        "data": {
            "user": {
                "username": "joesmoe@gmail.com",
                "pool": {
                    "userPoolId": "us-west-2_59ei1UOgF",
                    "clientId": "1f1blca88c1a2dti4d4dperho9",
                    "client": {
                        "endpoint": "https://cognito-idp.us-west-2.amazonaws.com/",
                        "userAgent": "aws-amplify/0.1.x js"
                    },
                    "advancedSecurityDataCollectionFlag": true
                },
                "Session": null,
                "client": {
                    "endpoint": "https://cognito-idp.us-west-2.amazonaws.com/",
                    "userAgent": "aws-amplify/0.1.x js"
                },
                "signInUserSession": null,
                "authenticationFlowType": "USER_SRP_AUTH",
                "keyPrefix": "CognitoIdentityServiceProvider.1f1blca88c1a2dti4d4dperho9",
                "userDataKey": "CognitoIdentityServiceProvider.1f1blca88c1a2dti4d4dperho9.joesmoe@gmail.com.userData"
            },
            "userConfirmed": false,
            "userSub": "f39c02f0-0cda-4179-9f14-a73380785a30",
            "codeDeliveryDetails": {
                "AttributeName": "email",
                "DeliveryMedium": "EMAIL",
                "Destination": "j***@g***.com"
            }
        },
        "message": "joesmoe@gmail.com has signed up successfully"
    },
    "source": "Auth",
    "patternInfo": [

    ]
};