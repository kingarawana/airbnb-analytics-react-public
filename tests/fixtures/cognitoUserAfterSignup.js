export default {
    "user": {
        "username": "conorlieu@gmail.com",
        "pool": {
            "userPoolId": "us-west-2_59ei1UOgF",
            "clientId": "1f1blca88c1a2dti4d4dperho9",
            "client": {
                "endpoint": "https: //cognito-idp.us-west-2.amazonaws.com/",
                "userAgent": "aws-amplify/0.1.xjs"
            },
            "advancedSecurityDataCollectionFlag": true
        },
        "Session": null,
        "client": {
            "endpoint": "https: //cognito-idp.us-west-2.amazonaws.com/",
            "userAgent": "aws-amplify/0.1.xjs"
        },
        "signInUserSession": null,
        "authenticationFlowType": "USER_SRP_AUTH",
        "keyPrefix": "CognitoIdentityServiceProvider.1f1blca88c1a2dti4d4dperho9",
        "userDataKey": "CognitoIdentityServiceProvider.1f1blca88c1a2dti4d4dperho9.conorlieu@gmail.com.userData"
    },
    "userConfirmed": false,
    "userSub": "571e3f62-923a-4f67-8b11-1b26ee330eab",
    "codeDeliveryDetails": {
        "AttributeName": "email",
        "DeliveryMedium": "EMAIL",
        "Destination": "c***@g***.com"
    }
}