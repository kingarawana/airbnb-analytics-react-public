import chai from 'chai'
import axios from 'axios'
import config from '../../../src/config/env.test';
import cognitoUser from '../../fixtures/cognitoUser';
import userAccountManager from '../../../src/managers/userAccountManager';

const exp = chai.expect;

let ax = axios.create({
    baseURL: config.baseUrl,
    validateStatus: false
});


describe('userAccountManager test', function(){
    afterEach(async function(){
        if(config.port !== 2999){
            throw Error('Must run test with ENV=test');
        }
        await ax.delete('/users');
    });
    describe('createNewUserAccount', function(){
       it('should create user in our system when valid data', async function(){
           let resp = await ax.get('/users');
           exp(resp.data).to.exist;
           exp(resp.data.data).to.deep.equal([]);
           let user = await userAccountManager.createNewUserAccount(cognitoUser);
           exp(user._id).to.exist;
           exp(user.cognitoId).to.equal(cognitoUser.username);
           resp = await ax.get('/users');
           exp(resp.data.data.length).to.equal(1);
           exp(resp.data.data[0]._id).to.equal(user._id);
       });
    });
});