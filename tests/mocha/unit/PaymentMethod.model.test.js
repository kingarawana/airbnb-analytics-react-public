import chai from 'chai';
import PaymentMethod from "../../../src/models/PaymentMethod";
import stripeCreateTokenResult from "../../fixtures/stripeCreateTokenResult";
const exp = chai.expect;


describe('PaymentMethod Test', function(){
    describe('createFromRawObj test', function(){
        it('should create an instance of PaymentMethod', async function(){
            let token = stripeCreateTokenResult.token;
            let pm = PaymentMethod.createFromRawObj(token);
            let card = token.card;
            exp(pm.tokenId).to.equal(token.id);
            exp(pm.cardBrand).to.equal(card.brand);
            exp(pm.last4).to.equal(card.last4);
            exp(pm.addressZip).to.equal(card.address_zip);
            exp(pm.cardId).to.equal(card.id);
            exp(pm.expireMonth).to.equal(card.exp_month);
            exp(pm.expireYear).to.equal(card.exp_year);
            exp(pm.fundType).to.equal(card.funding);
        });
    });
    describe('toDict test', function(){
        it('should create an instance of PaymentMethod', async function(){
            let token = stripeCreateTokenResult.token;
            let pm = PaymentMethod.createFromRawObj(token);
            let card = token.card;
            let pmDict = pm.toDict();
            exp(pmDict).to.be.an.instanceof(Object);
            exp(pmDict.tokenId).to.equal(token.id);
            exp(pmDict.cardBrand).to.equal(card.brand);
            exp(pmDict.last4).to.equal(card.last4);
            exp(pmDict.addressZip).to.equal(card.address_zip);
            exp(pmDict.cardId).to.equal(card.id);
            exp(pmDict.expireMonth).to.equal(card.exp_month);
            exp(pmDict.expireYear).to.equal(card.exp_year);
            exp(pmDict.fundType).to.equal(card.funding);
        });
    });
});