import _ from 'lodash'
import User from '../models/User';
import apiService from "../services/apiService";
import dataService from "../services/dataService";
import planService from "../services/planService";
import PaymentMethod from "../models/PaymentMethod";

export const fetchWatchLists = userEmail => {
    return async dispatch => {
        let lists = await dataService.fetchWatchLists(userEmail);
        return dispatch(watchListsFetchSuccess(lists));
    }
};

export const fetchCurrentSubscription = userId => {
    return async dispatch => {
        let subId = await apiService.fetchCurrentUserSubscription(userId);
        if(!subId){
            subId = planService.getBasicFreePlanId();
        }
        return dispatch(setCurrentPlanId(subId));
    }
};

export const fetchPaymentMethods = userId => {
    return async dispatch => {
        let lists = await apiService.fetchUserPaymentMethods(userId);
        return dispatch(setPaymentMethods(PaymentMethod.create(lists)));
    }
};

export const createWatchList = input => {
    return async dispatch => {
        let created = await apiService.createWatchList(input);
        await dispatch(emitWatchListCreated(created));
        return created;
    }
};

export const deleteWatchList = watchListId => {
    return async dispatch => {
        await apiService.deleteWatchList(watchListId);
        await dispatch(emitWatchListDeleted(watchListId));
        return watchListId;
    };
};

export const updateWatchList = watchList => {
    return async dispatch => {
        await apiService.updateWatchList(watchList);
        await dispatch(emitWatchListUpdated(watchList));
        return watchList;
    };
};

export const setCurrentPage = currentPage => {
    return {type: 'SET_CURRENT_PAGE', currentPage}
};

export const setCurrentPublicPage = currentPublicPage => {
    return {type: 'SET_CURRENT_PUBLIC_PAGE', currentPublicPage}
};

export const emitWatchListUpdated = watchList => {
    return {type: 'WATCH_LIST_UPDATED', watchList}
};

export const emitWatchListDeleted = watchListId => {
    return {type: 'WATCH_LIST_DELETED', watchListId}
};

export const emitWatchListCreated = watchList => {
    return {type: 'WATCH_LIST_CREATED', watchList}
};

export const setLoggedInUser = loggedInUser => {
    if(loggedInUser && !(loggedInUser instanceof User)){
        loggedInUser = new User(loggedInUser);
    }
    // i may want to consider moving this to userAccountManager
    localStorage.setItem('IS_LOGGED_IN', !!loggedInUser);
    return {
        type: 'SET_LOGGED_IN_USER',
        loggedInUser
    };
};

export const setNewUserWatchList = newUserWatchList => {
    return {
        type: 'SET_NEW_USER_WATCH_LIST',
        newUserWatchList
    };
};

export const setSelectedPlanId = selectedPlanId => {
    return {
        type: 'SET_SELECTED_PLAN_ID',
        selectedPlanId
    };
};

export const setCurrentPlanId = currentPlanId => {
    return {
        type: 'SET_CURRENT_PLAN_ID',
        currentPlanId
    };
};

export const setCurrentPaymentMethod = currentPaymentMethod => {
    return {
        type: 'SET_CURRENT_PAYMENT_METHOD',
        currentPaymentMethod
    };
};

export const setPaymentMethods = paymentMethods => {
    return {
        type: 'SET_PAYMENT_METHODS',
        paymentMethods
    };
};

export const setSelectedUserWatchList = selectedUserWatchList => {
    return {
        type: 'SET_SELECTED_USER_WATCH_LIST',
        selectedUserWatchList
    };
};

export const watchListsFetchSuccess = userWatchLists => {
    return {
        type: 'SET_USER_WATCH_LISTS',
        userWatchLists
    };
};

/**
 * type = success, error, warning and info
 * @deprecated. Please use emitter.showNotification
 */
export function showNotification(type = 'info', msg, props = {}) {
  var note = {};
  note.level = type;
  note.message = msg;

  var defaults = {
    position: 'tc'
  };

  note = _.assign(defaults, props, note);
  return {
      type: 'SHOW_NOTIFICATION',
      note
  };
}
