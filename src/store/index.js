import initialState from './initialState'
import configureStore from './configureStore'

let store = configureStore(initialState);
export default store;
