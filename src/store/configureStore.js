import { createStore, applyMiddleware } from 'redux'
import rootReducer from '../reducers'
import listener from '../listeners'
import thunk from 'redux-thunk';

export default function configureStore(initialState) {
    return createStore(rootReducer,
      initialState,
      applyMiddleware(thunk, listener))
}
