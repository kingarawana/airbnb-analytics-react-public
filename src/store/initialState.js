import { PAGE_TYPES } from "../classes/Page";

/**
 * // i `undefined` so i know the difference between data doesn't exist and data that hasn't been populated yet.
 */
export default {
    loggedInUser: null,
    userWatchLists: null,
    newUserWatchList: null,
    selectedUserWatchList: null,
    currentPage: PAGE_TYPES.SPY,
    selectedPlanId: null,
    paymentMethods: undefined,
    currentPaymentMethod: undefined
}
