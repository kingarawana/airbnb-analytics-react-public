import React from 'react';
import ReactDOM from 'react-dom';
import Public from './views/pages/Public';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { withRouter } from 'react-router'
import store from './store'
import { Provider, connect } from 'react-redux'
import emitter from './helpers/emitter.js'
import NotificationSystem from 'react-notification-system'
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import 'react-confirm-alert/src/react-confirm-alert.css';
import API from "@aws-amplify/api";
import awsconfig from "./aws-exports";
import Amplify, {Hub} from "aws-amplify";
import Auth from "@aws-amplify/auth";
import Admin from "./views/pages/Admin";
import DevAdmin from "./views/pages/DevAdmin";
import Spinner from './views/components/Spinner';
import userAccountManager from './managers/userAccountManager';
import './index.css'

awsconfig.oauth.redirectSignIn = process.env.REACT_APP_REDIRECT_URL;
awsconfig.oauth.redirectSignOut = process.env.REACT_APP_REDIRECT_URL;

API.configure(awsconfig);
Auth.configure(awsconfig);
Amplify.configure(awsconfig);

class Main extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            showSpinner: false
        };
        emitter.addListener('show_spinner', showSpinner => {
            this.setState({showSpinner});
        });
        emitter.addListener('show_notification', note => {
            this.refs.notificationSystem.addNotification(note);
        });
    }
    componentWillReceiveProps(props){
        if(props.note !== this.props.note){
            this.refs.notificationSystem.addNotification(props.note);
        }
    }

    componentDidMount(){
        Hub.listen('auth', (data) => {
            switch (data.payload.event) {
                case 'signIn':
                    userAccountManager.setLoggedInUserFromCognitoData(data.payload.data);
                    break;
                case 'signUp':
                    break;
                case 'signIn_failure':
                    break;
                case 'signOut':
                    break;
                default:
                    break;
            }
        });
        Auth.currentAuthenticatedUser().then(user => {
            userAccountManager.setLoggedInUserFromCognitoData(user)
        }).catch(e => {
            userAccountManager.logoutUserPromise();
        });
    }

    _goBack(){
        if(this.props.history.location.pathname.split('/').length === 3){
            this.props.setActionState();
        }
        this.props.history.goBack();
    }

    render () {
        return (
            <div id="outer-container">
                <div id="right-container">
                    <main id="page-wrap">
                        <div style={{width: '80%', margin: '0 auto'}}>
                            <Switch>
                                <Route path='/admin/' component={Admin}/>
                                <Route path='/super/' component={DevAdmin}/>
                                <Route path='/' component={Public}/>
                            </Switch>
                        </div>
                    </main>
                </div>
                <NotificationSystem allowHTML={true} style={style} ref="notificationSystem" />
                <div style={{position: 'absolute', top: '30%', left: '47%', zIndex: 10001}}>
                    <Spinner loading={this.state.showSpinner}/>
                </div>
                <div style={{position: 'absolute', width: '100%',
                    height: '100%', backgroundColor: 'white', opacity: '90%',
                    display: this.state.showSpinner ? 'block' : 'none',
                    top: 0, left: 0, zIndex: 10000
                }} onClick={e=>{
                    e.preventDefault();
                    e.stopPropagation();
                }}
                onMouseOver={e => {
                    e.preventDefault();
                    e.stopPropagation();
                }}>
                </div>
            </div>
        );
    }
}

const style = {
    NotificationItem: {
        DefaultStyle: {
            borderRadius: '10px',
            fontSize: '1.1rem',
            padding: '1rem'
        }
    }
};

const mapStateToProps = state => {
    return {
        note: state.appNotification,
    }
};

const mapDispatchToProps = dispatch => {
    return {
    }
};

Main = withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(Main));


ReactDOM.render((
    <BrowserRouter>
        <Provider store={store}>
            <Main/>
        </Provider>
    </BrowserRouter>
), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();


