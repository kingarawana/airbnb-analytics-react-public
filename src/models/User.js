import util from '../helpers/util'
import BaseModel from './BaseModel'

class User extends BaseModel {
  constructor(obj){
    super(obj);
    this.id = null;
    this.userId = null; // internal system id
    this.paymentCustomerId = null;
    this.email = null;
    this.name = null;
    this.verified = true;
    this.cognitoUser = null;
    if(obj && obj.attributes){
      this.id = obj.username;
      this.userId = obj.userId;
      this.paymentCustomerId = obj.paymentCustomerId;
      this.email = obj.attributes.email;
      this.name = obj.attributes.name;
      this.verified = obj.attributes.email_verified;
      this.cognitoUser = obj;
    }
  }
  setName(name){
    this.name = name;
    if(this.cognitoUser){
      this.cognitoUser.attributes.name = name;
    }
  }
}
util.addGettersAndSetters(User);

export default User;
