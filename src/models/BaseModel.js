let _ = require('lodash');

let ignore = new Set(['updateDate', 'archived', 'creationDate']);

class BaseModel {
  /**
   * Checks if any of the user editable fields are different.
   */
  isEqual(other){
    for(var prop in this){
      if(ignore.has(prop)) continue;
      if(!_.isEqual(this[prop], other[prop])){
        return false;
      }
    }
    return true;
  }

  clone(){
    var clone = _.cloneDeep(this);
    Object.setPrototypeOf(clone, Object.getPrototypeOf(this));
    return clone;
  }

  toObject(){
    return JSON.parse(this.toString());
  }

  toString(){
    return JSON.stringify(this);
  }
}

export default BaseModel;
