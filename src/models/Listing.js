import util from '../helpers/util'
import BaseModel from './BaseModel'

class Listing extends BaseModel {
    constructor(obj){
        super(obj);
        this.id = null;
        this.title = null;
        this.numBeds = null;
        this.numBaths = null;
        this.numSleeps = null;
        /**
         * This will is populated and refreshed locally. Does not come from server.
         *
         * @type {calendar: [{}..]}
         */
        this.data = null;
        if(obj){
            if(typeof obj === 'string'){
                obj = JSON.parse(obj);
            }
            util.setObjectProperties(this, obj);
        }
    }

    getDates(){
        if(this.data){
            return this.data.calendarDates;
        }
    }

    static fromRawData(data){
        let listing = new Listing();
        listing.id = data.id;
        listing.title = data.name;
        listing.numBeds = data.bedrooms;
        listing.numBaths = data.bathrooms;
        listing.numSleeps = data.person_capacity;
        listing.numGuestsIncluded = data.guests_included;
        return listing;
    }
}
util.addGettersAndSetters(Listing);

export default Listing;
