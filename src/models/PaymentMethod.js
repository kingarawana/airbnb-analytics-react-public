import util from "../helpers/util";
import Card from '../classes/Card';

class PaymentMethod {
    constructor(obj) {
        this._id = null;
        this.tokenId = null;
        /**
         * userId and email currently gets set in the backend.
         */
        this.userId = null;
        this.name = null; // name on card
        this.email = null;
        this.cardBrand = null;
        this.cardBrandId = null;
        this.last4 = null;
        this.addressZip = null;
        this.cardId = null;
        this.expireMonth = null;
        this.expireYear = null;
        this.fundType = null;
        this.isDefault = false; // this value is not stored in backend. It's set at run time.

        if(obj){
            util.setObjectProperties(this, obj);
        }
    }

    /**
     * "card": {
    "id": "card_1GQ7qMAEOGPkGCAIM6fMHjh4",
    "object": "card",
    "address_city": null,
    "address_country": null,
    "address_line1": null,
    "address_line1_check": null,
    "address_line2": null,
    "address_state": null,
    "address_zip": "90026",
    "address_zip_check": "unchecked",
    "brand": "Visa",
    "country": "US",
    "cvc_check": "unchecked",
    "dynamic_last4": null,
    "exp_month": 4,
    "exp_year": 2024,
    "funding": "credit",
    "last4": "4242",
    "metadata": {

    },
    "name": null,
    "tokenization_method": null
  },
     * @param tokenObj
     */
    static createFromStripeToken(tokenObj){
        let pm = new PaymentMethod();
        // util.setObjectProperties(pm, tokenObj);
        pm.tokenId = tokenObj.id;
        let card = tokenObj.card;
        pm.cardBrand = card.brand;
        pm.cardBrandId = Card.getCardTypeFromCardName(card.brand);
        pm.last4 = card.last4;
        pm.expireMonth = card.exp_month;
        pm.expireYear = card.exp_year;
        pm.fundType = card.funding;
        pm.cardId = card.id;
        pm.addressZip = card.address_zip;
        return pm;
    }

    getMmYy(){
        if(this.expireMonth && this.expireYear){
            let pad = function(val){
                if(val < 10){
                    return `0${val}`;
                }else{
                    return String(val);
                }
            };

            return `${pad(this.expireMonth)}/${pad(this.getFullYear())}`;
        }
    }

    getFullYear(){
        if(this.expireYear < 1000){
            return this.expireYear
        }else{
            return parseInt(String(this.expireYear).substr(-2));
        }
    }
    
    static createFromRawObj(obj){
        let card = obj.card;
        let pm = new PaymentMethod();
        pm.tokenId = obj.id;
        pm.cardId = card.id;
        // pm.userId = null;
        // pm.email = null;
        pm.cardBrand = card.brand;
        pm.last4 = card.last4;
        pm.addressZip = card.address_zip;
        pm.expireMonth = card.exp_month;
        pm.expireYear = card.exp_year;
        pm.fundType = card.funding;
        return pm;
    }

    static create(pmsObjs){
        return pmsObjs.map(pm => new PaymentMethod(pm));
    }

    static sortAndConvertIfNeeded(paymentMethodDatas){
        let pms = paymentMethodDatas.map(pm => {
            pm.isDefault = false;
            if(pm instanceof PaymentMethod){
                return pm;
            }else{
                return new PaymentMethod(pm);
            }
        });
        pms.sort((a, b)=>{
            if(a.lastTimeWasDefault > b.lastTimeWasDefault){
                return -1
            }else if(a.lastTimeWasDefault < b.lastTimeWasDefault){
                return 1;
            }
            return 0;
        });
        if(pms.length > 0){
            pms[0].isDefault = true;
        }
        return pms;
    }

    setChanges(updates){
        console.log('updates', updates);
        for(let key in updates){
            this[key] = updates[key];
        }
    }

    copy(){
        return new PaymentMethod(this.toDict());
    }

    toDict(){
        let keys = Object.keys(this);
        return keys.reduce((prev, key)=>{
            prev[key] = this[key];
            return prev;
        }, {})
    }
}

export default PaymentMethod