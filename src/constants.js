export const PAGE_TYPES = {
    SPY: 'SPY',
    COMMUNITY: 'COMMUNITY',
    ACCOUNT: 'ACCOUNT'
};

export const PUBLIC_PAGE_TYPES = {
    LOGIN: 'LOGIN',
    ADMIN: 'ADMIN',
    HOME: 'HOME',
    ANALYTICS: 'ANALYTICS',
    SUBSCRIPTION: 'SUBSRITPION',
    SHOWCASE: 'SHOWCASE',
    COMMUNITY: 'COMMUNITY',
    GUEST_REVIEW: 'GUEST_REVIEW',
    MANAGEMENT: 'MGMT',
    REPAIR: 'REPAIR',
    CLEANER: 'CLEANER',
};