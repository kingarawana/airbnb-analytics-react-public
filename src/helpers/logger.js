const config = require('../config');
const SHOULD_LOG_DEBUG = config.logger.logLevel.includes('debug');
const SHOULD_LOG_INFO = config.logger.logLevel.includes('info') || SHOULD_LOG_DEBUG;
const SHOULD_LOG_WARN = config.logger.logLevel.includes('warn') || SHOULD_LOG_INFO;
const SHOULD_LOG_ERROR = config.logger.logLevel.includes('error') || SHOULD_LOG_WARN;
const DELIMITER = '::';
/**
 * Adding this her so I can add a logger in the future.
 *
 */
module.exports = {
    info: function(){
        if(SHOULD_LOG_INFO) console.info(getDate(), DELIMITER, ...arguments);
    },
    debug: function(){
        if(SHOULD_LOG_DEBUG) console.debug(getDate(), DELIMITER, ...arguments);
    },
    warn: function(){
        if(SHOULD_LOG_WARN) console.warn(getDate(), DELIMITER, ...arguments);
    },
    error: function() {
        if(SHOULD_LOG_ERROR) console.error(getDate(), DELIMITER, ...arguments);
    }
};

function getDate(){
    return new Date().toLocaleString("en-US", {timeZone: "America/Los_Angeles"})
}