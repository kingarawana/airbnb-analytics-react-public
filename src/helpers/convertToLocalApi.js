import axios from 'axios';
import config from '../config';
import logger from './logger'

let ax = axios.create({
    baseURL: config.baseUrl,
    headers: {
        'x-apigateway-event': '{}',
        'x-apigateway-context': '{}'
    },
    validateStatus: false
});

/**
 * This is only used when running the app locally so that we can call locally run API vs the one in the cloud.
 * @param API
 */
export default function(API){
    API.get = async (apiName, path)=>{
        logger.debug('get path', path);
        return (await ax.get(path)).data;
    };
    API.post = async (apiName, path, params)=>{
        let body = params ? params.body : null;
        logger.debug('post path', path);
        return (await ax.post(path, body)).data;
    };
    API.put = async (apiName, path, params)=>{
        let body = params ? params.body : null;
        logger.debug('put path', path, body);
        return (await ax.put(path, body)).data;
    };
    API.del = async (apiName, path)=>{
        logger.debug('del path', path);
        return ax.delete(path);
    };
}
