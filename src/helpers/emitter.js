import {EventEmitter} from 'fbemitter';

var emitter = new EventEmitter();

emitter.showSpinner = (showSpinner) => {
  emitter.emit('show_spinner', showSpinner);
};

emitter.showNotification = (type = 'info', msg, props = {}) => {
  var note = {};
  note.level = type;
  note.message = msg;

  var defaults = {
    position: 'tc'
  };

  note = Object.assign(defaults, props, note);
  emitter.emit('show_notification', note);
};

export default emitter;
