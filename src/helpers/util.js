let util = {};
const DAY_NAMES = ['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'];

util.assertFieldsHaveValue = (obj, fields) => {
  fields.forEach(f => {
    if(obj[f] === null || obj[f] === undefined) {
      throw `[${f}] is required`
    }
  })
};

util.getWeekdayName = dateString => {
  return DAY_NAMES[new Date(dateString).getDay()];
};

util.extractListingIdFromUrl = listingUrl => {
  let url;
  try{
    url = new URL(listingUrl);
  }catch(e){
    return false;
  }
  let path = url.pathname;
  let parts = path.split('/').filter(p => !!p && !isNaN(p));
  console.log('parts', parts);
  if(parts.length === 1){
    return parts[0];
  }
  return null;
};

util.errorsFromGraphQLCreate = errors => {
  return errors.reduce((result, current)=>{
    return result + current + ' \n';
  }, '')
};

util.convertMilliToSeconds = milli => {
  if(typeof milli == 'number'){
    return Math.round((milli/1000)*100)/100
  }
  return milli.map(m => Math.round((m/1000)*100)/100);
};

util.setObjectProperties = function(instance, obj){
  for(var prop in obj){
    // console.log(prop, obj.hasOwnProperty(prop));
    if (obj.hasOwnProperty(prop)) {
      var fnSetName = 'set' + prop[0].toUpperCase() + prop.substring(1);
      if(typeof instance[fnSetName] === 'function'){
        instance[fnSetName](obj[prop]);
      }else{
        instance[prop] = obj[prop]; // not sure why, but the skill updates don't have the method.
      }
    }
  }
};

util.addGettersAndSetters = function(clazz){
  var obj = new clazz();
  for(let prop in obj){
    if (obj.hasOwnProperty(prop)) {
      var propUpper = prop[0].toUpperCase() + prop.substring(1)
      var fnSetName = 'set' + propUpper;
      var fnGetName = 'get' + propUpper;
      if(typeof clazz.prototype[fnSetName] === 'undefined'){
        clazz.prototype[fnSetName] = function(value){
          this[prop] = value;
        }
      }
      if(typeof clazz.prototype[fnGetName] === 'undefined'){
        clazz.prototype[fnGetName] = function(){
          return this[prop];
        }
      }
    }
  }
};

util.sleepPromise = function(time=1, result){
  return new Promise((fulfill, reject)=>{
    setTimeout(()=>{
      fulfill(result);
    }, time);
  })
};

export default util;
