'use strict';

class AppError {
    constructor(message, httpCode = 500, appCode, devMessage){
        if(appCode && appCode < 1000) throw 'Invalid values passed to AppError constructor.'
        if(message instanceof AppError){
            this.httpCode = message.httpCode;
            this.appCode = message.appCode;
            this.message = message.message;
            this.developerMessage = message.devMessage;
        }else{
            this.httpCode = httpCode;
            this.appCode = appCode;
            this.message = message;
            this.developerMessage = devMessage;
        }
    }

    toObject(){
        return JSON.parse(JSON.stringify(this));
    }

    sendResponse(res){
        res.status(this.httpCode).json(this.toObject());
    }
}

AppError.codes = {
    // server/app errors
    APP_ERROR: 1000,
    INVALID_UPDATE: 1001,
    UNAUTHORIZED: 1002,
    INVALID_DATA: 1003,
    // user errors
    USER_NOT_FOUND: 2000,
    USER_ALREADY_EXISTS: 2001,
    INVALID_EMAIL_ADDRESS: 2002,
    // account errors
    ACCOUNT_NOT_FOUND: 3000,
    // plaid errors
    // PLAID_EXPIRED_TOKEN: 4000,
    // PLAID_BAD_TOKEN: 4001,
    // PLAID_ACCOUNT_NOT_READY: 4002,
    // lotto errors
    LOTTERY_NOT_FOUND: 5000,
    LOTTERY_EXPIRED: 5001,
    LOTTERY_CANT_MODIFY: 5002, // this happens when users have already enterd
    // lottery entry errors
    LOTTERY_ENTRY_EXISTS: 6000,

    STRIPE_PAYMENT_FAILED: 7000
}

module.exports = AppError;
