import {combineReducers} from 'redux'
import Listing from "../models/Listing";

const userWatchLists = (state = null, action) => {
    if (action.type === 'SET_USER_WATCH_LISTS') {
        action.userWatchLists = action.userWatchLists.map(listing => {
            listing.rentals = listing.rentals.map(r => new Listing(r));
            return listing;
        });
        return action.userWatchLists;
    } else {
        return state;
    }
};

const newUserWatchList = (state = null, action) => {
    if (action.type === 'SET_NEW_USER_WATCH_LIST') {
        return action.newUserWatchList;
    } else {
        return state;
    }
};

const currentPage = (state = null, action) => {
    if (action.type === 'SET_CURRENT_PAGE') {
        return action.currentPage;
    } else {
        return state;
    }
};

const currentPublicPage = (state = null, action) => {
    if (action.type === 'SET_CURRENT_PUBLIC_PAGE') {
        return action.currentPublicPage;
    } else {
        return state;
    }
};

const selectedPlanId = (state = null, action) => {
    if (action.type === 'SET_SELECTED_PLAN_ID') {
        return action.selectedPlanId;
    } else {
        return state;
    }
};

/**
 * currentPlanId is the current plan the user is subscribed to. If user is not subscribed to any
 * plan, it will be null.
 * @param state
 * @param action
 * @returns {currentPlanId|*}
 */
const currentPlanId = (state = null, action) => {
    if (action.type === 'SET_CURRENT_PLAN_ID') {
        return action.currentPlanId;
    } else {
        return state;
    }
};

const selectedUserWatchList = (state = null, action) => {
    if (action.type === 'SET_SELECTED_USER_WATCH_LIST') {
        let newList = {};
        if(state){
            Object.assign(newList, state);
        }
        return Object.assign(newList, action.selectedUserWatchList);
    } else {
        return state;
    }
};

const loggedInUser = (state = null, action) => {
    if (action.type === 'SET_LOGGED_IN_USER') {
        return action.loggedInUser;
    } else {
        return state;
    }
};

const currentPaymentMethod = (state = null, action) => {
    if (action.type === 'SET_CURRENT_PAYMENT_METHOD') {
        return action.currentPaymentMethod;
    } else {
        return state;
    }
};

const paymentMethods = (state = null, action) => {
    if (action.type === 'SET_PAYMENT_METHODS') {
        return action.paymentMethods;
    } else {
        return state;
    }
};

const rootReducer = combineReducers({
    userWatchLists,
    loggedInUser,
    newUserWatchList,
    selectedUserWatchList,
    currentPage,
    currentPublicPage,
    selectedPlanId,
    currentPlanId,
    currentPaymentMethod,
    paymentMethods
});

export default rootReducer
