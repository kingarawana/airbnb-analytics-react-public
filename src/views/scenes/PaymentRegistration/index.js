import React, {forwardRef} from 'react';
import { connect } from 'react-redux';
import mStyles from './index.mcss';
import Checkbox from '../../components/Checkbox';
import planService from "../../../services/planService";
import userAccountManager from "../../../managers/userAccountManager";
import $ from 'jquery';
import emitter from "../../../helpers/emitter";
import CreditCardForm from "../../components/CreditCardForm";
import Modal from "../../components/Modal"
import GetPassword from '../../components/GetPassword';
import {confirmAlert} from "react-confirm-alert";
import {Redirect} from "react-router";
import subRouteService from "../../../services/subRouteService";

/**
 * @param ref
 * @param selectedPlanId
 * @param onSuccessHandler
 * @param onFailureHandler
 * @param defaultValues
 */

class Custom extends React.Component {
    constructor(props){
        super(props);
        let name, email = '';
        if(props.defaultValues){
            email = props.defaultValues.email || '';
            name = props.defaultValues.name || '';
        }
        this.state = {
            name,
            email,
            zip: '',
            checked: false,
            showModal: false,
            redirectToAcct: false
        };

        this._submitHandler = this._submitHandler.bind(this);
        this._changeHandler = this._changeHandler.bind(this);
        this._blurHandler = this._blurHandler.bind(this);
        this._checkedHandler = this._checkedHandler.bind(this);
        this._passwordFailureHandler = this._passwordFailureHandler.bind(this);
        this._passwordSuccessHandler = this._passwordSuccessHandler.bind(this);

        console.log('has child ref', this.props.childRef)
        if(this.props.childRef){
            this.props.childRef.current = this;
        }

        this.checkedRef = React.createRef();
        this.creditCardInfoFormRef = React.createRef();
    }

    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.defaultValues && !prevState.email && !prevState.name){
            let email = nextProps.defaultValues.email || '';
            let name = nextProps.defaultValues.name || '';
            return {name, email};
        }
        return null;
    }

    _showPasswordModal(showModal){
        this.setState({showModal});
    }

    async _submitHandler(){
        // if(this.props.onSuccessHandler){
        //     emitter.showSpinner(true);
        //     setTimeout(()=>{
        //         emitter.showSpinner(false);
        //         emitter.showNotification('success', 'Subscription Success!')
        //         this.props.onSuccessHandler();
        //     }, 3000);
        // }
        // return;
        let myFieldsHaveErrors = this._hasInvalidFields();
        if(myFieldsHaveErrors){
            return emitter.showNotification('warning', 'Please make sure all fields are populated correctly.')
        }
        if(this._checkInvalidCheckBox()){
            return emitter.showNotification('warning', 'Agreeing to our terms and conditions by checking the checkbox is required for signing up.')
        }
        let paymentMethod = await this.creditCardInfoFormRef.current.submit();
        if(paymentMethod.errors){
            paymentMethod.errors.forEach(errObj => {
                emitter.showNotification('warning', errObj.msg);
            });
        }else {
            if(this.props.loggedInUser){
                await this._signUpUser(this.props.loggedInUser, paymentMethod);
                emitter.showNotification('success', `Sign up successful`);
                if(this.props.onSuccessHandler){
                    this.props.onSuccessHandler();
                }
            }else{
                this._showPasswordModal(true);
            }
            this.pm = paymentMethod;
        }
        emitter.showSpinner(false);
    }

    _hasInvalidFields(){
        let ids = ['ccemail'];
        let foundInvalid = false;
        ids.forEach(id => {
            let el = $(`#${id}`);
            if(!el.val()){
                el.addClass('invalid');
                foundInvalid = true;
            }else{
                el.removeClass('invalid');
            }
        });
        let isInvalid = foundInvalid;
        return isInvalid;
    }

    _checkInvalidCheckBox(){
        if(!this.state.checked){
            $(this.checkedRef.current).addClass('invalid');
            return true;
        }
    }

    _changeHandler(e){
        this.setState({[e.target.getAttribute('data-key')]: e.target.value});
    }

    _blurHandler(e){
        if(e.target.value || (!e.target.value && e.target.className.indexOf('invalid'))){
            $(e.target).removeClass('invalid');
        }
        if(e.target.id === 'ccemail' && !!e.target.value){
            let regex = /@.+\./ig;
            if(!regex.test(e.target.value)){
                $(e.target).addClass('invalid');
            }else if(!this.props.loggedInUser){
                userAccountManager.doesUserExistPromise(e.target.value).then(exists => {
                    if(exists){
                        subRouteService.showPlans(true);
                        this._showAlertToRedirect();
                    }
                }).catch(err => {
                    console.log(err);
                });
            }
        }
    }

    _showAlertToRedirect(){
        confirmAlert({
            message: 'Looks like you already have an account. Please sign in to change your subscription.',
            buttons: [
                {
                    label: 'OK',
                    onClick: async () => {
                        this.setState({redirectToAcct: true});
                    }
                }
            ]
        });
    }
    // this is needed by Modal
    willDismissHandler(){
        console.log('called simisssss')
        this.setState({email: ''});
        if(this.creditCardInfoFormRef.current){
            this.creditCardInfoFormRef.current.willDismissHandler();
        }

        Array.prototype.forEach.call($('input.invalid'), el => {
            $(el).removeClass('invalid') ;
        });
    }

    _checkedHandler(checked){
        if(checked){
            $(this.checkedRef.current).removeClass('invalid');
        }
        this.setState({checked})
    }

    _passwordFailureHandler(){
        console.log('failed getting password');
    }

    async _passwordSuccessHandler(pw){
        this._showPasswordModal(false);
        emitter.showSpinner(true);
        let paymentMethod = this.pm;
        if(paymentMethod){
            let email = this.state.email;
            let cognitoUser = await userAccountManager.registerNewCognitoUser(email, pw, paymentMethod.name);
            await this._signUpUser(cognitoUser, paymentMethod);
            emitter.showNotification('success', `Sign up successful`);
            if(this.props.onSuccessHandler){
                this.props.onSuccessHandler();
            }
        }else{
            emitter.showNotification('error', 'Error creating your account. Please contact us.')
        }
        emitter.showSpinner(false);
    }

    async _signUpUser(cognitoUser, paymentMethod){
        paymentMethod.userId = cognitoUser.userId;
        paymentMethod.email = this.state.email;
        await userAccountManager.createPaymentMethodAndStartSubscription(paymentMethod.userId, paymentMethod, this.props.selectedPlanId);
    }

    render() {
        if(this.state.redirectToAcct){
            return <Redirect to={'/admin/account'}/>
        }
        let plan = planService.getPlanById(this.props.selectedPlanId);
        if(!plan){
            return <div/>;
        }
        return (
            <div className={mStyles.container}>
                <div className={mStyles.row}>
                    <button onClick={this._submitHandler} type="button" className={'btn btn-outline-danger btn-lg btn-block ' + mStyles.reg_button}>Start Your Membership</button>
                </div>
                <div className={mStyles.row}>
                    <div className={mStyles.agree_container} ref={this.checkedRef}>
                        <div style={{marginRight: '10px'}}>
                            <Checkbox onClick={this._checkedHandler} checked={this.state.checked}/>
                        </div>
                        <div>
                            By starting my Membership, I confirm that I have read and agree to the Shorrent Terms of Service, Privacy Policy, and Membership Terms, and I authorize Shorrent to charge my payment method for the monthly membership as described above.
                        </div>
                    </div>
                </div>
                <div className={mStyles.row}>
                    <div className={mStyles.package_container}>
                        <div className={mStyles.package_period}>
                            <span>30-DAY FREE TRIAL</span>
                        </div>
                        <div className={mStyles.package_title}>
                            <div>
                                {`Shorrent ${plan.title} Membership`}
                            </div>
                            <div style={{marginLeft: 'auto'}}>
                                {`$${plan.price}/month`}
                            </div>
                        </div>
                        <div className={mStyles.package_body}>
                            {`You are required to enter your credit card to initiate the free trial, but you will not be charged until after the 30 day free trial period. Your Shorrent Membership will renew automatically at $${plan.price}/month (exclusive of VAT or other taxes) until you cancel. Cancel anytime here. No cash value. Shorrent reserves the right to cancel or limit the promotion in its sole discretion at any time. Void where prohibited.`}
                        </div>
                    </div>
                </div>
                <div className={mStyles.row + ' ' + mStyles.cc_info}>
                    <CreditCardForm defaultValues={{name: this.state.name}} ref={this.creditCardInfoFormRef}/>
                </div>
                <div className={mStyles.row} style={{display: this.props.loggedInUser ? 'none' : 'block'}}>
                    <div className={mStyles.subtitle}>Email</div>
                    <div className="float_input_wrap">
                        <input type="text" id={'ccemail'} data-key={'email'} value={this.state.email} onChange={this._changeHandler} onBlur={this._blurHandler} autoComplete={'on'} className="float_input form-control" required/>
                        <span className="float_label">Email address</span>
                    </div>
                </div>
                <div className={mStyles.row} style={{textAlign: 'center'}}>
                    <h2>{`Try Shorrent ${plan.title} Free for 30 days`}</h2>
                </div>
                <Modal show={this.state.showModal} style={{width: '30%', top: '50px', minWidth: '400px'}}
                       onExitHandler={this._showPasswordModal.bind(this, false)}
                >
                    <GetPassword
                        onSuccess={this._passwordSuccessHandler}
                        onValidationError={errObjs => errObjs.forEach(obj => emitter.showNotification(obj.type, obj.msg))}
                    />
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        // selectedPlanId: state.selectedPlanId,
        loggedInUser: state.loggedInUser
    }
};

const mapDispatchToProps = dispatch => {
    return {
    }
};

Custom = connect(
    mapStateToProps,
    mapDispatchToProps
)(Custom);

let Stripped = forwardRef((props, ref) => {
    return (
        <Custom childRef={ref} {...props}/>
    );
});

export default Stripped;