import React from 'react'
import {connect} from "react-redux";
import mStyles from './index.mcss';
import ShowOneCard from "../../components/CreditCardIcons/ShowOneCard";
import ReactTooltip from "react-tooltip";
import Modal from '../../components/Modal';
import GatherCreditCardInfo from '../GatherCreditCardInfo';
import { confirmAlert } from 'react-confirm-alert';
import emitter from "../../../helpers/emitter";
import userAccountManager from "../../../managers/userAccountManager";

class Custom extends React.Component {
    constructor() {
        super();
        this.state = {
            showModal: false,
            editPm: null,
            editName: null,
            editMmYy: null
        };
        this._toggleModal = this._toggleModal.bind(this);
        this._cancelEdit = this._cancelEdit.bind(this);
        this._saveEdit = this._saveEdit.bind(this);
        this._editChangeHandler = this._editChangeHandler.bind(this);
        this._addCardCompleteHandler = this._addCardCompleteHandler.bind(this);
    }

    _toggleModal(){
        this.setState({showModal: !this.state.showModal});
    }

    _makePaymentMethodDefault(pm){
        if(pm.isDefault) return;
        confirmAlert({
            title: 'Please Confirm',
            message: 'Please confirm you want to switch your default payment method.',
            buttons: [
                {
                    label: 'Cancel'
                },
                {
                    label: 'SWITCH',
                    onClick: async () => {
                        emitter.showSpinner(true);
                        await userAccountManager.markAsDefaultPromise(this.props.loggedInUser.userId, pm._id);
                        emitter.showSpinner(false);
                        emitter.showNotification('success', `Default Payment method updated successfully.`)
                    }
                }
            ]
        });
    }

    _deletePaymentMethod(pm){
        if(pm.isDefault) return;
        confirmAlert({
            title: 'Are you sure?',
            message: 'Are you sure you want to delete this payment method?',
            buttons: [
                {
                    label: 'Cancel'
                },
                {
                    label: 'DELETE',
                    onClick: async () => {
                        emitter.showSpinner(true);
                        await userAccountManager.deletePaymentMethodPromise(this.props.loggedInUser.userId, pm._id);
                        emitter.showSpinner(false);
                        emitter.showNotification('success', `Payment method deleted.`)
                    }
                }
            ]
        });
    }

    _editPaymentMethod(editPm){
        this.setState({editPm, editName: editPm.name, editMmYy: editPm.getMmYy()});
    }

    _editChangeHandler(e){
        let key = e.target.name;
        let val = e.target.value;
        if(key === 'editMmYy'){
            val = val.replace(/[^0-9\/]/g, '');
            if(val.length === 2 && this.state.editMmYy.length < 3 && val[1] !== '/'){
                val += '/';
            }else if(val.length === 4 && val[3] === '/'){
                val = val.substr(0, 3);
            }else if(val.length === 2 && val[1] === '/'){
                val = '0' + val;
            }else if(val.length === 3 && val[2] !== '/' && val[1] !== '/'){
                val = val.substr(0, 2) + '/' + val.substr(2, 2);
            }
            if(val[0] === '/'){
                val = val.substr(1);
            }
            if(val.length > 5){
                val = val.substr(0, 5);
            }
            this.setState({editMmYy: val});
        }else{
            this.setState({[key]: val});
        }
    }

    _areEditsValid(){
        let re = /[0-9]{2}\/{1}[0-9]{2}/g;
        if(!re.test(this.state.editMmYy)){
            emitter.showNotification('error', 'Invalid expire date. Must be in MM/YY format');
            return false;
        }
        if(parseInt(this.state.editMmYy.substr(0, 2)) > 12){
            emitter.showNotification('error', 'Invalid expire Month (MM) must be less than 12');
            return false;
        }
        if(!this.state.editName){
            emitter.showNotification('error', 'Invalid name');
            return false;
        }
        return true;
    }

    _cancelEdit(){
        this.setState({editPm: null});
    }

    _getChangedInfo(){
        let pm = this.state.editPm;
        let updates = {};
        if(pm.name !== this.state.editName){
            updates['name'] = this.state.editName;
        }
        if(pm.getMmYy() !== this.state.editMmYy){
            let parts = this.state.editMmYy.split('/');
            let month = parseInt(parts[0]);
            if(parts[1].length === 2){
                parts[1] = '20'+parts[1];
            }
            let year = parseInt(parts[1]);
            updates['expireMonth'] = month;
            updates['expireYear'] = year;
        }
        if(Object.keys(updates).length){
            return updates;
        }
    }

    async _saveEdit(){
        if(this._areEditsValid()){
            // if(this.state.name == this.state.editPm.name &&)
            emitter.showSpinner(true);
            let updates = this._getChangedInfo();
            if(updates){
                await userAccountManager.updatePaymentMethodInfo(this.state.editPm, this._getChangedInfo());
            }
            this.setState({editPm: null, editName: null, editMmYy: null});
            emitter.showSpinner(false);
            emitter.showNotification('success', 'Credit info updated.');
        }
    }

    _addCardCompleteHandler(){
        this.setState({showModal: !this.state.showModal});
        emitter.showNotification('success', 'Card added successfully and is now the default payment method!');
    }

    render(){
        if(this.props.paymentMethods === undefined || this.props.currentPaymentMethod === undefined){
            return <div>Loading..</div>
        }
        let selClass = mStyles.selected_row;
        let cp = this.props.currentPaymentMethod;
        let pms = this.props.paymentMethods;
        return (
            <div className={mStyles.container}>
                <div className={mStyles.head}>
                    Payment Methods
                </div>
                <div className={mStyles.body}>
                    <section style={{marginTop: '20px'}}>
                        <table>
                            <colgroup>
                                <col width={"200"}/>
                                <col width={"400"}/>
                            </colgroup>
                            <tbody>
                            <tr>
                                <td><b>Default Payment Method:</b></td>
                                {
                                    cp ? (
                                        <td><ShowOneCard type={cp.cardBrandId}/> {cp.cardBrand} ending in <i>{cp.last4}</i></td>
                                    ) : (
                                        <td><b>None</b></td>
                                    )
                                }
                            </tr>
                            </tbody>
                        </table>
                    </section>
                    <section className={mStyles.add_card_row}>
                        <button onClick={this._toggleModal} className={'btn btn-secondary'}>Add New Card</button>
                    </section>
                    {
                        pms && pms.length ? (
                            <section>
                                <table className="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Credit Card</th>
                                        <th scope="col">Name on card</th>
                                        <th scope="col">Expires on</th>
                                        <th scope="col"/>
                                    </tr>
                                    </thead>
                                    <colgroup>
                                        <col width={"340"}/>
                                        <col width={"230"}/>
                                    </colgroup>
                                    <tbody className={mStyles.table_body}>
                                    {
                                        pms.map(pm => {
                                            return (
                                                <tr key={pm._id} className={pm.isDefault ? selClass : ''}>
                                                    <td><ShowOneCard type={pm.cardBrandId}/> {pm.cardBrand} ending in <i>{pm.last4}</i></td>
                                                    <td>
                                                        {
                                                            this.state.editPm && this.state.editPm._id === pm._id ? (
                                                                <input name={'editName'} onChange={this._editChangeHandler} className={'form-control ' + mStyles.name_input} type={'text'} value={this.state.editName}/>
                                                            ) : (
                                                                `${pm.name}`
                                                            )
                                                        }
                                                    </td>
                                                    <td>
                                                        {
                                                            this.state.editPm && this.state.editPm._id === pm._id ? (
                                                                <input name={'editMmYy'} onChange={this._editChangeHandler} className={'form-control ' + mStyles.expire_input} type={'text'} value={this.state.editMmYy}/>
                                                            ) : (
                                                                pm.getMmYy()
                                                            )
                                                        }
                                                    </td>

                                                    <td className={mStyles.action_container}>
                                                        {
                                                            this.state.editPm && this.state.editPm._id === pm._id ? (
                                                                <span className={mStyles.save_cancel_container}>
                                                          <button onClick={this._cancelEdit} className={'btn btn-secondary'}>Cancel</button>
                                                          <button onClick={this._saveEdit} className={'btn btn-success'}>Save</button>
                                                      </span>
                                                            ) : (
                                                                <span>
                                                          <span onClick={this._editPaymentMethod.bind(this, pm)} className={mStyles.action}>Edit</span>
                                                          <span onClick={this._deletePaymentMethod.bind(this, pm)} className={mStyles.action + (pm.isDefault ? (' ' + mStyles.disabled) : '')} data-tip={pm.isDefault ? DEL_MSG : ''}>Delete</span>
                                                          <span onClick={this._makePaymentMethodDefault.bind(this, pm)} className={mStyles.action + (pm.isDefault ? (' ' + mStyles.disabled) : '')} data-tip={pm.isDefault ? CUR_DEF_MSG : ''}>{pm.isDefault ? 'CURRENT DEFAULT' : 'Make Default'}</span>
                                                      </span>
                                                            )
                                                        }
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }
                                    </tbody>
                                </table>
                            </section>
                        ) : null
                    }
                </div>
                <Modal style={{width: '40%', top: '50px', minWidth: '400px'}} show={this.state.showModal} onExitHandler={this._toggleModal}>
                    <GatherCreditCardInfo
                        title={"Add New Credit Card"}
                        onCancel={this._toggleModal}
                        onAddCardComplete={this._addCardCompleteHandler}
                    />
                </Modal>
                <ReactTooltip multiline={true}/>
            </div>
        )
    }
}

const DEL_MSG = 'You must make another card your default payment method before deleting this one or add a new card as default payment method.';
const CUR_DEF_MSG = 'This card is already your current default payment method';

const mapStateToProps = state => {
    return {
        currentPaymentMethod: state.currentPaymentMethod,
        paymentMethods: state.paymentMethods,
        loggedInUser: state.loggedInUser
    }
};

const mapDispatchToProps = dispatch => {
    return {
    }
};

Custom = connect(
    mapStateToProps,
    mapDispatchToProps
)(Custom);

export default Custom