import React from 'react';
import { connect } from 'react-redux';
import mStyles from './index.mcss';
import {Link, Redirect} from "react-router-dom";
import AddListingsByUrlsComponent from "../CreateNewWatchList/AddListingsByUrlsComponent";
import $ from 'jquery';
import {setSelectedUserWatchList, deleteWatchList,
    updateWatchList, setNewUserWatchList} from "../../../actions";
import Octicon, {X} from '@primer/octicons-react';
import { confirmAlert } from 'react-confirm-alert';
import emitter from "../../../helpers/emitter";
import dataService from "../../../services/dataService";
import util from "../../../helpers/util";

class Custom extends React.Component {
    constructor(props){
        super(props);
        let selectedListId = null;
        if(props.selectedUserWatchList){
            selectedListId = props.selectedUserWatchList.id;
        }else if(props.userWatchLists && props.userWatchLists.length){
            let selectedList = props.userWatchLists[0];
            selectedListId = selectedList.id;
        }
        let mappedWatchList = null;
        if(props.userWatchLists){
            mappedWatchList = _mapWatchList(props.userWatchLists);
        }
        this.state = {
            isInEditState: false,
            selectedListId: selectedListId,
            mappedWatchList,
            watchListName: null,
            rentals: null,
        };

        this._showDeleteDialog = this._showDeleteDialog.bind(this);
        this._saveUpdatesHandler = this._saveUpdatesHandler.bind(this);
    }

    static getDerivedStateFromProps(nextProps, prevState){
        let nextState = null;
        if((!prevState.selectedUserWatchList && nextProps.selectedUserWatchList) ||
            (nextProps.selectedUserWatchList && nextProps.selectedUserWatchList.id !== prevState.selectedUserWatchList.id)){
            nextState = {selectedListId: nextProps.selectedUserWatchList.id};
        }
        // this is a bit inefficient, it calls _mapWatchList and setState every time the prop is set.
        if(nextProps.userWatchLists){
            nextState = nextState || {};
            nextState['mappedWatchList'] = _mapWatchList(nextProps.userWatchLists);
        }

        return nextState;
    }

    componentDidMount() {
        if(!this.props.selectedUserWatchList && this.props.userWatchLists && this.props.userWatchLists.length){
            this.props.setSelectedUserWatchList(this.props.userWatchLists[0]);
        }
    }

    async _optionSelected(e){
        let id = e.target.value;
        let watchList = this._getWatchListById(id);
        emitter.showSpinner(true);
        await dataService.populateWatchListWithAppDataIfNeeded(watchList);
        dataService.populateWatchListWithStatsIfNeeded(watchList);
        await this.props.setSelectedUserWatchList(watchList);
        emitter.showSpinner(false);
    }

    _getWatchListById(listId){
        if(this.state.mappedWatchList){
            return this.state.mappedWatchList[listId];
        }
        return null;
    }

    _onAddListingCloseHandler(){
        this._showModal(false);
    }

    _showModal(show=true){
        $(document).keyup(e => {
            if (e.keyCode === 27) { // escape key maps to keycode `27`
                this._showModal(false);
            }
        });
        if(show){
            this._changeEditState(false);
        }
        let shouldShow = show ? 'show' : 'hide';
        $('#add-listing-modal').modal(shouldShow);
    }

    _changeEditState(isInEditState){
        if(isInEditState){
            this._storePreEditState();
        }
        this.setState({isInEditState});
    }

    _storePreEditState(){
        let list = this.props.selectedUserWatchList;
        this.setState({watchListName: list.name, rentals: list.rentals});
    }

    _showDeleteDialog(){
        confirmAlert({
            title: 'Are you sure?',
            message: 'Are you sure you want to delete your list?',
            buttons: [
                {
                    label: 'Cancel'
                },
                {
                    label: 'DELETE',
                    onClick: async () => {
                        let watchList = this.props.selectedUserWatchList;
                        await this.props.deleteWatchList(this.state.selectedListId);
                        emitter.showNotification('info', `[${watchList.name}] deleted successfully.`)
                    }
                }
            ]
        });
    }

    async _saveUpdatesHandler(){
        let watchList = this.props.selectedUserWatchList;
        if(watchList.name !== this.state.watchListName ||
            watchList.rentals.length !== this.state.rentals.length
        ){
            emitter.showSpinner(true);
            watchList.name = this.state.watchListName;
            watchList.rentals = this.state.rentals;
            dataService.rePopulateWatchListWithStats(watchList);
            await this.props.updateWatchList(watchList);
            emitter.showSpinner(false);
            emitter.showNotification('info', 'Update successful');
        }
        this._changeEditState(false);
    }

    _removeRow(rentalId){
        this.setState({rentals: this.state.rentals.filter(r => r.id !== rentalId)});
    }

    _onNameChangeHandler(e){
        this.setState({watchListName: e.target.value});
    }

    render() {
        if(!this.props.userWatchLists || !this.props.selectedUserWatchList){
            // show nothing if the above items don't exist yet
            return <div/>
        }
        if(!this.props.userWatchLists.length){
            return <Redirect to="/admin/spy/create"/>
        }
        let rentals = this.props.selectedUserWatchList ? this.props.selectedUserWatchList.rentals : [];
        if(this.state.isInEditState){
            rentals = this.state.rentals;
        }
        let stats = this.props.selectedUserWatchList ? this.props.selectedUserWatchList.stats : null;
        return (
            <div className={mStyles.container}>
                <div className={mStyles.container_row}>
                    <Link className={'btn btn-light'} style={{float: 'right'}} to={'/admin/spy/create'}>Create New Spy List</Link>
                </div>
                <div className={mStyles.container_row}>
                    {
                        this.state.isInEditState && this.props.selectedUserWatchList ? (
                            <input onChange={this._onNameChangeHandler.bind(this)} className="form-control" aria-label="Large" style={{width: '25%', minWidth: '200px'}} value={this.state.watchListName}/>
                        ) : (
                            <select className={'form-control'} style={{width: '300px'}} value={this.state.selectedListId} onChange={this._optionSelected.bind(this)}>
                                {
                                    this.props.userWatchLists.map(list => {
                                        return (
                                            <option key={list.id} value={list.id}>{list.name}</option>
                                        )
                                    })
                                }
                            </select>
                        )
                    }
                </div>
                <div className={mStyles.container_row}>
                    {this.state.isInEditState ? <button className={'btn btn-success'} onClick={this._saveUpdatesHandler} style={{marginRight: '1rem'}}>Save</button> : <span onClick={this._changeEditState.bind(this, true)} className={mStyles.links} style={{marginRight: '1rem'}}>edit</span>}
                    {this.state.isInEditState ? <button className={'btn btn-secondary'} onClick={this._changeEditState.bind(this, false)} style={{marginRight: '1rem'}}>Cancel</button> : <span onClick={this._showDeleteDialog} className={mStyles.links}>delete</span>}
                </div>
                <div className={mStyles.container_row}>
                    <div style={{overflow:'auto', maxHeight: '800px'}}>
                        <table className="table" style={{position: 'relative'}}>
                            <thead>
                            <tr>
                                {this.state.isInEditState ? <th scope="col" className={mStyles.table_th} style={{width: '50px'}}>Delete</th> : null}
                                <th className={mStyles.table_th} style={{lineHeight: '2rem'}} scope="col">Competitor</th>
                                {
                                    rentals.length > 0 && rentals[0].data ? (rentals[0].data.calendar.map(date => {
                                        return <th className={mStyles.table_th} key={date.date} scope="col">
                                            <div className={mStyles.date_container}>
                                                <div>{date.date.replace('2020-', '')}</div>
                                                <div className={mStyles.date_day}>{util.getWeekdayName(date.date)}</div>
                                            </div>
                                        </th>
                                    })) : null
                                }
                            </tr>
                            </thead>
                            <tbody>
                            <tr style={{backgroundColor:'#F0F0F0'}}>
                                {this.state.isInEditState ? <th scope="col" style={{width: '50px', backgroundColor: 'gainsboro'}}> </th> : null}
                                <td className={mStyles.stats_label}>
                                    <div className={mStyles.stats_label}>% Booked</div>
                                    <div className={mStyles.stats_label}>Min Price</div>
                                    <div className={mStyles.stats_label}>Max Price</div>
                                    <div className={mStyles.stats_label}>Avg Price</div>
                                    <div className={mStyles.stats_label}>Median Price</div>
                                </td>
                                {
                                    stats ? (
                                        stats.map((stat, idx) => {
                                            return (
                                                <td key={`stats_label_${idx}`}>
                                                    <div className={mStyles.stats_data}>{stat.percentBooked}%</div>
                                                    <div className={mStyles.stats_data}>${stat.min}</div>
                                                    <div className={mStyles.stats_data}>${stat.max}</div>
                                                    <div className={mStyles.stats_data}>${stat.mean}</div>
                                                    <div className={mStyles.stats_data}>${stat.median}</div>
                                                </td>
                                            )
                                        })
                                    ) : null
                                }
                            </tr>
                            {
                                rentals.length > 0 && rentals[0].data ? (rentals.map((rental, index) => {
                                    return (
                                        <tr key={rental.id}>
                                            {this.state.isInEditState ? <td><span onClick={this._removeRow.bind(this, rental.id)} style={{cursor: 'pointer', backgroundColor: 'red', padding: '.7em', margin: '.3em'}}><Octicon icon={X}/></span></td> : null}
                                            <td>{index+1}</td>
                                            {
                                                rental.data.calendar.map((date, index) => {
                                                    return <td key={index} className={!date.available ? mStyles.booked_cell : ''}>{date.price.local_price_formatted}</td>
                                                    // return <td key={index} className={Math.random() > .5 ? mStyles.booked_cell : ''}>{date.price.local_price_formatted}</td>
                                                })
                                            }
                                        </tr>)
                                })) : null
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className={mStyles.container_row}>
                    <button className={"btn btn-light"} onClick={this._showModal.bind(this, true)} style={{float: 'right'}}>Add New Rental</button>
                </div>
                <div id="add-listing-modal" className="modal fade" data-backdrop="false">
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header">
                                <AddListingsByUrlsComponent
                                    onBackHandler={this._showModal.bind(this, false)}
                                    onNextHandler={this._onAddListingCloseHandler.bind(this)}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function _mapWatchList(watchList){
    return watchList.reduce((prev, curr)=>{
        prev[curr.id] = curr;
        return prev;
    }, {});
}

const mapStateToProps = state => {
    return {
        userWatchLists: state.userWatchLists,
        selectedUserWatchList: state.selectedUserWatchList
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setSelectedUserWatchList: selectedUserWatchList => {
            return dispatch(setSelectedUserWatchList(selectedUserWatchList))
        },
        deleteWatchList: watchListId => {
            return dispatch(deleteWatchList(watchListId))
        },
        updateWatchList: watchList => {
            return dispatch(updateWatchList(watchList))
        },
        setNewUserWatchList: watchList => {
            return dispatch(setNewUserWatchList(watchList))
        },
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Custom);