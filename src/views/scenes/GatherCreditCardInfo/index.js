import React, {createRef, forwardRef} from 'react';
import { connect } from 'react-redux';
import mStyles from './index.mcss';
import userAccountManager from "../../../managers/userAccountManager";
import emitter from "../../../helpers/emitter";
import CreditCardForm from "../../components/CreditCardForm";

class Custom extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            name: '',
            checked: false
        };
        this._addCardHandler = this._addCardHandler.bind(this);
        this._dismissHandler = this._dismissHandler.bind(this);
    }

    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.defaultValues && !prevState.name){
            let name = nextProps.defaultValues.name || '';
            return {name};
        }
        if(nextProps.loggedInUser && !prevState.name){
            let name = nextProps.loggedInUser.name || '';
            return {name};
        }
        return null;
    }

    async _addCardHandler(){
        emitter.showSpinner(true);
        let paymentMethod = await this.props.childRef.current.submit();
        console.log('the created PM', paymentMethod);
        if(paymentMethod.errors){
            paymentMethod.errors.forEach(errObj => {
               emitter.showNotification('warning', errObj.msg);
            });
        }else{
            paymentMethod.userId = this.props.loggedInUser.userId;
            paymentMethod.email = this.props.loggedInUser.email;
            await userAccountManager.createPaymentMethodForUserPromise(paymentMethod.userId, paymentMethod, true);
            emitter.showSpinner(false);
            if(this.props.onAddCardComplete){
                this.props.onAddCardComplete();
            }
            /**
             *  TODO: make API service call to charge user
             *      - if user doesn't yet, I will need to create a user
             *        Then charge them.
             *      - if user already exist, just crate a payment method and charge the user
             **/
        }
        emitter.showSpinner(false);
    };

    _dismissHandler(){
        if(this.props.onCancel){
            this.props.onCancel();
        }
    }

    render() {
        return (
            <div className={mStyles.container}>
                <div className={mStyles.row + ' ' + mStyles.row_buttons}>
                    <button onClick={this._dismissHandler} className={'btn btn-secondary'}>Cancel</button>
                    <button onClick={this._addCardHandler} className={'btn btn-primary'}>Add Card</button>
                </div>
                <div className={mStyles.row}>
                    <CreditCardForm defaultValues={{name: this.state.name}} ref={this.props.childRef}/>
                </div>
                {
                    this.props.title ? (
                        <div className={mStyles.row + ' ' + mStyles.title}>
                            {this.props.title}
                        </div>
                    ) : null
                }
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        loggedInUser: state.loggedInUser
    }
};

const mapDispatchToProps = dispatch => {
    return {
    }
};

Custom = connect(
    mapStateToProps,
    mapDispatchToProps
)(Custom);

const Forwarded = forwardRef((props, ref) => {
    return (
        <Custom childRef={ref} {...props} />
    );
});

export default Forwarded;