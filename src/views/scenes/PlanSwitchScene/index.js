import React from 'react'
import {connect} from "react-redux";
import {setSelectedPlanId} from "../../../actions";
import SubscriptionPlans from "../../components/SubscriptionPlans";
import Modal from "../../components/Modal";
import apiService from "../../../services/apiService";
import userAccountManager from "../../../managers/userAccountManager";
import SubscriptionChangeConfirmation from '../../components/SubscriptionChangeConfirmation';
import planService from "../../../services/planService";
import {confirmAlert} from "react-confirm-alert";
import emitter from "../../../helpers/emitter";
import PaymentRegistration from "../PaymentRegistration";

/**
 * props: onPlanSwitchSuccess, onPlanSwitchFailure,
 */
class Custom extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
            prorateData: null,
            prorateCost: null,
            selectedPlanId: null
        };

        this._onCancelHandler = this._onCancelHandler.bind(this);
        this._packageSelectedHandler = this._packageSelectedHandler.bind(this);
        this._onConfirmHandler = this._onConfirmHandler.bind(this);
        this._cancelSubscriptionHandler = this._cancelSubscriptionHandler.bind(this);
        this._cancelHandler = this._cancelHandler.bind(this);
        this._onPaymentSuccessHandler = this._onPaymentSuccessHandler.bind(this);
        this._onPaymentFailureHandler = this._onPaymentFailureHandler.bind(this);
    }

    async _packageSelectedHandler(selectedPlanId){
        if(selectedPlanId === planService.getBasicFreePlanId() && this.props.currentPlanId !== planService.getBasicFreePlanId()){
            confirmAlert({
                title: 'Are you sure?',
                message: `Are you sure you want to cancel your ${planService.getPlanTitleById(this.props.currentPlanId)} subscription and switch to the free plan?`,
                buttons: [
                    {
                        label: 'No'
                    },
                    {
                        label: 'Yes - Cancel Plan',
                        onClick: this._cancelHandler
                    }
                ]
            });
        }else{
            emitter.showSpinner(true);
            let prorateData = await apiService.getProrateAmount(this.props.loggedInUser.userId, selectedPlanId);
            emitter.showSpinner(false);
            console.log('prorateData', prorateData);
            this.setState({prorateData, showModal: true, selectedPlanId});
        }

    }

    async _onConfirmHandler(){
        emitter.showSpinner(true);
        console.log('this.state.prorateData', this.state.prorateData)
        try{
            if(this.state.prorateData && this.state.prorateData.t){
                await userAccountManager.changeSubscriptionForExistingUser(this.state.selectedPlanId, this.state.prorateData.t);
                emitter.showNotification('success', 'Subscription changed success!');
            }else{
                await userAccountManager.startSubscriptionForExistingUser(this.state.selectedPlanId);
                emitter.showNotification('success', 'Subscription started!');
            }
            if(this.props.onPlanSwitchSuccess){
                this.props.onPlanSwitchSuccess();
            }
        }catch(err){
            if(this.props.onPlanSwitchFailure){
                this.props.onPlanSwitchFailure();
            }
            emitter.showNotification('error', 'Subscription change failed. Please try again later or contact us.');
        }
        emitter.showSpinner(false);
        this._showModal(false);
    }

    async _onPaymentSuccessHandler(){
        this._showModal(false);
        if(this.props.onPlanSwitchSuccess){
            this.props.onPlanSwitchSuccess();
        }
    }

    async _onPaymentFailureHandler(){
        emitter.showNotification('error', 'Subscription failed. Please contact us!');
        this._showModal(false);
        if(this.props.onPlanSwitchFailure){
            this.props.onPlanSwitchFailure();
        }
    }

    _onCancelHandler(){
        this._showModal(false);
    }

    _showModal(show=true){
        this.setState({showModal: show});
    }

    _cancelSubscriptionHandler(){
        confirmAlert({
            title: 'Are you sure?',
            message: `Are you sure you want to cancel your ${planService.getPlanTitleById(this.props.currentPlanId)} subscription?`,
            buttons: [
                {
                    label: 'No'
                },
                {
                    label: 'Yes - Cancel Plan',
                    onClick: this._cancelHandler
                }
            ]
        });
    }

    async _cancelHandler(){
        emitter.showSpinner(true);
        let currentPlanId = this.props.currentPlanId;
        await userAccountManager.cancelSubscriptionForUser(this.props.loggedInUser.userId);
        emitter.showNotification('info', `${planService.getPlanTitleById(currentPlanId)} plan cancelled successfully.`);
        emitter.showSpinner(false);
    }

    render(){
        let {userId, name, email} = this.props.loggedInUser ? this.props.loggedInUser : {};
        // if(this.props.loggedInUser){
        //     userId = this.props.loggedInUser
        // }
        // this could be a potential bug if we ever allow users to remove all their payment methods.
        // let paymentCustomerId = this.props.loggedInUser ? this.props.loggedInUser.paymentCustomerId : null;
        return (
            <div style={{width: '100%'}}>
                <SubscriptionPlans onPlanSelected={this._packageSelectedHandler} cancelHandler={this._cancelSubscriptionHandler}/>
                <Modal style={{...(this.props.currentPaymentMethod ? {width: '600px'} : {}), top: '50px'}} show={this.state.showModal} onExitHandler={this._modalExistHandler}>
                    {
                        this.props.currentPaymentMethod ? (
                            <SubscriptionChangeConfirmation
                                userId={userId}
                                fromPlanId={this.props.currentPlanId}
                                toPlanId={this.state.selectedPlanId}
                                prorateData={this.state.prorateData}
                                onConfirmHandler={this._onConfirmHandler}
                                onCancelHandler={this._onCancelHandler}
                            />
                        ) : (
                            <PaymentRegistration
                                selectedPlanId={this.state.selectedPlanId}
                                onSuccessHandler={this._onPaymentSuccessHandler}
                                onFailureHandler={this._onPaymentFailureHandler}
                                defaultValues={{email, name}}
                            />
                        )
                    }

                </Modal>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        selectedPlanId: state.selectedPlanId,
        loggedInUser: state.loggedInUser,
        currentPlanId: state.currentPlanId,
        currentPaymentMethod: state.currentPaymentMethod
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setSelectedPlanId: planId => {
            dispatch(setSelectedPlanId(planId))
        }
    }
};

Custom = connect(
    mapStateToProps,
    mapDispatchToProps
)(Custom);

export default Custom