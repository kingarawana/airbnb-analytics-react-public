import React from 'react'
import { connect } from 'react-redux'
import mStyles from './index.mcss'
import ReactTooltip from 'react-tooltip'
import { setNewUserWatchList } from "../../../actions";
import emitter from "../../../helpers/emitter";

class Custom extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            name: props.newUserWatchList ? props.newUserWatchList.name : ''
        };
    }

    _changeHandler(event){
        this.setState({name: event.target.value})
    }

    _nextHandler(){
        if(!this.state.name){
            emitter.showNotification('info', 'Please enter a name for your watch list.')
        }else{
            this.props.setNewUserWatchList({name: this.state.name});
            this.props.onNextHandler();
        }
    }

    render() {
        return (
            <div id={mStyles.spy_container}>
                <div className={mStyles.spy_row}>
                    <p style={{fontSize: '1.5rem'}}>Create a New Watch List</p>
                </div>
                <div className={mStyles.spy_row + ' input-group-lg'}>
                    <input type={"text"} style={{width: '350px'}} onChange={this._changeHandler.bind(this)} value={this.state.name} data-tip="This is the name of your new watch list. You can create several watch lists in the future." placeholder={"Enter a name for your new watch list"} className="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"/>
                </div>
                <div className={mStyles.spy_row}>
                    <button className={"btn btn-lg btn-primary"} onClick={this._nextHandler.bind(this)}>Next</button>
                </div>
                <ReactTooltip multiline={true}/>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        newUserWatchList: state.newUserWatchList
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setNewUserWatchList: newUserWatchList => {
            return dispatch(setNewUserWatchList(newUserWatchList))
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Custom);