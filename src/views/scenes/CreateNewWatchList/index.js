import React from 'react'
import { connect } from 'react-redux'
import EnterNameComponent from "./EnterNameComponent";
import AddListingsByUrlsComponent from "./AddListingsByUrlsComponent";
import {Redirect} from "react-router-dom";
import {setNewUserWatchList} from "../../../actions";

class Custom extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            currentStep: 1
        };
        this._onNextHandler = this._onNextHandler.bind(this);
        this._onBackHandler = this._onBackHandler.bind(this);
    }

    _onNextHandler(){
        let currentStep = this.state.currentStep + 1;
        this.setState({ currentStep })

    }

    _onBackHandler(){
        let currentStep = this.state.currentStep - 1;
        this.setState({ currentStep })
    }

    componentWillUnmount() {
        this.props.setNewUserWatchList(null);
    }

    render() {
        let currentStep = String(this.state.currentStep);
        if(currentStep === '3'){
            let to = this.props.match.url.replace('create', 'view');
            return <Redirect to={to}/>
        }
        return (
            <div>
                {
                    {
                        '1': <EnterNameComponent onNextHandler={this._onNextHandler}/>,
                        '2': <AddListingsByUrlsComponent onNextHandler={this._onNextHandler} onBackHandler={this._onBackHandler}/>,
                    }[currentStep]
                }
            </div>

            // <CreateNewWatchLists/>

        )
    }
}

const mapStateToProps = state => {
    return {
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setNewUserWatchList: newUserWatchList => {
            return dispatch(setNewUserWatchList(newUserWatchList))
        },
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Custom);