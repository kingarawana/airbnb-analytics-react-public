import React from 'react'
import { connect } from 'react-redux'
import mStyles from './index.mcss'
import ReactTooltip from 'react-tooltip'
import util from '../../../helpers/util'
import emitter from "../../../helpers/emitter"
import apiService from "../../../services/apiService";
import dataService from "../../../services/dataService";
import Listing from "../../../models/Listing";
import {
    setNewUserWatchList,
    setSelectedUserWatchList,
    createWatchList,
    updateWatchList
} from "../../../actions";

class Custom extends React.Component {
    constructor(props){
        super(props);
        let listings = [];
        if(props.newUserWatchList){
            if(props.newUserWatchList.rentals){
                listings = [...props.newUserWatchList.rentals];
            }else{
                listings = [];
            }
        }else if(props.selectedUserWatchList){
            listings = [...props.selectedUserWatchList.rentals];
        }

        this.state = {
            url: '',
            pasted: false,
            listings,
        };
    }

    static getDerivedStateFromProps(nextProps, prevState){
        /**
         * when newUserWatchList is set, we don't need to do anything b/c by default a new list won't have
         * any data by definition - it's new.
         */
        if(nextProps.newUserWatchList){
            return null;
        }

        if(!prevState.listings.length && nextProps.selectedUserWatchList){
            let listings = [...nextProps.selectedUserWatchList.rentals];
            return {listings}
        }
        return null;
    }

    async _addHandler(){
        let value = this.state.url;
        if(value){
            if(isNaN(value)){
                value = this._prepareUrlValue(value);
                value = util.extractListingIdFromUrl(value);
            }
            await this._addListingIdHandler(value);
        }else{
            emitter.showNotification('error', `Please enter a valid airbnb listing URL or listing ID.`);
        }
    }

    _prepareUrlValue(value){
        value = value.trim();
        if(value.slice(0, 4) !== 'http'){
            value = 'http://' + value;
        }
        return value;
    }

    _isValidListingId(listingId){
        if(!listingId || isNaN(listingId)) return false;
        return String(listingId).length >= 3;
    }

    _listingExist(listingId){
        listingId = parseInt(listingId);
        for(let l of this.state.listings){
            if(l.id === listingId){
                return true;
            }
        }
    }

    _addListing(listing){
        let listings = [...this.state.listings];
        listings.push(listing);
        console.log('the pushed listings', listings);
        // setTimeout(()=>this.setState({ listings: listings }), 100)
        this.setState({ listings: listings });
    }

    async _addListingIdHandler(listingId){
        if(this._listingExist(listingId)){
            return emitter.showNotification('error', `This listing already exists. [${listingId}].`);
        }
        if(!this._isValidListingId(listingId)){
            return emitter.showNotification('error', `Invalid URL/ID. [${listingId}].`);
        }

        emitter.showSpinner(true);
        let listingData = await apiService.getListingDataById(listingId);
        // let listingData = {"id":1688018,"city":"Los Angeles","thumbnail_url":"https://a0.muscache.com/im/pictures/30542b66-cc94-449c-a5c0-898e15e4685d.jpg?aki_policy=small","medium_url":"https://a0.muscache.com/im/pictures/30542b66-cc94-449c-a5c0-898e15e4685d.jpg?aki_policy=medium","user_id":3844352,"picture_url":"https://a0.muscache.com/im/pictures/30542b66-cc94-449c-a5c0-898e15e4685d.jpg?aki_policy=large","xl_picture_url":"https://a0.muscache.com/im/pictures/30542b66-cc94-449c-a5c0-898e15e4685d.jpg?aki_policy=x_large","price":85,"native_currency":"USD","price_native":85,"price_formatted":"$85","lat":34.10626,"lng":-118.23431,"country":"United States","name":"The NELA Yurt - Mt. Washington","smart_location":"Los Angeles, CA","has_double_blind_reviews":false,"instant_bookable":true,"bathrooms":1,"bedrooms":1,"beds":1,"market":"Los Angeles","min_nights":31,"neighborhood":"Glassell Park","person_capacity":4,"state":"CA","zipcode":"90065","user":{"user":{"id":3844352,"first_name":"Carrick","picture_url":"https://a0.muscache.com/im/pictures/user/468196bc-ff78-40d1-85b9-bffddc0a0ea1.jpg?aki_policy=profile_x_medium","thumbnail_url":"https://a0.muscache.com/im/pictures/user/468196bc-ff78-40d1-85b9-bffddc0a0ea1.jpg?aki_policy=profile_small","has_profile_pic":true,"created_at":"2012-10-12T07:16:20Z","reviewee_count":597,"recommendation_count":3,"last_name":"","thumbnail_medium_url":"https://a0.muscache.com/im/pictures/user/468196bc-ff78-40d1-85b9-bffddc0a0ea1.jpg?aki_policy=profile_medium","picture_large_url":"https://a0.muscache.com/im/pictures/user/468196bc-ff78-40d1-85b9-bffddc0a0ea1.jpg?aki_policy=profile_large","response_time":"within an hour","response_rate":"100%","acceptance_rate":"97%","wishlists_count":13,"publicly_visible_wishlists_count":4,"is_superhost":true}},"hosts":[{"id":3844352,"first_name":"Carrick","picture_url":"https://a0.muscache.com/im/pictures/user/468196bc-ff78-40d1-85b9-bffddc0a0ea1.jpg?aki_policy=profile_x_medium","thumbnail_url":"https://a0.muscache.com/im/pictures/user/468196bc-ff78-40d1-85b9-bffddc0a0ea1.jpg?aki_policy=profile_small","has_profile_pic":true}],"primary_host":{"id":3844352,"first_name":"Carrick","picture_url":"https://a0.muscache.com/im/pictures/user/468196bc-ff78-40d1-85b9-bffddc0a0ea1.jpg?aki_policy=profile_x_medium","thumbnail_url":"https://a0.muscache.com/im/pictures/user/468196bc-ff78-40d1-85b9-bffddc0a0ea1.jpg?aki_policy=profile_small","has_profile_pic":true,"created_at":"2012-10-12T07:16:20Z","reviewee_count":597,"recommendation_count":3,"last_name":"","thumbnail_medium_url":"https://a0.muscache.com/im/pictures/user/468196bc-ff78-40d1-85b9-bffddc0a0ea1.jpg?aki_policy=profile_medium","picture_large_url":"https://a0.muscache.com/im/pictures/user/468196bc-ff78-40d1-85b9-bffddc0a0ea1.jpg?aki_policy=profile_large","response_time":"within an hour","response_rate":"100%","acceptance_rate":"97%","wishlists_count":13,"publicly_visible_wishlists_count":4,"is_superhost":true},"address":"Glassell Park, Los Angeles, CA 90065, United States","country_code":"US","cancellation_policy":"strict_14_with_grace_period","property_type":"Yurt","reviews_count":362,"room_type":"Entire home/apt","room_type_category":"entire_home","bathroom_type":"private","picture_urls":["https://a0.muscache.com/im/pictures/30542b66-cc94-449c-a5c0-898e15e4685d.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/74058638/c56f4551_original.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/c2c560b8-236c-46da-8209-891ab8ff1329.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/7976e598-9a95-46ae-b6e0-1d0be7db92ef.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/dae6dc77-5654-492f-ad4c-151defbf3802.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/d6b02bd6-572c-449e-a1ca-704bd6f6d1ab.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/64ed3d46-7c57-4564-9afe-08b5d6cfd6f5.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/9c8bb566-80a9-4c15-b1a6-fb6224943591.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/0b6ba50b-3815-40fb-ac18-7c08a8dbcb4c.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/e7956585-1508-4437-9cea-9512acf14833.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/2d31732c-ff9b-4c16-97ff-48cd9be985a9.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/191199cf-8db8-429c-9857-7f7aee305631.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/7d0eb7be-c837-4284-a4a9-aed9137737e5.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/d7078aa5-c4fc-481c-85bb-21dfa4999fbb.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/e59da726-80f1-4b5c-b484-3f495e261e98.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/b5c18e20-e9e6-4f88-83ba-d26ed4d6e12f.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/b680cd79-e349-47db-979c-eb920f90027c.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/2c0c618f-989d-413a-b1e6-ae3d22ae8614.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/f3537651-9a8b-47c9-8eb9-223b659e9352.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/7b31f637-410c-4a8b-9041-946ab160855f.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/19c695bd-7556-4751-a164-ded906f20b54.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/74559156/db75c45b_original.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/26084800/880db1cb_original.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/fd579d76-ecdb-4000-b7a1-3bcc5a064aed.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/e433442c-e837-4930-9086-82f3f8f2b3a9.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/221adc8e-e89a-40cb-ba28-341681ba6ef7.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/7cfe6ccf-f0c6-42b9-b5ff-fad311d25f57.jpg?aki_policy=large","https://a0.muscache.com/im/pictures/0c8e1888-b964-42cf-87c6-f81134c167c4.jpg?aki_policy=large"],"thumbnail_urls":["https://a0.muscache.com/im/pictures/30542b66-cc94-449c-a5c0-898e15e4685d.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/74058638/c56f4551_original.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/c2c560b8-236c-46da-8209-891ab8ff1329.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/7976e598-9a95-46ae-b6e0-1d0be7db92ef.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/dae6dc77-5654-492f-ad4c-151defbf3802.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/d6b02bd6-572c-449e-a1ca-704bd6f6d1ab.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/64ed3d46-7c57-4564-9afe-08b5d6cfd6f5.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/9c8bb566-80a9-4c15-b1a6-fb6224943591.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/0b6ba50b-3815-40fb-ac18-7c08a8dbcb4c.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/e7956585-1508-4437-9cea-9512acf14833.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/2d31732c-ff9b-4c16-97ff-48cd9be985a9.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/191199cf-8db8-429c-9857-7f7aee305631.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/7d0eb7be-c837-4284-a4a9-aed9137737e5.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/d7078aa5-c4fc-481c-85bb-21dfa4999fbb.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/e59da726-80f1-4b5c-b484-3f495e261e98.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/b5c18e20-e9e6-4f88-83ba-d26ed4d6e12f.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/b680cd79-e349-47db-979c-eb920f90027c.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/2c0c618f-989d-413a-b1e6-ae3d22ae8614.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/f3537651-9a8b-47c9-8eb9-223b659e9352.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/7b31f637-410c-4a8b-9041-946ab160855f.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/19c695bd-7556-4751-a164-ded906f20b54.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/74559156/db75c45b_original.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/26084800/880db1cb_original.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/fd579d76-ecdb-4000-b7a1-3bcc5a064aed.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/e433442c-e837-4930-9086-82f3f8f2b3a9.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/221adc8e-e89a-40cb-ba28-341681ba6ef7.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/7cfe6ccf-f0c6-42b9-b5ff-fad311d25f57.jpg?aki_policy=small","https://a0.muscache.com/im/pictures/0c8e1888-b964-42cf-87c6-f81134c167c4.jpg?aki_policy=small"],"xl_picture_urls":["https://a0.muscache.com/im/pictures/30542b66-cc94-449c-a5c0-898e15e4685d.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/74058638/c56f4551_original.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/c2c560b8-236c-46da-8209-891ab8ff1329.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/7976e598-9a95-46ae-b6e0-1d0be7db92ef.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/dae6dc77-5654-492f-ad4c-151defbf3802.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/d6b02bd6-572c-449e-a1ca-704bd6f6d1ab.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/64ed3d46-7c57-4564-9afe-08b5d6cfd6f5.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/9c8bb566-80a9-4c15-b1a6-fb6224943591.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/0b6ba50b-3815-40fb-ac18-7c08a8dbcb4c.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/e7956585-1508-4437-9cea-9512acf14833.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/2d31732c-ff9b-4c16-97ff-48cd9be985a9.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/191199cf-8db8-429c-9857-7f7aee305631.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/7d0eb7be-c837-4284-a4a9-aed9137737e5.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/d7078aa5-c4fc-481c-85bb-21dfa4999fbb.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/e59da726-80f1-4b5c-b484-3f495e261e98.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/b5c18e20-e9e6-4f88-83ba-d26ed4d6e12f.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/b680cd79-e349-47db-979c-eb920f90027c.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/2c0c618f-989d-413a-b1e6-ae3d22ae8614.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/f3537651-9a8b-47c9-8eb9-223b659e9352.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/7b31f637-410c-4a8b-9041-946ab160855f.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/19c695bd-7556-4751-a164-ded906f20b54.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/74559156/db75c45b_original.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/26084800/880db1cb_original.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/fd579d76-ecdb-4000-b7a1-3bcc5a064aed.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/e433442c-e837-4930-9086-82f3f8f2b3a9.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/221adc8e-e89a-40cb-ba28-341681ba6ef7.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/7cfe6ccf-f0c6-42b9-b5ff-fad311d25f57.jpg?aki_policy=x_large","https://a0.muscache.com/im/pictures/0c8e1888-b964-42cf-87c6-f81134c167c4.jpg?aki_policy=x_large"],"picture_count":28,"currency_symbol_left":"$","currency_symbol_right":null,"picture_captions":["","The yurt looking north (March 2015)","Super comfy bed under the dome skylight. ","The dome skylight as seen from the bed, a great sight to open your eyes to.","","The yurt kitchenette: toaster, mini-fridge, cold water sink, electric tea kettle with french press and Stumptown coffee.","Twin robes for the walk to the outdoor shower.","Wurlitzer organ for vibe control. Sounds great! Bose speaker to play along with your beats.","Rattan pendant lamp.","Green inside and out.","A great spot to write.","Oversized mirror above the sink.","A necessary accessory for hikes and walks in Southern California.","The view from the yurt doorway: hammock and loungers between the lemon and orange trees.","The yurt bathroom as seen from the doorway of the yurt, with the barn (also on Airbnb) in the background.","The separate yurt bathroom with full toilet, sink, and spa-style outdoor shower. Just steps away from the yurt.","The interior of the separate yurt bathroom.","River rocks underfoot in the outdoor shower.","Bird’s eye view of the outdoor shower—big enough for two.","Spa-style outdoor shower with rain-can shower head. Enclosed in a redwood fence for privacy.","Redwood bench inside the outdoor shower enclosure.","The yurt looking south.","Warm toes by the fire-pit.","Yurt Floor Plan","The Cazador Barn, next to the yurt and also on Airbnb.","The welcoming committee. Our very friendly Border Collie puppy, Delta.","View from nearby Elyria Canyon Park.",""],"bed_type":"Real Bed","bed_type_category":"real_bed","require_guest_profile_picture":false,"require_guest_phone_verification":false,"force_mobile_legal_modal":false,"allowed_currencies":["AED","ARS","AUD","AWG","BAM","BBD","BGN","BHD","BND","BRL","BTN","BZD","CAD","CHF","CLP","CNY","COP","CRC","CZK","DKK","EUR","FJD","GBP","GTQ","GYD","HKD","HNL","HRK","HUF","IDR","ILS","JMD","JOD","JPY","KRW","LAK","LKR","MAD","MOP","MXN","MYR","NOK","NZD","OMR","PEN","PGK","PHP","PLN","RON","RUB","SAR","SEK","SGD","THB","TRY","TTD","TWD","UAH","USD","UYU","VND","ZAR"],"cancel_policy":44,"check_in_time":15,"check_in_time_ends_at":-1,"check_out_time":11,"guests_included":2,"license":null,"max_nights":90,"square_feet":null,"locale":"en","has_viewed_terms":true,"has_viewed_cleaning":null,"has_agreed_to_legal_terms":true,"has_viewed_ib_perf_dashboard_panel":true,"localized_city":"Los Angeles","language":"en","public_address":"Los Angeles, CA, United States","map_image_url":"https://maps.googleapis.com/maps/api/staticmap?maptype=roadmap&markers=34.10626%2C-118.23431&size=480x320&zoom=15&client=gme-airbnbinc&channel=monorail-prod&signature=BHYraatOvhp7mHU5bxCN1T9RUec%3D","has_license":false,"experiences_offered":"none","max_nights_input_value":90,"min_nights_input_value":31,"requires_license":true,"property_type_id":15,"house_rules":"--PET OWNERS: if you want to bring a pet, please select \"additional guest\" when you book your reservation and do a little extra cleanup afterwards\n--PHOTOGRAPHERS/FILMMAKERS: I love having photoshoots in the yurt, but it will be a separate fee. Just ask\n--Try to keep noise to a minimum\n--Don't hesitate to text me if you have a problem/concern/question\n--If you want to have more than a couple friends over, just ask\n--If you want to use the fire-pit, please let me know so I can set it up. Out of respect for my neighbors I ask that the fire be put out by 10:30 on week nights and 11:30pm on weekends.","cleaning_fee_native":45,"extras_price_native":45,"security_deposit_native":200,"security_price_native":200,"security_deposit_formatted":"$200","description":"When a friend is thinking of moving to Los Angeles, people make sure to bring them to my house. The yurt is the most compelling part of the conversion experience. Nestled among 30 fruit trees on a half-acre lot, it's classic bohemian California.  Note: It’s a pet friendly place, but please read about my pet charge (under house rules) and let me know if you plan to bring a pet.\n\n*PLEASE READ PET CHARGE POLICY (UNDER HOUSE RULES) & CANCELLATION POLICY BEFORE BOOKING*\n\nStaying in the yurt is definitely \"glamping\" (a more luxurious version of camping). If you usually like to stay at the Marriott, this rustic spot may not be right for you. Once inside, you wouldn't guess that you're only a few miles from Hollywood and Downtown Los Angeles. A yurt is the Mongolian version of a teepee, invented by nomads in the steppes of Central Asia. Unlike the traditional version, this yurt sits on a raised platform with hardwood floors, windows, and modern amenities. The circular room is 16 feet in diameter, and the conical roof has a skylight directly over the bed. The setting is a rural oasis in the middle of the city: the yurt sits on a half-acre lot which it shares with a 1920's farm house (where I live), and a little red barn (which is also on Airbnb). There are several hammocks hung around the yard and a firepit that's great for campfire sing-alongs. Just outside the yurt are some mature fig trees, a lemon tree, an avocado tree, and an orange tree (to name a few). Ten steps across the yard is the outdoor bathroom with a toilet, sink and outdoor shower. You stand on rocks from the LA river beneath a giant \"raincan\" shower head that is big enough for two. Above you are tree branches and the sky. The tankless water heater allows infinitely long hot showers.\n\nPET OWNERS: I love pets (especially dogs)! If you are bringing one, please select \"additional guest\" when you book and do a little extra cleanup afterwards.\n\nPHOTOGRAPHERS: I love having photoshoots in the yurt, but it will be a separate fee. Just ask!\n\n--Internet (Wifi and hard-wired)\n--Electric Tea Kettle, French Press, Toaster Oven, Tea and Coffee\n--Mini-Refrigerator\n--Fruit from the fruit trees (seasonal)\n--Wurlitzer Organ\n--Wood-Burning Stove\n--Swamp Cooler (eco AC)\n--Hammocks\n--Outdoor  chairs and hammocks\n--Fire-pit\n\nMy interaction with guests will be minimal. Although  I may bump into you occasionally, we'll give you as much privacy as we can. Since I live in another house on the property, I'm always around if you need anything. Texting is best, but you can call or knock on my door if it's urgent.\n\nGlassell Park is a sleepy gem of a neighborhood about three miles from Silver Lake and Echo Park, four miles from downtown, and four miles from Hollywood. It's part of a vast, hilly, residential area that includes Eagle Rock, Mount Washington, Highland Park, and South Pasadena. If you head northeast, you can wander for hours through the narrow winding streets without hitting a freeway. It's in the process of gentrification, but is still extremely diverse, with lots of authentic Mexican and Salvadoran food. There are some awesome bars (Verdugo Bar and Los Candiles), and one of the best brunch places in L.A. (Lemon Poppy Kitchen). There are several great parks. Elyria Canyon has some short, beautiful hikes. Rio Los Angeles and Glassell Park Recreation center have tennis and basketball courts, an outdoor pool (open year-round), soccer, baseball fields. One of the most beautiful spots in the neighborhood is the L.A. river, which in the section closest to us is still wild. It has truly great bird-watching, a ten mile bike path, and just this year opened for kayaking!  Griffith Park and Elysian park are also just across the river. If you aren't familiar with L.A., the East Side (where we are) has really been blossoming lately, with downtown, Silverlake, Echo Park and Highland Park dominating on the nightlife and foodie fronts.\n\nMy house is a couple blocks from buses that go all over the city, and just two miles from the Lincloln/Cypress Gold Line Subway stop. It's close to the 2, 5, and 110 freeways.\n\nNote: I recommend you bring flip flops and a bathrobe for the walk to the outdoor shower.","description_locale":"en","summary":"When a friend is thinking of moving to Los Angeles, people make sure to bring them to my house. The yurt is the most compelling part of the conversion experience. Nestled among 30 fruit trees on a half-acre lot, it's classic bohemian California.  Note: It’s a pet friendly place, but please read about my pet charge (under house rules) and let me know if you plan to bring a pet.","space":"*PLEASE READ PET CHARGE POLICY (UNDER HOUSE RULES) & CANCELLATION POLICY BEFORE BOOKING*\n\nStaying in the yurt is definitely \"glamping\" (a more luxurious version of camping). If you usually like to stay at the Marriott, this rustic spot may not be right for you. Once inside, you wouldn't guess that you're only a few miles from Hollywood and Downtown Los Angeles. A yurt is the Mongolian version of a teepee, invented by nomads in the steppes of Central Asia. Unlike the traditional version, this yurt sits on a raised platform with hardwood floors, windows, and modern amenities. The circular room is 16 feet in diameter, and the conical roof has a skylight directly over the bed. The setting is a rural oasis in the middle of the city: the yurt sits on a half-acre lot which it shares with a 1920's farm house (where I live), and a little red barn (which is also on Airbnb). There are several hammocks hung around the yard and a firepit that's great for campfire sing-alongs. Just outside the yurt are some mature fig trees, a lemon tree, an avocado tree, and an orange tree (to name a few). Ten steps across the yard is the outdoor bathroom with a toilet, sink and outdoor shower. You stand on rocks from the LA river beneath a giant \"raincan\" shower head that is big enough for two. Above you are tree branches and the sky. The tankless water heater allows infinitely long hot showers.\n\nPET OWNERS: I love pets (especially dogs)! If you are bringing one, please select \"additional guest\" when you book and do a little extra cleanup afterwards.\n\nPHOTOGRAPHERS: I love having photoshoots in the yurt, but it will be a separate fee. Just ask!","access":"--Internet (Wifi and hard-wired)\n--Electric Tea Kettle, French Press, Toaster Oven, Tea and Coffee\n--Mini-Refrigerator\n--Fruit from the fruit trees (seasonal)\n--Wurlitzer Organ\n--Wood-Burning Stove\n--Swamp Cooler (eco AC)\n--Hammocks\n--Outdoor  chairs and hammocks\n--Fire-pit","interaction":"My interaction with guests will be minimal. Although  I may bump into you occasionally, we'll give you as much privacy as we can. Since I live in another house on the property, I'm always around if you need anything. Texting is best, but you can call or knock on my door if it's urgent.","neighborhood_overview":"Glassell Park is a sleepy gem of a neighborhood about three miles from Silver Lake and Echo Park, four miles from downtown, and four miles from Hollywood. It's part of a vast, hilly, residential area that includes Eagle Rock, Mount Washington, Highland Park, and South Pasadena. If you head northeast, you can wander for hours through the narrow winding streets without hitting a freeway. It's in the process of gentrification, but is still extremely diverse, with lots of authentic Mexican and Salvadoran food. There are some awesome bars (Verdugo Bar and Los Candiles), and one of the best brunch places in L.A. (Lemon Poppy Kitchen). There are several great parks. Elyria Canyon has some short, beautiful hikes. Rio Los Angeles and Glassell Park Recreation center have tennis and basketball courts, an outdoor pool (open year-round), soccer, baseball fields. One of the most beautiful spots in the neighborhood is the L.A. river, which in the section closest to us is still wild. It has truly great bird-watching, a ten mile bike path, and just this year opened for kayaking!  Griffith Park and Elysian park are also just across the river. If you aren't familiar with L.A., the East Side (where we are) has really been blossoming lately, with downtown, Silverlake, Echo Park and Highland Park dominating on the nightlife and foodie fronts.","notes":"Note: I recommend you bring flip flops and a bathrobe for the walk to the outdoor shower.","transit":"My house is a couple blocks from buses that go all over the city, and just two miles from the Lincloln/Cypress Gold Line Subway stop. It's close to the 2, 5, and 110 freeways.","amenities":["Internet","Wifi","Free parking on premises","Pets allowed","Free street parking","Indoor fireplace","Heating","Family/kid friendly","Suitable for events","Smoke detector","Carbon monoxide detector","Fire extinguisher","Essentials","Shampoo","24-hour check-in","Hangers","Hair dryer","Iron","Laptop friendly workspace","Hot water","Coffee maker","Refrigerator","Dishes and silverware","Other"],"amenities_ids":[3,4,9,12,23,27,30,31,32,35,36,39,40,41,43,44,45,46,47,77,90,91,93,131],"is_location_exact":true,"in_building":false,"in_landlord_partnership":false,"in_toto_area":false,"recent_review":{"review":{"id":555584294,"reviewer_id":2568652,"reviewee_id":3844352,"created_at":"2019-10-27T22:10:41Z","reviewer":{"user":{"id":2568652,"first_name":"Tom","picture_url":"https://a0.muscache.com/im/pictures/user/4b0abf7e-75e7-4a31-bc39-6716a102e480.jpg?aki_policy=profile_x_medium","thumbnail_url":"https://a0.muscache.com/im/pictures/user/4b0abf7e-75e7-4a31-bc39-6716a102e480.jpg?aki_policy=profile_small","has_profile_pic":true}},"comments":"This yurt is a true hidden gem. For a very relaxing and unique experience in LA, this place is a must. It has everything you need to feel like you're getting away in the \"middle\" of the city. The neighborhood is cozy and safe, and there's plenty of awesome spots close by. Definitely reccomended!","listing_id":1688018}},"calendar_updated_at":"5 days ago","cancel_policy_short_str":"Strict (grace period)","photos":[{"xl_picture":"https://a0.muscache.com/im/pictures/30542b66-cc94-449c-a5c0-898e15e4685d.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/30542b66-cc94-449c-a5c0-898e15e4685d.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/30542b66-cc94-449c-a5c0-898e15e4685d.jpg?aki_policy=small","caption":"","id":742885971,"sort_order":1},{"xl_picture":"https://a0.muscache.com/im/pictures/74058638/c56f4551_original.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/74058638/c56f4551_original.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/74058638/c56f4551_original.jpg?aki_policy=small","caption":"The yurt looking north (March 2015)","id":15568106,"sort_order":2},{"xl_picture":"https://a0.muscache.com/im/pictures/c2c560b8-236c-46da-8209-891ab8ff1329.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/c2c560b8-236c-46da-8209-891ab8ff1329.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/c2c560b8-236c-46da-8209-891ab8ff1329.jpg?aki_policy=small","caption":"Super comfy bed under the dome skylight. ","id":742886126,"sort_order":3},{"xl_picture":"https://a0.muscache.com/im/pictures/7976e598-9a95-46ae-b6e0-1d0be7db92ef.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/7976e598-9a95-46ae-b6e0-1d0be7db92ef.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/7976e598-9a95-46ae-b6e0-1d0be7db92ef.jpg?aki_policy=small","caption":"The dome skylight as seen from the bed, a great sight to open your eyes to.","id":742885670,"sort_order":4},{"xl_picture":"https://a0.muscache.com/im/pictures/dae6dc77-5654-492f-ad4c-151defbf3802.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/dae6dc77-5654-492f-ad4c-151defbf3802.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/dae6dc77-5654-492f-ad4c-151defbf3802.jpg?aki_policy=small","caption":"","id":742885175,"sort_order":5},{"xl_picture":"https://a0.muscache.com/im/pictures/d6b02bd6-572c-449e-a1ca-704bd6f6d1ab.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/d6b02bd6-572c-449e-a1ca-704bd6f6d1ab.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/d6b02bd6-572c-449e-a1ca-704bd6f6d1ab.jpg?aki_policy=small","caption":"The yurt kitchenette: toaster, mini-fridge, cold water sink, electric tea kettle with french press and Stumptown coffee.","id":742885817,"sort_order":6},{"xl_picture":"https://a0.muscache.com/im/pictures/64ed3d46-7c57-4564-9afe-08b5d6cfd6f5.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/64ed3d46-7c57-4564-9afe-08b5d6cfd6f5.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/64ed3d46-7c57-4564-9afe-08b5d6cfd6f5.jpg?aki_policy=small","caption":"Twin robes for the walk to the outdoor shower.","id":700218711,"sort_order":7},{"xl_picture":"https://a0.muscache.com/im/pictures/9c8bb566-80a9-4c15-b1a6-fb6224943591.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/9c8bb566-80a9-4c15-b1a6-fb6224943591.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/9c8bb566-80a9-4c15-b1a6-fb6224943591.jpg?aki_policy=small","caption":"Wurlitzer organ for vibe control. Sounds great! Bose speaker to play along with your beats.","id":742885570,"sort_order":8},{"xl_picture":"https://a0.muscache.com/im/pictures/0b6ba50b-3815-40fb-ac18-7c08a8dbcb4c.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/0b6ba50b-3815-40fb-ac18-7c08a8dbcb4c.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/0b6ba50b-3815-40fb-ac18-7c08a8dbcb4c.jpg?aki_policy=small","caption":"Rattan pendant lamp.","id":742886234,"sort_order":9},{"xl_picture":"https://a0.muscache.com/im/pictures/e7956585-1508-4437-9cea-9512acf14833.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/e7956585-1508-4437-9cea-9512acf14833.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/e7956585-1508-4437-9cea-9512acf14833.jpg?aki_policy=small","caption":"Green inside and out.","id":700217647,"sort_order":10},{"xl_picture":"https://a0.muscache.com/im/pictures/2d31732c-ff9b-4c16-97ff-48cd9be985a9.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/2d31732c-ff9b-4c16-97ff-48cd9be985a9.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/2d31732c-ff9b-4c16-97ff-48cd9be985a9.jpg?aki_policy=small","caption":"A great spot to write.","id":742890470,"sort_order":11},{"xl_picture":"https://a0.muscache.com/im/pictures/191199cf-8db8-429c-9857-7f7aee305631.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/191199cf-8db8-429c-9857-7f7aee305631.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/191199cf-8db8-429c-9857-7f7aee305631.jpg?aki_policy=small","caption":"Oversized mirror above the sink.","id":742886347,"sort_order":12},{"xl_picture":"https://a0.muscache.com/im/pictures/7d0eb7be-c837-4284-a4a9-aed9137737e5.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/7d0eb7be-c837-4284-a4a9-aed9137737e5.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/7d0eb7be-c837-4284-a4a9-aed9137737e5.jpg?aki_policy=small","caption":"A necessary accessory for hikes and walks in Southern California.","id":742885440,"sort_order":13},{"xl_picture":"https://a0.muscache.com/im/pictures/d7078aa5-c4fc-481c-85bb-21dfa4999fbb.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/d7078aa5-c4fc-481c-85bb-21dfa4999fbb.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/d7078aa5-c4fc-481c-85bb-21dfa4999fbb.jpg?aki_policy=small","caption":"The view from the yurt doorway: hammock and loungers between the lemon and orange trees.","id":742891016,"sort_order":14},{"xl_picture":"https://a0.muscache.com/im/pictures/e59da726-80f1-4b5c-b484-3f495e261e98.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/e59da726-80f1-4b5c-b484-3f495e261e98.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/e59da726-80f1-4b5c-b484-3f495e261e98.jpg?aki_policy=small","caption":"The yurt bathroom as seen from the doorway of the yurt, with the barn (also on Airbnb) in the background.","id":742010680,"sort_order":15},{"xl_picture":"https://a0.muscache.com/im/pictures/b5c18e20-e9e6-4f88-83ba-d26ed4d6e12f.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/b5c18e20-e9e6-4f88-83ba-d26ed4d6e12f.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/b5c18e20-e9e6-4f88-83ba-d26ed4d6e12f.jpg?aki_policy=small","caption":"The separate yurt bathroom with full toilet, sink, and spa-style outdoor shower. Just steps away from the yurt.","id":742011881,"sort_order":16},{"xl_picture":"https://a0.muscache.com/im/pictures/b680cd79-e349-47db-979c-eb920f90027c.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/b680cd79-e349-47db-979c-eb920f90027c.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/b680cd79-e349-47db-979c-eb920f90027c.jpg?aki_policy=small","caption":"The interior of the separate yurt bathroom.","id":742011353,"sort_order":17},{"xl_picture":"https://a0.muscache.com/im/pictures/2c0c618f-989d-413a-b1e6-ae3d22ae8614.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/2c0c618f-989d-413a-b1e6-ae3d22ae8614.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/2c0c618f-989d-413a-b1e6-ae3d22ae8614.jpg?aki_policy=small","caption":"River rocks underfoot in the outdoor shower.","id":742011205,"sort_order":18},{"xl_picture":"https://a0.muscache.com/im/pictures/f3537651-9a8b-47c9-8eb9-223b659e9352.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/f3537651-9a8b-47c9-8eb9-223b659e9352.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/f3537651-9a8b-47c9-8eb9-223b659e9352.jpg?aki_policy=small","caption":"Bird’s eye view of the outdoor shower—big enough for two.","id":742010796,"sort_order":19},{"xl_picture":"https://a0.muscache.com/im/pictures/7b31f637-410c-4a8b-9041-946ab160855f.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/7b31f637-410c-4a8b-9041-946ab160855f.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/7b31f637-410c-4a8b-9041-946ab160855f.jpg?aki_policy=small","caption":"Spa-style outdoor shower with rain-can shower head. Enclosed in a redwood fence for privacy.","id":742010932,"sort_order":20},{"xl_picture":"https://a0.muscache.com/im/pictures/19c695bd-7556-4751-a164-ded906f20b54.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/19c695bd-7556-4751-a164-ded906f20b54.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/19c695bd-7556-4751-a164-ded906f20b54.jpg?aki_policy=small","caption":"Redwood bench inside the outdoor shower enclosure.","id":742011080,"sort_order":21},{"xl_picture":"https://a0.muscache.com/im/pictures/74559156/db75c45b_original.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/74559156/db75c45b_original.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/74559156/db75c45b_original.jpg?aki_policy=small","caption":"The yurt looking south.","id":15568108,"sort_order":22},{"xl_picture":"https://a0.muscache.com/im/pictures/26084800/880db1cb_original.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/26084800/880db1cb_original.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/26084800/880db1cb_original.jpg?aki_policy=small","caption":"Warm toes by the fire-pit.","id":15568119,"sort_order":23},{"xl_picture":"https://a0.muscache.com/im/pictures/fd579d76-ecdb-4000-b7a1-3bcc5a064aed.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/fd579d76-ecdb-4000-b7a1-3bcc5a064aed.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/fd579d76-ecdb-4000-b7a1-3bcc5a064aed.jpg?aki_policy=small","caption":"Yurt Floor Plan","id":700233579,"sort_order":24},{"xl_picture":"https://a0.muscache.com/im/pictures/e433442c-e837-4930-9086-82f3f8f2b3a9.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/e433442c-e837-4930-9086-82f3f8f2b3a9.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/e433442c-e837-4930-9086-82f3f8f2b3a9.jpg?aki_policy=small","caption":"The Cazador Barn, next to the yurt and also on Airbnb.","id":414775843,"sort_order":25},{"xl_picture":"https://a0.muscache.com/im/pictures/221adc8e-e89a-40cb-ba28-341681ba6ef7.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/221adc8e-e89a-40cb-ba28-341681ba6ef7.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/221adc8e-e89a-40cb-ba28-341681ba6ef7.jpg?aki_policy=small","caption":"The welcoming committee. Our very friendly Border Collie puppy, Delta.","id":530132694,"sort_order":26},{"xl_picture":"https://a0.muscache.com/im/pictures/7cfe6ccf-f0c6-42b9-b5ff-fad311d25f57.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/7cfe6ccf-f0c6-42b9-b5ff-fad311d25f57.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/7cfe6ccf-f0c6-42b9-b5ff-fad311d25f57.jpg?aki_policy=small","caption":"View from nearby Elyria Canyon Park.","id":616558806,"sort_order":27},{"xl_picture":"https://a0.muscache.com/im/pictures/0c8e1888-b964-42cf-87c6-f81134c167c4.jpg?aki_policy=x_large","picture":"https://a0.muscache.com/ac/pictures/0c8e1888-b964-42cf-87c6-f81134c167c4.jpg?interpolation=lanczos-none&size=large_cover&output-format=jpg&output-quality=70","thumbnail":"https://a0.muscache.com/im/pictures/0c8e1888-b964-42cf-87c6-f81134c167c4.jpg?aki_policy=small","caption":"","id":530133113,"sort_order":28}],"star_rating":5,"jurisdiction_names":"City of Los Angeles, CA","jurisdiction_rollout_names":"City of Los Angeles, CA","price_for_extra_person_native":25,"weekly_price_native":500,"monthly_price_native":1700,"time_zone_name":"America/Los_Angeles","weekly_price_factor":0.98,"monthly_price_factor":0.82,"guest_controls":{"allows_children_as_host":true,"allows_infants_as_host":true,"allows_pets_as_host":true,"allows_smoking_as_host":false,"allows_events_as_host":true,"children_not_allowed_details":null,"id":1688018,"structured_house_rules":["No smoking","Check-in is anytime after 3:00 PM"]},"check_in_time_start":"15","check_in_time_end":"FLEXIBLE","formatted_check_out_time":"11","localized_check_in_time_window":"After 3:00 PM","localized_check_out_time":"11:00 AM"}
        if(listingData){
            let listing = Listing.fromRawData(listingData);
            this._addListing(listing);
            this._clear();
            emitter.showNotification('info', 'Listing added successfully');
        }else{
            emitter.showNotification('error', `Listing with id [${listingId}] not found.`);
        }
        emitter.showSpinner(false);
    }

    async _changeHandler(event){
        let pasted = this.state.pasted;
        let value = event.target.value;

        if(pasted && !isNaN(value)){
            this._addListingIdHandler(value);
        }else if(pasted && value){
            let preparedValue = this._prepareUrlValue(value);
            let listingId = util.extractListingIdFromUrl(preparedValue);
            if(!listingId){
                emitter.showNotification('error', `Invalid URL/ID. [${value}].`);
            }else{
                this._addListingIdHandler(listingId);
            }
        }else if(!pasted){
            this.setState({url: value});
        }

        if(pasted){
            this._clear();
        }
    }

    _clear(){
        this.setState({pasted: false, url: ''});
    }

    _pasteHandler(event){
        this.setState({pasted: true});
    }

    async _nextHandler(){
        if(this.state.listings.length >= 2){
            emitter.showSpinner(true);
            if(this.props.newUserWatchList){
                let userWatchList = {...this.props.newUserWatchList};
                userWatchList.rentals = [...this.state.listings];
                console.log('xxx userWatchList', userWatchList);
                let result = await this.props.createWatchList(userWatchList);
                await dataService.populateWatchListWithAppDataIfNeeded(result);
                dataService.rePopulateWatchListWithStats(result);
                await this.props.setNewUserWatchList(null);
                await this.props.setSelectedUserWatchList(result);
                this.props.onNextHandler();
            }else if(this.props.selectedUserWatchList){
                let userWatchList = {...this.props.selectedUserWatchList};
                // userWatchList.rentals = [...userWatchList.rentals];
                // userWatchList.rentals = userWatchList.rentals.concat(this.state.listings);
                userWatchList.rentals = this.state.listings;
                await dataService.populateWatchListWithAppDataIfNeeded(userWatchList);
                dataService.rePopulateWatchListWithStats(userWatchList);
                await this.props.updateWatchList(userWatchList);
                await this.props.setSelectedUserWatchList(userWatchList);
                this.props.onNextHandler(); // the caller that provided this onNextHandler will probably close a modal.
            }else{
                throw Error('Error!');
            }
            emitter.showSpinner(false);
        }else{
            emitter.showNotification('error', `Please copy and paste at least 2 airbnb listings.`);
        }
    }

    _keyPressedHandler(e){
        if(e.key === 'Enter'){
            if(!e.target.value){
                return emitter.showNotification('error', 'Please paste an airbnb listing url or type in the listing in first.');
            }
            this._pasteHandler();
            this._changeHandler(e);
        }
    }

    render() {
        let name = this.props.newUserWatchList ? this.props.newUserWatchList.name : null;
        if(!name){
            name = this.props.selectedUserWatchList ? this.props.selectedUserWatchList.name : null;
        }
        return (
            <div id={mStyles.spy_container}>
                <div className={mStyles.add_url_row}>
                    <p style={{fontSize: '1.5rem', fontWeight: 'bold', textAlign: 'center'}}>
                        {name}
                    </p>
                </div>
                <div className={mStyles.add_url_row}>
                    <p style={{fontSize: '1.5rem'}}>
                        Go to <a target='_blank' rel='noopener noreferrer' href={'https://www.airbnb.com/s/homes'}>airbnb.com</a> and look for the properties you want to spy on and copy and paste the url to their listing below.
                    </p>
                    <p>
                        Hot keys: Mac: <b>Command+V</b>. Windows: <b>Ctrl+V</b> to paste.
                    </p>
                </div>
                <div className={mStyles.add_url_row}>
                    <div className="input-group input-group-lg mb-3">
                        <input type={"text"} onKeyPress={this._keyPressedHandler.bind(this)} value={this.state.url} onPaste={this._pasteHandler.bind(this)} onChange={this._changeHandler.bind(this)} style={{width: '300px'}} data-tip="Copy and paste the url of your competitors airbnb listing." placeholder={"Paste listing URL here"} className="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"/>
                        {/*<div className="input-group-append">*/}
                        {/*    <button onClick={this._addHandler.bind(this)} className="btn btn-outline-secondary" type="button">Add</button>*/}
                        {/*</div>*/}
                    </div>
                </div>
                <div className={mStyles.add_url_row}>
                    <table className="table">
                        <thead>
                        <tr>
                            <th scope="col">Listing ID</th>
                            <th scope="col">Listing Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.listings.map(listing => {
                              return (
                                  <tr key={listing.id}>
                                      <td>{listing.id}</td>
                                      <td>{listing.title}</td>
                                  </tr>
                              )
                            })
                        }
                        </tbody>
                    </table>
                </div>
                <div className={mStyles.add_url_row}>
                    <button className={"btn btn-lg btn-outline-secondary"} style={{float:'left'}} onClick={this.props.onBackHandler}>Back</button>
                    <button className={"btn btn-lg btn-primary"} style={{float:'right'}} onClick={this._nextHandler.bind(this)}>Next</button>
                </div>
                <ReactTooltip multiline={true}/>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        newUserWatchList: state.newUserWatchList,
        selectedUserWatchList: state.selectedUserWatchList
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setSelectedUserWatchList: selectedUserWatchList => {
            return dispatch(setSelectedUserWatchList(selectedUserWatchList))
        },
        setNewUserWatchList: newUserWatchList => {
            return dispatch(setNewUserWatchList(newUserWatchList))
        },
        updateWatchList: userWatchList => {
            return dispatch(updateWatchList(userWatchList))
        },
        createWatchList: newUserWatchList => {
            return dispatch(createWatchList(newUserWatchList))
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Custom);