import React, {forwardRef} from 'react';
import { connect } from 'react-redux';
import mStyles from './index.mcss';
import CreditCardIcons from '../../components/CreditCardIcons';
import {loadStripe} from '@stripe/stripe-js';
import {Elements} from '@stripe/react-stripe-js';
import {ElementsConsumer} from '@stripe/react-stripe-js';
import {CardNumberElement, CardCvcElement, CardExpiryElement} from '@stripe/react-stripe-js';
import $ from 'jquery';
import PaymentMethod from "../../../models/PaymentMethod";
import config from '../../../config'

const stripePromise = loadStripe(config.payment.publishableKey);

const elStyle = {
    base: {
        fontFamily: 'Roboto, sans-serif',
        fontSize: '16px',
        color: '#495057'
    }
};

const elFonts = [
    {
        src: 'url(https://fonts.googleapis.com/css?family=Roboto&display=swap)',
        family: 'Roboto',
        style: 'normal',
    }
];

class Custom extends React.Component {
    constructor(props){
        super(props);
        let name = '';
        this.state = {
            name,
            zip: '',
            checked: false
        };

        this._changeHandler = this._changeHandler.bind(this);
        this._blurHandler = this._blurHandler.bind(this);
        this.props.childRef.current = this;

        this.cardNameRef = React.createRef();
        this.cardZipRef = React.createRef();
    }

    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.defaultValues && !prevState.name){
            let name = nextProps.defaultValues.name || '';
            return {name};
        }
        return null;
    }

    async submit(){
        let errors = [];
        const {stripe, elements} = this.props;

        if (!stripe || !elements) {
            // Stripe.js has not yet loaded.
            // Make  sure to disable form submission until Stripe.js has loaded.
            errors.push({type:'error', msg: 'Please ensure your device is connected to the internet.'});
            return {errors};
        }
        let myFieldsHaveErrors = this._hasInvalidFields();

        if(myFieldsHaveErrors){
            console.log('myFieldsHaveErrors', myFieldsHaveErrors)
            errors.push({type: 'error', msg: 'Please make sure all fields are populated correctly.'})
        }

        let details = {
            name: this.state.name,
            zip: this.state.zip,
        };

        let extraDetails = {
            address_zip: this.state.zip,
        };

        const card = elements.getElement(CardNumberElement);
        const result = await stripe.createToken(card, extraDetails);

        // avoiding multiple error messages.
        if (!myFieldsHaveErrors && result.error) {
            errors.push({type: 'error', msg: result.error.message});
        }

        if(errors.length){
            return {errors};
        }

        // if the code gets to this point, there are no missing/invalid values/fields.

        if(result.token){
            let pm = PaymentMethod.createFromStripeToken(result.token);
            pm.name = details.name;
            return pm;
        }else{
            // this state probably never happens since we do a `result.error` check above.
            return {errors: [{type: 'error', msg: "Cannot complete action. Please contact us."}]};
        }
    };

    willDismissHandler(){
        const {elements} = this.props;
        elements.getElement(CardNumberElement).clear();
        elements.getElement(CardCvcElement).clear();
        elements.getElement(CardExpiryElement).clear();
        this.setState({zip: '', name: ''});
        Array.prototype.forEach.call($('input.invalid'), el=>{
            $(el).removeClass('invalid') ;
        });
    }

    _hasInvalidFields(){
        let ids = ['ccname', 'cczip'];
        let foundInvalid = false;
        ids.forEach(id => {
            let el = $(`#${id}`);
            console.log('xxxx', id, el.val(), el);
            if(!el.val()){
                el.addClass('invalid');
                foundInvalid = true;
            }else{
                el.removeClass('invalid');
            }
        });
        let isInvalid = foundInvalid;
        // if($('.StripeElement--invalid') || $('.StripeElement--empty')){
        //     isInvalid = true;
        // }
        return isInvalid;
    }

    _changeHandler(e){
        // debugger
        this.setState({[e.target.getAttribute('data-key')]: e.target.value});
    }

    _blurHandler(e){
        // if(e.target.value || (!e.target.value && e.target.className.indexOf('invalid'))){
        if(e.target.value){
            $(e.target).removeClass('invalid');
        }

        if(e.target.id === 'cczip' && !!e.target.value){
            let regex = /[a-zA-Z]/ig;
            if(regex.test(e.target.value)){
                $(e.target).addClass('invalid');
            }
        }
    }

    render() {
        return (
            <div className={mStyles.container}>
                <div className={mStyles.row + ' ' + mStyles.cc_info}>
                    <div style={{display: 'flex', flexDirection: 'row'}}>
                        <div className={mStyles.subtitle}>Credit Card</div>
                        <CreditCardIcons/>
                    </div>
                    <div className="float_input_wrap">
                        <input type="text" id={'ccname'} name="ccname" ref={this.cardNameRef} onChange={this._changeHandler} onBlur={this._blurHandler} data-key={'name'} value={this.state.name} autoComplete="cc-name" className="float_input form-control" required/>
                        <label htmlFor={'name'} className="float_label">Cardholder Name</label>
                    </div>
                    <div className="float_input_wrap">
                        <CardNumberElement className="float_input form-control" options={{placeholder: '', style: elStyle}}/>
                        <label htmlFor={'ccnum'} className="float_label">Credit Card Number</label>
                    </div>
                    <div className={mStyles.cc_details}>
                        <div className="float_input_wrap">
                            <CardExpiryElement className="float_input form-control" options={{placeholder: '', style: elStyle}}/>
                            <label htmlFor={'cc-exp'} className="float_label">Expire (MM/YY)</label>
                        </div>
                        <div className="float_input_wrap">
                            <CardCvcElement className="float_input form-control" options={{placeholder: '', style: elStyle}}/>
                            <label htmlFor={'cvc'} className="float_label">CVC</label>
                        </div>
                        <div className="float_input_wrap">
                            <input type="text" id={'cczip'} data-key={'zip'} ref={this.cardZipRef} value={this.state.zip} onChange={this._changeHandler} onBlur={this._blurHandler} className="float_input form-control" name="ship-zip" required autoComplete="shipping postal-code"/>
                            <label htmlFor={'ship-zip'} className="float_label">Zip</label>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
    }
};

const mapDispatchToProps = dispatch => {
    return {
    }
};

Custom = connect(
    mapStateToProps,
    mapDispatchToProps
)(Custom);

let Stripped = forwardRef((props, ref) => {
    return (
        <Elements stripe={stripePromise} options={{fonts: elFonts}}>
            <ElementsConsumer>
                {({stripe, elements}) => {
                    return <Custom childRef={ref} {...props} stripe={stripe} elements={elements} />
                }}
            </ElementsConsumer>
        </Elements>
    );
});

export default Stripped;