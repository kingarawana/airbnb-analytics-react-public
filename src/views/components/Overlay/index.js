import mStyles from "../../pages/Subscription/index.mcss";
import React from "react";

class Custom extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false
        };
        if(props.theRef){
            props.theRef.current = this;
        }
    }

    show(show){
        this.setState({show});
    }

    render(){
        return <div className={mStyles.modal_overlay} style={{display: this.state.show ? 'block' : 'none'}}/>
    }
}

export default Custom;