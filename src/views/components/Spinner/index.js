import React from "react";
import mStyles from './index.mcss'


export default props => {
    return (
        <div style={{display: props.loading ? '' : 'none'}} className={mStyles.spinner_container}>
            <div className={mStyles.spinner_ring}/>
            <img className={mStyles.spinner_img} alt={'spinner'} src={'/logo48.png'}/>
        </div>

    )
}