import mStyles from "./index.mcss";
import React from "react";
import {SignUp} from "aws-amplify-react";
import $ from "jquery";
import userAccountManager from "../../../managers/userAccountManager";
import emitter from "../../../helpers/emitter";
import {Redirect} from "react-router";

const MIN_PW_LENGTH = 8;
class Custom extends SignUp {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            name: '',
            password: '',
            redirect: false
        };
        this._dupEmail = false;

        this._submitHandler = this._submitHandler.bind(this);
        this._changeHandler = this._changeHandler.bind(this);
        this._blurHandler = this._blurHandler.bind(this);
    }

    _changeHandler(e){
            this.setState({[e.target.getAttribute('name')]: e.target.value});
    }

    _blurHandler(e){
        if(e.target.value || (!e.target.value && e.target.className.indexOf('invalid'))){
            $(e.target).removeClass('invalid');
        }
        if(e.target.name === 'username' && !!e.target.value){
            let regex = /@.+\./ig;
            if(!regex.test(e.target.value)){
                $(e.target).addClass('invalid');
            }else if(!this.props.loggedInUser){
                let targetEl = $(e.target);
                userAccountManager.doesUserExistPromise(e.target.value).then(exists => {
                    if(exists){
                        targetEl.addClass('invalid');
                        emitter.showNotification('warning', 'This email already registered.');
                        this._dupEmail = true;
                    }else{
                        this._dupEmail = false;
                    }
                }).catch(err => {
                    console.log(err);
                });
            }
        }else if(e.target.name === 'password' && e.target.value && e.target.value.length < MIN_PW_LENGTH){
            $(e.target).addClass('invalid');
        }
    }

    _areAllFieldsValid(){
        let result = true;
        if(this._dupEmail){
            emitter.showNotification('warning', 'This email already registered.');
            result = false;
        }
        if(this.state.password.length < MIN_PW_LENGTH){
            emitter.showNotification('warning', 'Passwords must be at least 8 characters long');
            result = false;
        }
        if(this.state.name.length < 3){
            emitter.showNotification('warning', 'Please enter your full name');
            result = false;
        }
        return result;
    }

    async _submitHandler(e){
        if(this._areAllFieldsValid()){
            emitter.showSpinner(true);
            await userAccountManager.registerNewCognitoUser(this.state.username, this.state.password, this.state.name);
            emitter.showSpinner(false);
            this.setState({redirect: true});
        }
    }

    showComponent(theme){
        if(this.state.redirect){
            return <Redirect to={'/admin'}/>
        }
        return (
            <div className={mStyles.outer_container}>
                <div className={mStyles.container}>
                    <div className={mStyles.head}>
                        Create New Account
                    </div>
                    <div className={mStyles.row}>
                        <div>email*</div>
                        <input
                            id="username"
                            key="username"
                            name="username"
                            type="text"
                            placeholder={'Email'}
                            value={this.state.username}
                            onChange={this._changeHandler}
                            onBlur={this._blurHandler}
                        />
                    </div>
                    <div className={mStyles.row}>
                        <div>Full Name*</div>
                        <input
                            id="name"
                            name="name"
                            type="text"
                            placeholder={'First & last name'}
                            value={this.state.name}
                            onChange={this._changeHandler}
                            onBlur={this._blurHandler}
                        />
                    </div>
                    <div className={mStyles.row}>
                        <div>Password*</div>
                        <input
                            id="password"
                            key="password"
                            name="password"
                            type="password"
                            placeholder={'Password'}
                            value={this.state.password}
                            onChange={this._changeHandler}
                            onBlur={this._blurHandler}
                        />
                    </div>
                    <div className={mStyles.row + ' ' + mStyles.footer}>
                        <span>Have an account? <span className={mStyles.sign_in} onClick={()=>super.changeState("signIn")}>Sign In</span></span>
                        <button name={'customSignUp'} onClick={this._submitHandler}>Create Account</button>
                    </div>
                </div>
            </div>
        )
    }

}

export default Custom;