import React from 'react'
import {connect} from "react-redux";
import mStyle from './index.mcss';
import {setSelectedPlanId} from "../../../actions";
import $ from "jquery";

class Custom extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            password: ''
        };

        this._changeHandler = this._changeHandler.bind(this);
        this._submitHandler = this._submitHandler.bind(this);
        this._keyUpHandler = this._keyUpHandler.bind(this);
    }

    _submitHandler(){
        if(this.state.password.length < 8){
            this.props.onValidationError([{type: 'warning', msg: 'password must be at least 8 characters long'}]);
            $('#password').addClass('invalid');
        }else{
            this.props.onSuccess(this.state.password);
        }
    }

    _changeHandler(e){
        this.setState({[e.target.getAttribute('data-key')]: e.target.value});
    }

    _blurHandler(e){
        if(e.target.value || (!e.target.value && e.target.className.indexOf('invalid'))){
            $(e.target).removeClass('invalid');
        }
    }

    _keyUpHandler(e){
        if (e.key === 'Enter') {
            this._submitHandler();
        }
    }

    render(){
        return (
            <div style={{width: '100%'}}>
                <div className={mStyle.title}>Create a password</div>
                <div className={mStyle.form_container}>
                    <div className="float_input_wrap">
                        <input id={'password'} type="password" name="password" onKeyUp={this._keyUpHandler} onChange={this._changeHandler} onBlur={this._blurHandler} data-key={'password'} value={this.state.password} className="float_input form-control" required/>
                        <label htmlFor={'name'} className="float_label">New Password</label>
                    </div>
                    <div className="input-group-append">
                        <span className="input-group-text" onClick={this._submitHandler} >Set Password</span>
                    </div>
                </div>

            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        loggedInUser: state.loggedInUser
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setSelectedPlanId: planId => {
            dispatch(setSelectedPlanId(planId))
        }
    }
};

Custom = connect(
    mapStateToProps,
    mapDispatchToProps
)(Custom);

export default Custom