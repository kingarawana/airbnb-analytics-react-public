import React from "react";
import Visa from '../Visa';
import AmericanExpress from '../AmericanExpress';
import MasterCard from '../MasterCard';
import Discover from '../Discover';
import {CardTypes} from "../../../../classes/Card";

export default ({type}) => {
    let Card = null;
    switch(type){
        case CardTypes.VISA:
            Card = Visa;
            break;
        case CardTypes.AMERICAN_EXPRESS:
            Card = AmericanExpress;
            break;
        case CardTypes.MASTER_CARD:
            Card = MasterCard;
            break;
        case CardTypes.DISCOVER:
            Card = Discover;
            break;
        default:
    }
    return <Card/>
}