import React from "react";
import mStyles from './index.mcss';
import Visa from './Visa'
import AmericanExpress from './AmericanExpress'
import MasterCard from './MasterCard'
import Discover from './Discover'

export default () => {
    return (
        <div className={mStyles.card_icon_container}>
            <AmericanExpress/>
            <Discover/>
            <MasterCard/>
            <Visa/>
        </div>
    )
}