import React, {createRef} from "react";
import $ from "jquery";
import Overlay from "../Overlay";
import mStyles from './index.mcss'
import Octicon, {X} from '@primer/octicons-react';

class Custom extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false
        };
        this.overlayRef = createRef();
        this.childRef = createRef();
        this._modalId = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        this._closeHandler = this._closeHandler.bind(this);
        this._keyUpHandler = this._keyUpHandler.bind(this);
    }

    static getDerivedStateFromProps(props, state) {
        return {showModal: !!props.show};
    }

    _dismissChild(){
        if(this.childRef.current && this.childRef.current.willDismissHandler){
            this.childRef.current.willDismissHandler();
        }
    }

    _keyUpHandler(e){
        if (e.keyCode === 27) { // escape key maps to keycode `27`
            if(this.props.onExitHandler){
                    this.props.onExitHandler();
            }
            this._dismissChild();
            this.overlayRef.current.show(false);
            $(`#${this._modalId}`).modal('hide');
            $(document).unbind('keyup', this._keyUpHandler);
        }
    }

    show(show){
        if(show){
            // let handler = e => {
            //     if (e.keyCode === 27) { // escape key maps to keycode `27`
            //         if(this.props.onExitHandler){
            //             this.props.onExitHandler();
            //         }
            //         this._dismissChild();
            //         this.overlayRef.current.show(false);
            //         $(`#${this._modalId}`).modal('hide');
            //         $(document).unbind('keyup', handler);
            //         handler = undefined;
            //     }
            // };
            $(document).keyup(this._keyUpHandler);
        }else{
            $(document).unbind('keyup', this._keyUpHandler);
            this._dismissChild();
        }

        let shouldShow = show ? 'show' : 'hide';

        $(`#${this._modalId}`).modal(shouldShow);
        if(this.overlayRef.current){
            this.overlayRef.current.show(show);
        }
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        this.show(nextProps.show);
        return true;
    }

    _closeHandler(){
        this.show(false);
        if(this.props.onExitHandler){
            this.props.onExitHandler();
        }
    }

    render(){
        return (
            <div>
                <div id={this._modalId} className="modal fade" data-backdrop="false">
                    <div className="modal-dialog modal-lg" style={ this.props.style ? this.props.style : {} }>
                        <div className={"modal-content " + mStyles.modal_content}>
                            <div className="modal-header">
                                {React.cloneElement(this.props.children, {childRef: this.childRef})}
                                <div onClick={this._closeHandler} className={mStyles.close_button}><Octicon icon={X} size={'medium'}/></div>
                            </div>
                        </div>
                    </div>
                </div>
                <Overlay theRef={this.overlayRef} show={this.state.showModal}/>
            </div>
        )
    }
}

export default Custom;