import React from "react";
import Auth from "@aws-amplify/auth";
import {connect} from "react-redux";
import { setLoggedInUser, setCurrentPage, setCurrentPublicPage } from "../../actions";
import Page, {PAGE_TYPES} from "../../classes/Page";
import {Link} from "react-router-dom";
import userAccountManager from '../../managers/userAccountManager'
import { Redirect } from 'react-router-dom'

class Custom extends React.Component {
    constructor(props) {
        super(props);
    }

    signOut() {
        Auth.signOut().then(() => {
            userAccountManager.logoutUserPromise();
        });
    }

    _clickHandler(pageType){
        this.props.setCurrentPage(pageType);
        console.log('the page type', pageType)
    }

    _clickHomeHandler(pageType){
        this.props.setCurrentPage(pageType);
    }

    componentDidMount() {
        this.props.setCurrentPage(Page.getPageTypeFromPath(window.location.pathname));
    }

    render() {
        if(!userAccountManager.isUserLoggedIn()){
            return <Redirect to={'/login'}/>
        }
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <Link onClick={this._clickHomeHandler.bind(this, PAGE_TYPES.HOME)} className={'navbar-brand'} to={'/'}>Shorrent</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className={`nav-item ${this.props.currentPage === PAGE_TYPES.SPY ? 'active' : ''}`}>
                            <Link onClick={this._clickHandler.bind(this, PAGE_TYPES.SPY)} className={'nav-link'} to={'/admin'}>Spying/Research</Link>
                        </li>
                        <li onClick={this._clickHandler.bind(this, PAGE_TYPES.COMMUNITY)} className={`nav-item ${this.props.currentPage === PAGE_TYPES.COMMUNITY ? 'active' : ''}`}>
                            <Link className={'nav-link'} to={'/admin/answers'}>Community</Link>
                        </li>
                        {/*<li className="nav-item">*/}
                        {/*    <Link className={'nav-link disabled'} to={'/'}>New Feature</Link>*/}
                        {/*</li>*/}
                    </ul>
                    <ul className="navbar-nav">
                        <li className="nav-item dropdown">
                            <span className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {
                                    this.props.loggedInUser ? this.props.loggedInUser.name : ''
                                }
                            </span>
                            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <Link onClick={this._clickHandler.bind(this, PAGE_TYPES.ACCOUNT)} className={'dropdown-item'} to={'/admin/account'}>Account</Link>
                                <div className="dropdown-divider"/>
                                <a className="dropdown-item" onClick={this.signOut.bind(this)}>Sign out</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

const mapStateToPropsAdmin = state => {
    return {
        loggedInUser: state.loggedInUser,
        currentPage: state.currentPage
    }
};

const mapDispatchToPropsAdmin = dispatch => {
    return {
        setLoggedInUser: user => {
            return dispatch(setLoggedInUser(user));
        },
        setCurrentPage: nextCurrentPage => {
            return dispatch(setCurrentPage(nextCurrentPage));
        },
        setCurrentPublicPage: nextCurrentPublicPage => {
            return dispatch(setCurrentPublicPage(nextCurrentPublicPage));
        }
    }
};

export default connect(
    mapStateToPropsAdmin,
    mapDispatchToPropsAdmin
)(Custom);