import React from 'react'
import mStyles from './index.mcss'
import planService from "../../../services/planService";
import {connect} from "react-redux";
import Octicon, {Check} from '@primer/octicons-react';

const PLANS = planService.getPlans();
const FREE_PLAN_ID = planService.getBasicFreePlanId();

class Custom extends React.Component {
    _packageSelectedHandler(selectedPlanId){
        if(this.props.onPlanSelected){
            this.props.onPlanSelected(selectedPlanId);
        }
    }

    render(){
        let currentPlanId = this.props.currentPlanId;
        return (
            <div>
                <div className={mStyles.plans_container}>
                    {
                        PLANS.map(plan => {
                            return (
                                <div key={plan.title} className={mStyles.card}>
                                    <div className={mStyles.card_title}>
                                        {plan.title}
                                    </div>
                                    <div className={mStyles.card_billing_interval}>
                                        {plan.interval}
                                    </div>
                                    <div className={mStyles.card_price}>
                                        $<span>{plan.price}</span>
                                    </div>
                                    <div className={mStyles.card_features_container}>
                                        {
                                            plan.features.map(feature => <div key={feature} className={mStyles.card_feature}>{feature}</div>)
                                        }
                                    </div>
                                    <div className={mStyles.card_button}>
                                        {
                                            plan.id === currentPlanId ? (
                                                <button disabled={true} className={"btn btn-outline-info btn-block " + mStyles.selected_package}>
                                                    <span className={mStyles.current_plan}><Octicon size={'medium'} icon={Check}/></span>
                                                </button>
                                            ) : (
                                                <button onClick={this._packageSelectedHandler.bind(this, plan.id)} className="btn btn-outline-info btn-block">{currentPlanId ? 'Select' : 'Sign Up'}</button>
                                            )
                                        }
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
                {
                    this.props.cancelHandler && currentPlanId && currentPlanId !== FREE_PLAN_ID ? (
                        <span className={mStyles.cancel_subs} onClick={this.props.cancelHandler}>
                            {`Cancel ${planService.getPlanById(currentPlanId).title} Subscription`}
                        </span>
                    ) : null
                }
            </div>
        )
    }
}

/**
 * I may need this. I can either pass the current subsribed plan down, or have it check redux
 * directly.
 * @param state
 * @returns {{}}
 */
const mapStateToProps = state => {
    return {
        currentPlanId: state.currentPlanId,
    }
};

const mapDispatchToProps = dispatch => {
    return {
    }
};

Custom = connect(
    mapStateToProps,
    mapDispatchToProps
)(Custom);

export default Custom