import React from "react";
import Auth from "@aws-amplify/auth";
import {connect} from "react-redux";
import { setLoggedInUser, setCurrentPublicPage } from "../../actions";
import Page, {PAGE_TYPES} from "../../classes/Page";
import {Link} from "react-router-dom";
import userAccountManager from '../../managers/userAccountManager'

class Custom extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pages: Page.getPageObjsForNavBar()
        };
    }

    signOut() {
        Auth.signOut().then(() => {
            userAccountManager.logoutUserPromise();
        }).catch(e => {
            console.log(e);
        });
    }

    _clickHandler(pageType){
        this.props.setCurrentPublicPage(pageType);
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <Link onClick={this._clickHandler.bind(this, PAGE_TYPES.HOME)} className={'navbar-brand'} to={'/'}>Shorrent</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        {
                            this.state.pages.map(page => {
                               return (
                                   <li key={page.name} onClick={this._clickHandler.bind(this, page.type)} className={`nav-item ${this.props.currentPage === page.type ? 'active' : ''}`}>
                                       <Link className={'nav-link'} to={page.path}>{page.name}</Link>
                                   </li>
                               )
                            })
                        }
                    </ul>
                    <form className="form-inline my-2 my-lg-0">
                        {
                            this.props.loggedInUser ? (
                                <span>
                                    <Link onClick={this._clickHandler.bind(this, PAGE_TYPES.ADMIN)} className={'btn btn-outline-success my-2 my-sm-0'} style={{marginRight: '10px'}} to={'/admin/'}>Admin</Link>
                                    <button className="btn btn-outline-danger my-2 my-sm-0" onClick={this.signOut.bind(this)}>Sign out</button>
                                </span>

                            ) : (
                                <Link onClick={this._clickHandler.bind(this, PAGE_TYPES.LOGIN)} className={'btn btn-outline-success my-2 my-sm-0'} to={'/login/'}>Sign In</Link>
                            )
                        }
                    </form>
                </div>
            </nav>
        );
    }
}

const mapStateToPropsAdmin = state => {
    return {
        loggedInUser: state.loggedInUser,
        currentPublicPage: state.currentPublicPage
    }
};

const mapDispatchToPropsAdmin = dispatch => {
    return {
        setLoggedInUser: user => {
            return dispatch(setLoggedInUser(user));
        },
        setCurrentPublicPage: nextCurrentPublicPage => {
            return dispatch(setCurrentPublicPage(nextCurrentPublicPage));
        }
    }
};

export default connect(
    mapStateToPropsAdmin,
    mapDispatchToPropsAdmin
)(Custom);