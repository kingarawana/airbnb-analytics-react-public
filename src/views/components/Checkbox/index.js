import React from "react";

class Custom extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            selected: !!props.checked
        };
        this._clickHandler = this._clickHandler.bind(this);
    }

    _clickHandler(){
        let selected = !this.state.selected;
        this.setState({selected});
        if(this.props.onClick){
            this.props.onClick(selected);
        }
    }

    render(){
        return (
            <div onClick={this._clickHandler} style={{height: '18px', width: '18px', 'minWidth': '18px'}}>
                {
                    this.state.selected ? (
                        <svg role="img" viewBox="0 0 21 20" className="sc-giOsra dnwkyQ">
                            <path fill="#ffffff" d="M1 3h16v16h-16z"/>
                            <path fill="#ff3347" d="M17,10h1V20H17ZM0,2H1V20H0ZM1,19H17v1H1ZM1,2H13V3H1Z"/>
                            <path fill="#ff3347" d="M10.3,16.4,2.9,9.1,5.1,6.9l4.8,4.9L18.2,0l2.4,1.7Z"/>
                        </svg>
                    ) : (
                        <div className="sc-SFOxd bKVnhc sc-ijnzTp ggusLs">
                            <svg role="img" viewBox="0 0 21 20" className="sc-fUdGnz jAnZRj">
                                <path fill="" d="M17 3v16H1V3h16m1-1H0v18h18V2z"/>
                                <path fill="#ffffff" d="M1 3h16v16h-16z"/>
                            </svg>
                        </div>
                    )
                }
            </div>
        )
    }
}

export default Custom;