import React from 'react'
import mStyles from './index.mcss'
import planService from "../../../services/planService";
import {connect} from "react-redux";
import Octicon, {ArrowRight} from '@primer/octicons-react';
import ReactTooltip from "react-tooltip";
import moment from 'moment';

class Custom extends React.Component {
    constructor() {
        super();
        this._confirmHandler = this._confirmHandler.bind(this);
        this._cancelHandler = this._cancelHandler.bind(this);
    }

    _confirmHandler(){
        if(this.props.onConfirmHandler){
            this.props.onConfirmHandler();
        }
    }

    _cancelHandler(){
        if(this.props.onCancelHandler){
            this.props.onCancelHandler();
        }
    }

    render(){
        if(!this.props.fromPlanId || !this.props.toPlanId || !this.props.userId){
            return <span/>
        }
        let fromPlan = planService.getPlanById(this.props.fromPlanId);
        let toPlan = planService.getPlanById(this.props.toPlanId);
        let prorateData = this.props.prorateData;

        return (
            <div>
                <div className={mStyles.confirmation_rows + ' ' + mStyles.switch_title}>
                    <span className={mStyles.arrow_text}>{fromPlan.title}</span> {<Octicon width={20} height={33} icon={ArrowRight}/>} <span className={mStyles.arrow_text}>{toPlan.title}</span>
                </div>
                <div className={mStyles.confirmation_rows} dangerouslySetInnerHTML={getBodyText(fromPlan, toPlan, prorateData)}>

                </div>
                <div className={mStyles.confirmation_rows}>
                    <table>
                        <colgroup>
                            <col width={"135"}/>
                            <col width={"120"}/>
                        </colgroup>
                        <tbody>
                        {
                            !!prorateData && prorateData.cost ? (
                                <tr data-tip={'This is a one time charge to account for the price difference.'}>
                                    <td>Prorated Charge:</td>
                                    <td>${prorateData.cost}</td>
                                </tr>
                            ) : null
                        }
                            <tr>
                                <td>Monthly:</td>
                                <td>${toPlan.price}</td>
                            </tr>
                        </tbody>

                    </table>
                </div>
                <div className={mStyles.confirmation_rows + ' ' + mStyles.button_container}>
                    <button onClick={this._cancelHandler} className={'btn btn-secondary'}>Cancel</button><button onClick={this._confirmHandler} className={'btn btn-primary'}>Confirm</button>
                </div>
                <ReactTooltip multiline={true}/>
            </div>

        )
    }
}

function getBodyText(fromPlan, toPlan, prorate){
    let text = '';
    if(!prorate){
        return text;
    }
    if(prorate.freeTrial){
        text = `By clicking Confirm, you are confirming and agreeing to start the 
                    <b> ${toPlan.title}</b> plan. Since this is your first
                    paid subscription you will get a 30 day free trial! Your free trial will start today. After 30 days your 
                    Credit Card on file will be charged <b> ${toPlan.price}/month</b> starting the next billing cycle 
                    onward, until you cancel.`
    }else if(fromPlan.id === planService.getBasicFreePlanId() && !prorate.freeTrial) {
        text = `By clicking Confirm, you are confirming and agreeing to switch from the
                     <b> ${fromPlan.title}</b> plan to the <b>${toPlan.title}</b> plan. Your subscription will begin today and will
                     be charged <b>$${toPlan.price}/month</b> starting today, until you cancel.`
    } else if(fromPlan.id === planService.getBasicFreePlanId()){
        text = `By clicking Confirm, you are confirming and agreeing to switch from the
                    <b> ${fromPlan.title}</b> plan to the <b>${toPlan.title}</b> plan. Since this is your first
                    upgrade you will get a 30 day free trial! Your free trial will start today. After 30 days your 
                    Credit Card on file will be charged <b> ${toPlan.price}/month</b> starting the next billing cycle 
                    onward, until you cancel.`
    } else if(prorate.cost === 0){
        text = `By clicking Confirm, you are confirming and agreeing to switch from the
                    <b> ${fromPlan.title}</b> plan to the <b>${toPlan.title}</b> plan. Your
                    credit card will be charged <b> ${toPlan.price}/month</b> starting your next billing cycle, ${getDateString(prorate.nextChargeDate * 1000)}, onward, until you cancel.`
    } else {
        text = `By clicking Confirm, you are confirming and agreeing to switch from the
                    <b> ${fromPlan.title}</b> plan to the <b>${toPlan.title}</b> plan. Your
                    credit card will be charged the one-time prorated amount of <b>$${prorate.cost}</b> today and
                    <b> ${toPlan.price}/month</b> starting the next billing cycle onward, until you cancel.`
    }
    return {__html: text};
}

function getDateString(milli){
    return moment(milli).format('MM/DD');
}

/**
 * I may need this. I can either pass the current subsribed plan down, or have it check redux
 * directly.
 * @param state
 * @returns {{}}
 */
const mapStateToProps = state => {
    return {
    }
};

const mapDispatchToProps = dispatch => {
    return {
    }
};

Custom = connect(
    mapStateToProps,
    mapDispatchToProps
)(Custom);

export default Custom