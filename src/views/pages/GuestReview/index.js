import React from 'react'
import { connect } from 'react-redux'

class Custom extends React.Component {
    constructor(props){
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <div style={{textAlign: 'center', marginTop: '10%'}}>
                Guest Reviews
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
    }
};

const mapDispatchToProps = dispatch => {
    return {
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Custom);