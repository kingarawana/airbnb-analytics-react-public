import React, { Component } from 'react';
import { Auth } from 'aws-amplify';

/**
 * One thing to note if you get an error logging in that says "Token is not from a supported provider of this identity pool"
 * you need to go to your Identity pool > Edit Identity Pool > Authentication providers > Facebook and ensure that
 * you have your fb app id populated.
 *
 * If you're using other providers, same process but for their respective tabs.
 *
 * NOTE: There's a problem w/ this. Upon login, it only creates/logins in w/ a Federated identity. Not from UserPool.
 * The problem w/ that is that to access AWS resources, we must have credentials from the UserPool.
 *
 * TODO: Look into how to create user in the UserPool after receiving the Federated identity.
 */
export default class SignInWithFacebook extends Component {
    constructor(props) {
        super(props);
        this.signIn = this.signIn.bind(this);
    }

    componentDidMount() {
        if (!window.FB) this.createScript();
    }

    signIn() {
        const fb = window.FB;
        fb.getLoginStatus(response => {
            console.log('the fb response: ', response);
            if (response.status === 'connected') {
                this.getAWSCredentials(response.authResponse);
            } else {
                fb.login(
                    response => {
                        if (!response || !response.authResponse) {
                            return;
                        }
                        this.getAWSCredentials(response.authResponse);
                    },
                    {
                        // the authorized scopes
                        scope: 'public_profile,email'
                    }
                );
            }
        });
    }

    getAWSCredentials(response) {
        const { accessToken, expiresIn } = response;
        const date = new Date();
        const expires_at = expiresIn * 1000 + date.getTime();
        if (!accessToken) {
            return;
        }

        const fb = window.FB;
        console.log('here ****', accessToken, expires_at);
        fb.api('/me', { fields: 'name,email' }, response => {
            const user = {
                name: response.name,
                email: response.email
            };
            console.log('the login', user, response)
            Auth.federatedSignIn('facebook', { token: accessToken, expires_at }, user)
                .then(credentials => {
                    console.log('my creds', credentials);
                    console.log(credentials);
                }).catch(err => {
                console.log('the error', err)
            });
        });
    }

    createScript() {
        // load the sdk
        window.fbAsyncInit = this.fbAsyncInit;
        const script = document.createElement('script');
        script.src = 'https://connect.facebook.net/en_US/sdk.js';
        script.async = true;
        script.onload = this.initFB;
        document.body.appendChild(script);
    }

    initFB() {
        const fb = window.FB;
        console.log('FB SDK inited');
    }

    fbAsyncInit() {
        // init the fb sdk client
        const fb = window.FB;
        fb.init({
            appId   : '847922565691807',
            cookie  : true,
            xfbml   : true,
            version : 'v2.8'
        });
    }

    render() {
        return (
            <div>
                <button onClick={this.signIn}>Sign in with Facebook</button>
            </div>
        );
    }
}
