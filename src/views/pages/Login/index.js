import {withOAuth, Authenticator, SignIn, SignUp, ConfirmSignUp, ForgotPassword} from "aws-amplify-react";
import React from "react";
import {setCurrentPublicPage} from "../../../actions";
import {withRouter} from "react-router";
import {connect} from "react-redux";
import emitter from "../../../helpers/emitter";
import {PAGE_TYPES} from "../../../classes/Page";
import { Redirect } from 'react-router-dom'
import _ from 'lodash';
import MySignUp from '../../components/SignUp';

class CustomAuthenticator extends Authenticator {
    handleAuthEvent(state, event, showToast){
        setTimeout(()=>emitter.showSpinner(false), 1);
        super.handleAuthEvent(state, event, showToast)
    }
}

class OAuthButton extends React.Component {
    constructor(props) {
        super(props);
        this._clickHandler = this._clickHandler.bind(this);
        this.state = {
            defaultState: props.authState || 'signIn'
            // defaultState: 'signUp'
        }
    }

    _clickHandler(e){
        // console.log('something was clicked', e.target.tagName, e.target, e.target.innerHTML);
        if(e.target.tagName === 'BUTTON' && e.target.innerHTML.trim() !== 'Send Code' && e.target.name !== 'customSignUp'){
            emitter.showSpinner(true);
        }
    }

    _homeClickHandler(){
        this.props.setCurrentPublicPage(PAGE_TYPES.HOME);
    }

    /**
     * <SignIn federated={{facebook_app_id: '847922565691807'}}/>
     */
    render() {
        if(this.props.loggedInUser){
            return <Redirect to={'/admin'}/>
        }
        let theme = myTheme;
        if(this.props.hideBorder){
            theme = _.cloneDeep(myTheme);
            theme.formSection = {
                boxShadow: 'none'
            };
            theme.formContainer = {
                margin: 0
            }
        }
        return (
            <section>
                <div onClick={this._clickHandler}>
                    <CustomAuthenticator theme={theme} defaultCountryCode={7} hideAllDefaults={true}
                                         usernameAttributes={'email'} hideDefault={true}
                                         onStateChange={this.handleAuthStateChange} authState={this.state.defaultState}
                                         hide={[SignUp]}
                    >
                        <SignIn federated={{oauth_config: {label: 'Sign In with Facebook'}}}/>
                        <ForgotPassword/>
                        <MySignUp override={'SignUp'} signUpConfig={{hiddenDefaults: ['phone_number'], signUpFields: signUpFields}}/>
                        {/*<SignUp signUpConfig={{hiddenDefaults: ['phone_number'], signUpFields: signUpFields}}/>*/}
                        <ConfirmSignUp/>
                    </CustomAuthenticator>
                </div>
            </section>
        )
    }
}

/**
 * https://github.com/aws-amplify/amplify-js/blob/master/packages/aws-amplify-react/src/Amplify-UI/Amplify-UI-Theme.tsx
 */
const myTheme = {
    signInButtonIcon: {display: 'none'},
    oAuthSignInButton: {
        backgroundColor: '#4267B2',
        color: 'white'
    },
};

const signUpFields = [
    {
        label: 'Full Name',
        key: 'name',
        required: true,
        displayOrder: 1,
        type: 'string',
        custom: false,
    },
];

const mapStateToProps = state => {
    return {
        loggedInUser: state.loggedInUser,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setCurrentPublicPage: nextCurrentPublicPage => {
            return dispatch(setCurrentPublicPage(nextCurrentPublicPage));
        }
    }
};

OAuthButton = withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(OAuthButton));

export default withOAuth(OAuthButton);