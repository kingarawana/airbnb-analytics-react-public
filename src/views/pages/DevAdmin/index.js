import React from 'react';
import mStyles from './index.mcss'
import {connect} from "react-redux";
import API from '@aws-amplify/api';
import convert from "../../../helpers/convertToLocalApi";
import logger from "../../../helpers/logger";
import emitter from "../../../helpers/emitter";

if(process.env.REACT_APP_ANB_IS_LOCAL || process.env.ENV === 'test'){
    convert(API);
    logger.info('DEV ADMIN Using LOCAL API');
}


class Custom extends React.Component {
    constructor() {
        super();
        this.state = {
            email: ''
        };
        this._clickHandler = this._clickHandler.bind(this);
        this._onChangeHandler = this._onChangeHandler.bind(this);
    }

    async _clickHandler(pageType){
        console.log('delete', this.state.email);
        emitter.showSpinner(true);
        let resp = await API.del('airbnbAnalyticsApi', `/users?email=${this.state.email}`);;
        console.log('the resp', resp);
        emitter.showSpinner(false);
        emitter.showNotification('success', `${this.state.email} deleted.`)
        this.setState({email: ''});


    }

    _onChangeHandler(e){
        this.setState({[e.target.name]: e.target.value})
    }

    render(){
        return (
            <div className={mStyles.body_container}>
                <input className={'form-control'} type={"text"} name={'email'} onChange={this._onChangeHandler} value={this.state.email}/>
                <button className={'btn btn-primary'} onClick={this._clickHandler}>Delete</button>
            </div>
        )
    }
}

const mapStateToPropsAdmin = state => {
    return {
    }
};

const mapDispatchToPropsAdmin = dispatch => {
    return {

    }
};

export default connect(
    mapStateToPropsAdmin,
    mapDispatchToPropsAdmin
)(Custom);