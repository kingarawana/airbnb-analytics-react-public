import React from 'react';
import mStyles from './index.mcss'
import {setCurrentPublicPage, setLoggedInUser} from "../../../actions";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {PAGE_TYPES} from "../../../classes/Page";

class Custom extends React.Component {
    _clickHandler(pageType){
        this.props.setCurrentPublicPage(pageType);
    }

    render(){
        return (
            <div>
                <section className={mStyles.body_container}>
                    <div className={mStyles.text_container}>
                        <h3 className={mStyles.temp_text}>A Home For Hosts</h3>
                        <h4>Find the best tools and services here</h4>
                        <h5 style={{color: 'grey', marginTop: '2rem'}}>Beta</h5>
                    </div>
                    {
                        !this.props.loggedInUser ? (
                            <Link onClick={this._clickHandler.bind(this, PAGE_TYPES.LOGIN)} className={'btn btn-outline-success my-2 my-sm-0'} to={'/admin/'}>Sign In To Begin</Link>
                        ) : null
                    }
                </section>
            </div>
        )
    }
}

const mapStateToPropsAdmin = state => {
    return {
        loggedInUser: state.loggedInUser,
        currentPublicPage: state.currentPublicPage
    }
};

const mapDispatchToPropsAdmin = dispatch => {
    return {
        setLoggedInUser: user => {
            return dispatch(setLoggedInUser(user));
        },
        setCurrentPublicPage: nextCurrentPublicPage => {
            return dispatch(setCurrentPublicPage(nextCurrentPublicPage));
        }
    }
};

export default connect(
    mapStateToPropsAdmin,
    mapDispatchToPropsAdmin
)(Custom);

// export default Custom