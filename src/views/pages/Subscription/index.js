import React from 'react'
import PaymentRegistration from '../../scenes/PaymentRegistration'
import {connect} from "react-redux";
import {setSelectedPlanId} from "../../../actions";
import SubscriptionPlans from "../../components/SubscriptionPlans";
import Modal from "../../components/Modal";
import Login from "../../pages/Login";
import { Redirect } from 'react-router-dom'
import { confirmAlert } from 'react-confirm-alert';
import planService from "../../../services/planService";
const FREE_PLAN_ID = planService.getBasicFreePlanId();


class Custom extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
            signedUpSuccess: false,
            redirectToAdmin: false,
            selectedPlanId: null,
            showPaymentReg: true
        };

        this.paymentSceneRef = React.createRef();
        this._onPaymentSuccessHandler = this._onPaymentSuccessHandler.bind(this);
        this._onPaymentFailureHandler = this._onPaymentFailureHandler.bind(this);
        this._packageSelectedHandler = this._packageSelectedHandler.bind(this);
        this._modalExistHandler = this._modalExistHandler.bind(this);
    }

    async _packageSelectedHandler(selectedPlanId){
        /**
         * I should probably just use state and props instead of redux
         * If the user redirects to admin, they can just re-select. Or if I want, I can pass it as a query param
         * in the redirect so that the admin can automatically pull up the right view.
         */

        // await this.props.setSelectedPlanId(selectedPlanId);
        if(this.props.loggedInUser && this.props.loggedInUser.paymentCustomerId){
            confirmAlert({
                message: 'You already have an acct. Would you like to be redirected to your account page so you can' +
                    ' switch your subscription?',
                buttons: [
                    {
                        label: 'No'
                    },
                    {
                        label: 'Yes',
                        onClick: async () => {
                            this.setState({redirectToAdmin: true});
                        }
                    }
                ]
            });
        }else{
            this.setState({selectedPlanId, showModal: true, showPaymentReg: selectedPlanId !== FREE_PLAN_ID});
        }
    }

    _onPaymentSuccessHandler(){
        console.log('_onPaymentSuccessHandler');
        this.setState({signedUpSuccess: true, showModal: false});
    }

    _onPaymentFailureHandler(){
        console.log('_onPaymentFailureHandler');
        this._showModal(false);
    }

    _modalExistHandler(){
        if(this.state.showPaymentReg && this.paymentSceneRef.current){
            this.paymentSceneRef.current.willDismissHandler();
        }
        this._showModal(false);
    }

    _showModal(show=true){
        this.setState({showModal: show});
    }

    render(){
        if(this.state.redirectToAdmin){
            return <Redirect to={'/admin/account'}/>
        }else if(this.state.signedUpSuccess){
            return <Redirect to={'/admin'}/>
        }else{
            let email = '';
            let name = '';
            if(this.props.loggedInUser){
                email = this.props.loggedInUser.email;
                name = this.props.loggedInUser.name;
            }
            return (
                <div>
                    <SubscriptionPlans onPlanSelected={this._packageSelectedHandler}/>
                        {
                            this.state.showPaymentReg ? (
                                <Modal show={this.state.showModal} onExitHandler={this._modalExistHandler}>
                                    <PaymentRegistration
                                        childRef={this.paymentSceneRef}
                                        selectedPlanId={this.state.selectedPlanId}
                                        onSuccessHandler={this._onPaymentSuccessHandler}
                                        onFailureHandler={this._onPaymentFailureHandler}
                                        defaultValues={{email, name}}
                                    />
                                </Modal>
                            ) : (
                                <Modal style={{width: '500px'}} show={this.state.showModal} onExitHandler={this._modalExistHandler}>
                                    <Login hideBorder={true} authState={'signUp'}/>
                                </Modal>
                            )
                        }

                </div>
            )
        }
    }
}

const mapStateToProps = state => {
    return {
        loggedInUser: state.loggedInUser
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setSelectedPlanId: planId => {
            dispatch(setSelectedPlanId(planId))
        }
    }
};

Custom = connect(
    mapStateToProps,
    mapDispatchToProps
)(Custom);

export default Custom