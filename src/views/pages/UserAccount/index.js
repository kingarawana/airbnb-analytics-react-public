import React from 'react'
import { connect } from 'react-redux'
import mStyles from './index.mcss'
import emitter from "../../../helpers/emitter";
import accountManager from "../../../managers/userAccountManager";
import PlanSwitchScene from '../../scenes/PlanSwitchScene';
import Modal from '../../components/Modal';
import CreditCardManagement from '../../scenes/CreditCardManagement';
import ShowOneCard, {CardTypes} from '../../components/CreditCardIcons/ShowOneCard';
import {fetchPaymentMethods} from '../../../actions'
import planService from "../../../services/planService";

const EDIT_SECTIONS = {
    ACCOUNT: 'accountEdit',
    SUBSCRIPTONS: 'subscriptionEdit',
    PAYMENT: 'paymentEdit'
};

class Custom extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            fullName: undefined,
            accountEdit: false,
            subscriptionEdit: false,
            currentPw: '',
            newPw: '',
            showPlanSwitchModal: false,
            showPaymentSwitchModal: false,
        };
        this._onChangeHandler = this._onChangeHandler.bind(this);
        this._saveAccountInfoHandler = this._saveAccountInfoHandler.bind(this);
        this._changeHandler = this._changeHandler.bind(this);
        this._togglePlanSwitchModal = this._togglePlanSwitchModal.bind(this);
        this._togglePaymentSwitchModal = this._togglePaymentSwitchModal.bind(this);
        this._onPlanSwitchSuccessHandler = this._onPlanSwitchSuccessHandler.bind(this);
        this._onPlanSwitchFailedHandler = this._onPlanSwitchFailedHandler.bind(this);
    }

    static getDerivedStateFromProps(props, state){
        if(props.loggedInUser && state.fullName === undefined){
            return {fullName: props.loggedInUser.name};
        }
        return null
    }

    async _editClickHandler(type){
        switch (type) {
            case EDIT_SECTIONS.SUBSCRIPTONS:
                this._togglePlanSwitchModal();
                break;
            case EDIT_SECTIONS.PAYMENT:
                emitter.showSpinner(true);
                await accountManager.initializeLoggedInUserPaymentData();
                emitter.showSpinner(false);
                this._togglePaymentSwitchModal();
                break;
            default:
                this.setState({[type]: !this.state[type]});
        }
    }

    _cancelHandler(type){
        let state;
        switch (type) {
            case EDIT_SECTIONS.ACCOUNT:
                state = {[type]: !this.state[type]};
                state.fullName = this.props.loggedInUser.name;
                break;
            case EDIT_SECTIONS.SUBSCRIPTONS:

                break;
            default:
                state = {[type]: !this.state[type]};
        }
        this.setState(state);
    }

    _onChangeHandler(e){
        let target = e.target;
        this.setState({fullName: target.value})
    }

    async _saveAccountInfoHandler(){
        var shouldExitEdit = true;

        if(this.state.fullName !== this.props.loggedInUser.name){
            emitter.showSpinner(true);
            try{
                await accountManager.changeUserFullNamePromise(this.props.loggedInUser, this.state.fullName);
                emitter.showNotification('success', 'Name update success!');
            }catch(err){
                console.log('updating name err', err);
                emitter.showNotification('error', 'Name failed');
            }
            emitter.showSpinner(false);
        }
        if(this._hasPasswordChange()){
            if(this._arePwsValid()){
                emitter.showSpinner(true);
                try{
                    await accountManager.changeUserPasswordPromise(this.props.loggedInUser.cognitoUser, this.state.currentPw, this.state.newPw);
                    emitter.showNotification('success', 'Password update success!');
                }catch(msg){
                    shouldExitEdit = false;
                    emitter.showNotification('warning', 'Invalid password');
                }
                emitter.showSpinner(false);
            }else{
                emitter.showNotification('warning', 'Passwords must be at least 8 characters');
            }
        }
        if(shouldExitEdit){
            this._editClickHandler(EDIT_SECTIONS.ACCOUNT)
        }
    }

    _arePwsValid(){
        return (this.state.currentPw.length > 7 || this.state.newPw.length > 7);
    }

    _hasPasswordChange(){
        return (this.state.currentPw || this.state.newPw)
    }

    _changeHandler(e){
        this.setState({[e.target.name]: e.target.value});
    }

    _togglePlanSwitchModal(show=null){
        if(show === null){
            this.setState({showPlanSwitchModal: !this.state.showPlanSwitchModal});
        }else{
            this.setState({showPlanSwitchModal: show});
        }
    }

    _togglePaymentSwitchModal(){
        this.setState({showPaymentSwitchModal: !this.state.showPaymentSwitchModal});
    }

    _onPlanSwitchSuccessHandler(){
        this._togglePlanSwitchModal(false);
    }

    _onPlanSwitchFailedHandler(){
        this._togglePlanSwitchModal(false);
    }

    render() {
        let ae = this.state.accountEdit;
        let se = this.state.subscriptionEdit;
        return (
            <div className={mStyles.container}>
                <section>
                    <div className={mStyles.section_head}>
                        Account Info
                    </div>
                    <div className={mStyles.section_body}>
                        <table>
                            <colgroup>
                                <col width={"120"}/>
                                <col width={"200"}/>
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>Full Name:</td>
                                    <td>{ae ? <input type={'text'} name={'fullName'} className={'form-control'} onChange={this._onChangeHandler} value={this.state.fullName}/> : this.state.fullName}</td>
                                </tr>
                                {
                                    ae ? (
                                            [<tr key={'current_password'}>
                                                <td>Current Password:</td>
                                                <td><input type={'password'} name={'currentPw'} onChange={this._changeHandler} className={'form-control'}/></td>
                                            </tr>,
                                            <tr key={'new_password'}>
                                                <td>New Password:</td>
                                                <td><input type={'password'} name={'newPw'} onChange={this._changeHandler} className={'form-control'}/></td>
                                            </tr>]
                                    ) : (
                                        <tr>
                                            <td>Password:</td>
                                            <td>****</td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>
                        <span className={mStyles.edit_container}>
                            {
                                ae ? (
                                    <span>
                                        <button onClick={this._saveAccountInfoHandler} className={'btn btn-success'}>Save</button>
                                        <button onClick={this._cancelHandler.bind(this, 'accountEdit')} className={'btn btn-secondary'}>Cancel</button>
                                    </span>
                                ) : (
                                    <span className={mStyles.edit} onClick={this._editClickHandler.bind(this, EDIT_SECTIONS.ACCOUNT)}>Edit</span>
                                )
                            }
                        </span>
                    </div>
                </section>
                <section>
                    <div className={mStyles.section_head}>
                        Subscription
                    </div>
                    <div className={mStyles.section_body}>
                        <table>
                            <colgroup>
                                <col width={"120"}/>
                                <col width={"200"}/>
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>Current Plan:</td>
                                    <td>{planService.getPlanTitleById(this.props.currentPlanId)} plan</td>
                                </tr>
                            </tbody>
                        </table>
                        <span className={mStyles.edit_container}>
                            {
                                se ? (
                                    <span>
                                        <button onClick={this._saveAccountInfoHandler} className={'btn btn-success'}>Save</button>
                                        <button onClick={this._cancelHandler.bind(this, 'subscriptionEdit')} className={'btn btn-secondary'}>Cancel</button>
                                    </span>
                                ) : (
                                    <span className={mStyles.edit} onClick={this._editClickHandler.bind(this, EDIT_SECTIONS.SUBSCRIPTONS)}>Change Plans</span>
                                )
                            }
                        </span>
                    </div>
                    <Modal style={{maxWidth: '70%'}} show={this.state.showPlanSwitchModal} onExitHandler={this._togglePlanSwitchModal}>
                        <PlanSwitchScene
                            onPlanSwitchSuccess={this._onPlanSwitchSuccessHandler}
                            onPlanSwitchFailure={this._onPlanSwitchFailedHandler}
                        />
                    </Modal>
                </section>
                <section>
                    <div className={mStyles.section_head}>
                        Payment
                    </div>
                    <div className={mStyles.section_body}>
                        <table>
                            <colgroup>
                                <col width={"200"}/>
                                <col width={"200"}/>
                            </colgroup>
                            <tbody>
                            <tr>
                                <td>Default Payment Method:</td>
                                    {
                                        this.props.currentPaymentMethod ? (
                                            <td><ShowOneCard type={this.props.currentPaymentMethod.cardBrandId}/> ending in <i>{this.props.currentPaymentMethod.last4}</i></td>
                                        ) : (
                                            <td><b>None</b></td>
                                        )
                                    }
                            </tr>
                            </tbody>
                        </table>
                        <span className={mStyles.edit_container}>
                            {
                                se ? (
                                    <span>
                                        <button onClick={this._saveAccountInfoHandler} className={'btn btn-success'}>Save</button>
                                        <button onClick={this._cancelHandler.bind(this, 'subscriptionEdit')} className={'btn btn-secondary'}>Cancel</button>
                                    </span>
                                ) : (
                                    <span className={mStyles.edit} onClick={this._editClickHandler.bind(this, EDIT_SECTIONS.PAYMENT)}>{this.props.currentPaymentMethod ? 'Change' : 'Add'} Default Payment Method</span>
                                )
                            }
                        </span>
                    </div>
                    <Modal style={{maxWidth: '75%'}} show={this.state.showPaymentSwitchModal} onExitHandler={this._togglePaymentSwitchModal}>
                        <CreditCardManagement/>
                    </Modal>
                </section>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        loggedInUser: state.loggedInUser,
        currentPaymentMethod: state.currentPaymentMethod,
        currentPlanId: state.currentPlanId
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchPaymentMethods: userId => {
            dispatch(fetchPaymentMethods(userId));
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Custom);