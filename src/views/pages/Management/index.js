import React from 'react'
import { connect } from 'react-redux'

class Custom extends React.Component {
    constructor(props){
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <div style={{textAlign: 'center', marginTop: '10%'}}>
                Find a manager for your listings
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
    }
};

const mapDispatchToProps = dispatch => {
    return {
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Custom);