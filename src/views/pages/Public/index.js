import React from "react";
import { Switch, Route } from 'react-router-dom'
import PublicNavBar from "../../components/PublicNavBar";
import Subscription from "../Subscription";
import Landing from "../Landing";
import Login from "../Login";
import Showcase from "../Showcase";
import QNA from '../QuestionsAndAnswers';
import GuestReview from '../GuestReview';
import Repair from "../Repair";
import Cleaner from "../Cleaner";
import Management from "../Management";

const Custom = ({match})=>{
    return (
        <div>
            <header>
                <PublicNavBar/>
            </header>
            <Switch>
                <Route exact path={match.url} component={Landing}/>
                <Route path={`/membership`} component={Subscription}/>
                <Route path={`/showcase`} component={Showcase}/>
                <Route path={`/qna`} component={QNA}/>
                <Route path='/guestreviews' component={GuestReview}/>
                <Route path='/cleaners' component={Cleaner}/>
                <Route path='/repair' component={Repair}/>
                <Route path='/management' component={Management}/>
                <Route path='/login' component={Login}/>
            </Switch>
        </div>
    );
};

export default Custom;