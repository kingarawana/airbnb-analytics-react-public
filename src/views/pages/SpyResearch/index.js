import React from 'react'
import { connect } from 'react-redux'
import CreateNewWatchLists from '../../scenes/CreateNewWatchList';
import ViewCompetitors from '../../scenes/ViewCompetitors'
import {Route, Switch} from "react-router-dom";
import emitter from "../../../helpers/emitter";

class Custom extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            currentTab: '1'
        };
    }

    componentDidMount() {
        emitter.showSpinner(true);
        let timer = setInterval(()=>{
            if(this.props.userWatchLists){
                emitter.showSpinner(false);
                clearInterval(timer);
            }

        }, 100);
    }

    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.userWatchLists && nextProps.userWatchLists.length > 0){
            return {currentTab: '2'};
        }

        return null;
    }

    render() {
        let currentTab = String(this.state.currentTab);
        return (
            <div>
                {
                    {
                        '1': <CreateNewWatchLists/>,
                        '2': <ViewCompetitors/>,
                    }[currentTab]
                }
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        userWatchLists: state.userWatchLists
    }
};

const mapDispatchToProps = dispatch => {
    return {
    }
};

Custom = connect(
    mapStateToProps,
    mapDispatchToProps
)(Custom);

export default ({match})=>{
    return (
        <div>
            <Switch>
                {/*
                Note:
                Using `Custom` as the default b/c `Custom` was designed to default to CreateNewWatchLists
                if the user has no lists created yet i.e. First time using the system. If the user has
                watchLists, then it defaults to `ViewCompetitors`, since the user already knows how to
                use the system. A better way of doing this might be to store a user state object in the
                server and initialize the app w/ it before displaying to the user. But I'll leave that
                optimization for another time.
                */}
                <Route exact path={match.url} component={Custom}/>
                <Route path={`${match.url}/create`} component={CreateNewWatchLists}/>
                <Route path={`${match.url}/view`} component={ViewCompetitors}/>
            </Switch>
        </div>
    );
};
