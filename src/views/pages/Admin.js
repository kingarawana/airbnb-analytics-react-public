import React from "react";
import { Switch, Route, Redirect } from 'react-router-dom'
import AdminNavBar from "../components/AdminNavBar";
import SpyPage from "./SpyResearch";
import QandA from "./QuestionsAndAnswers";
import UserAccount from "./UserAccount";
import userAccountManager from '../../managers/userAccountManager';
import subRouteService from "../../services/subRouteService";

const Custom = ({match})=>{
    if(!userAccountManager.isUserLoggedIn()){
        subRouteService.getLastDesiredPath(window.location.pathname);
        return <Redirect to="/login"/>
    }
    let path = subRouteService.getLastDesiredPath();
    if(path){
        return <Redirect to={path}/>
    }
    return (
        <div>
            <header>
                <AdminNavBar/>
            </header>
            <Switch>
                <Route exact path={match.url} component={SpyPage}/>
                <Route path={`${match.url}/spy`} component={SpyPage}/>
                <Route path={`${match.url}/answers`} component={QandA}/>
                <Route path={`${match.url}/account`} component={UserAccount}/>
            </Switch>
        </div>
    );
};

export default Custom;