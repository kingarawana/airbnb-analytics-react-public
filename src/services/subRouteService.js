const TYPES = {
    PLANS: 'SUBSCRIPTION_PLANS',
    LAST_PATH: 'LAST_DESIRED_PATH'
};

export default {
    showPlans: shouldShow=>{
        if(shouldShow){
            setKeyValue(TYPES.PLANS, shouldShow)
        }else{
            return getKeyValue(TYPES.PLANS);
        }
    },
    getLastDesiredPath: (path)=>{
        if(path){
            setKeyValue(TYPES.LAST_PATH, path);
        }else{
            return getKeyValue(TYPES.LAST_PATH);
        }
    }
}

function setKeyValue(key, value){
    localStorage.setItem(key, JSON.stringify(value));
}

function getKeyValue(key){
    let val = localStorage.getItem(key);
    if(val === null){
        return false;
    }else{
        localStorage.removeItem(key);
        return JSON.parse(val);
    }
}