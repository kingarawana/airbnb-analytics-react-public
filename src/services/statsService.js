import stats from 'stats-lite'

const service = {
    calculateRentalsStatsForWatchList: watchList => {
        if(!watchList || !watchList.rentals || !watchList.rentals[0].data) return;
        let bookedData = {};
        let priceData = {};
        let numRentals = watchList.rentals.length;
        watchList.rentals.forEach(rental =>{
            rental.data.calendar.forEach(date => {
                let dateString = date.date;

                if(!bookedData[dateString]){
                    bookedData[dateString] = [];
                    priceData[dateString] = [];
                }

                bookedData[dateString].push(date.available);
                priceData[dateString].push(date.price.local_price);
            });

        });
        return watchList.rentals[0].data.calendar.reduce((prev, {date})=>{
            prev.push({
                percentBooked: Math.round((bookedData[date].filter(b=>!b).length/numRentals)*100),
                mean: Math.round(stats.mean(priceData[date])),
                median: Math.round(stats.median(priceData[date])),
                min: Math.min(...priceData[date]),
                max: Math.max(...priceData[date])
            });
            return prev;
        }, []);
    }
};

export default service;