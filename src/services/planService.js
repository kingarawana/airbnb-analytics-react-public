let free = [
    'Community Forums',
    'Cleaning/Repair Services',
    'Your Own Showroom Showcase',
    'View Guest Reviews',
    'Basic Data Analytics'
];

let basic = [
    ...free,
    'Post Guest Reviews'
];

let premium = [
    ...basic,
    'Unlimited Analytics Data'
];

const plans = [
    {id:'airbnb-analytics-free', title: 'Free', price: '0', interval: 'Billed Never', features: free},
    {id:'plan_GtMD6yZoMpd63J', title: 'Basic', price: '19.99', interval: 'Billed Monthly', features: basic},
    {id:'plan_GtMDu92B8dUuKm', title: 'Pro', price: '99.98', interval: 'Billed Monthly', features: premium},
];

let mappedPlans = plans.reduce((prev, next)=>{
    prev[next.id] = next;
    return prev;
}, {});

export default {
    getPlans: ()=>{
        return plans;
    },
    getPlanById: planId => {
        return mappedPlans[planId];
    },
    getPlanTitleById: planId => {
        return mappedPlans[planId] ? mappedPlans[planId].title : '';
    },
    getBasicFreePlanId: ()=>{
        return plans[0].id;
    }
}