import apiService from "./apiService";
import localStorageService from "./localStorageService";
import statsService from './statsService';

const HOUR = 1000 * 60 * 60;
const MIN_REFRESH_PERIOD = HOUR;
/**
 * The purpose of dataService to abstract away caching of data retrieved from the API. I'm trying to limit the number
 * of calls made to the airbnb API.
 */
const service = {
    fetchWatchLists: async userEmail =>{
        let watchLists = await apiService.fetchWatchLists(userEmail);
        /**
         * Not populating all watchlists since the API calls to get data for each list takes a while
         * and it might trigger an IP block. It also clogs up the UI if it takes a while for fetch
         * lists to return.
         */
        if(watchLists.length){
            await service.populateWatchListWithAppDataIfNeeded(watchLists[0]);
            service.populateWatchListWithStatsIfNeeded(watchLists[0]);
        }
        return watchLists;
    },
    getListingAppData: async listingId => {
        if(shouldRefresh(localStorageService.getListingAppDataTime(listingId))){
            let data = await apiService.getListingAppData(listingId);
            localStorageService.putListingAppData(listingId, data);
            return data;
        }
        return localStorageService.getListingAppData(listingId);
    },
    populateWatchListWithAppDataIfNeeded: async watchlist => {
        if(!watchlist) return;
        for(let rental of watchlist.rentals){
            if(!rental.data){
                rental.data = await service.getListingAppData(rental.id);
            }
        }
        return watchlist;
    },
    populateWatchListWithStatsIfNeeded: async watchlist => {
        /**
         * watchlist.stats will be null/undefined whenever data gets refreshed.
         */
        if(!watchlist.stats && watchlist && watchlist.rentals && watchlist.rentals.length && watchlist.rentals[0].data){
            watchlist.stats = statsService.calculateRentalsStatsForWatchList(watchlist);
        }
        return watchlist;
    },
    rePopulateWatchListWithStats: async watchlist => {
        watchlist.stats = statsService.calculateRentalsStatsForWatchList(watchlist);
        return watchlist;
    }
};

const shouldRefresh = lastStoredTime => {
    return !lastStoredTime || (lastStoredTime && lastStoredTime < Date.now() - MIN_REFRESH_PERIOD);
};

export default service;