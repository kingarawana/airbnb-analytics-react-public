const LISTING_APP_DATA_KEY = 'L_D';
const TIME_KEY = 'TT';

export default {
    getListingAppData: listingId => {
        return JSON.parse(localStorage.getItem(dataKey(LISTING_APP_DATA_KEY, listingId)));
    },
    getListingAppDataTime: listingId => {
        return JSON.parse(localStorage.getItem(timeKey(LISTING_APP_DATA_KEY, listingId)));
    },
    putListingAppData: (listingId, listingData) => {
        localStorage.setItem(dataKey(LISTING_APP_DATA_KEY, listingId), JSON.stringify(listingData));
        localStorage.setItem(timeKey(LISTING_APP_DATA_KEY, listingId), JSON.stringify(Date.now()));
    }
}

function dataKey(prefix, dataKey){
    return `${prefix}:${dataKey}`
}

function timeKey(prefix, dataKey){
    return `${prefix}:${dataKey}:${TIME_KEY}`
}