export const CardTypes = {
    VISA: 0,
    AMERICAN_EXPRESS: 1,
    MASTER_CARD: 2,
    DISCOVER: 3
};

export default {
    getCardTypeFromCardName: function(cardName){
        if(/visa/i.test(cardName)){
            return CardTypes.VISA
        } else if(/master/i.test(cardName)){
            return CardTypes.MASTER_CARD
        } else if(/discover/i.test(cardName)){
            return CardTypes.DISCOVER
        } else if(/express/i.test(cardName)){
            return CardTypes.AMERICAN_EXPRESS
        }
        return null;
    }
}