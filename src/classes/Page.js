export const PAGE_TYPES = {
    // admin page types
    SPY: 'SPY',
    COMMUNITY: 'COMMUNITY',
    ACCOUNT: 'ACCOUNT',

    // public page types
    LOGIN: 'LOGIN',
    ADMIN: 'ADMIN',
    HOME: 'HOME',
    ANALYTICS: 'ANALYTICS',
    SUBSCRIPTION: 'SUBSRITPION',
    SHOWCASE: 'SHOWCASE',
    GUEST_REVIEW: 'GUEST_REVIEW',
    MANAGEMENT: 'MGMT',
    REPAIR: 'REPAIR',
    CLEANER: 'CLEANER',
};

const KEY_TO_PATH_MAPPING = {
    [PAGE_TYPES.ACCOUNT]: ['/admin/account'],
    [PAGE_TYPES.COMMUNITY]: ['/admin/answers'],
    [PAGE_TYPES.SPY]: ['/admin', '/admin/spy'],
};

const removeSlashes = val => val.replace(/\//g, '');

const PATH_TO_KEY_MAPPING = {};

for(let key in KEY_TO_PATH_MAPPING){
    KEY_TO_PATH_MAPPING[key].forEach(path => {
        PATH_TO_KEY_MAPPING[path] = key;
    })
}

const STRIPPED_PATH_TO_KEY_MAPPING = {};

for(let key in KEY_TO_PATH_MAPPING){
    KEY_TO_PATH_MAPPING[key].forEach(path => {
        STRIPPED_PATH_TO_KEY_MAPPING[removeSlashes(path)] = key;
    })
}


export default {
    getPageTypeFromPath: path => {
        let pageType = STRIPPED_PATH_TO_KEY_MAPPING[removeSlashes(path)];
        if(pageType){
            return pageType;
        }else{
            // TODO I can probably do a fuzzy match in the future. I'll see if it actually comes up.
            return undefined;
        }
    },
    getDefaultPathForPageType: pageType => {
        return KEY_TO_PATH_MAPPING[pageType][0];
    },
    getPageObjsForNavBar: ()=>{
        return [
            // TODO: refactor this so that it uses `KEY_TO_PATH_MAPPING` instead of duplicating the path names.
            {name: 'Analytics', path: '/admin', type: PAGE_TYPES.ANALYTICS},
            {name: 'Showcases', path: '/showcase', type: PAGE_TYPES.SHOWCASE},
            {name: 'Questions & Answers', path: '/qna', type: PAGE_TYPES.COMMUNITY},
            {name: 'Guest Reviews', path: '/guestreviews', type: PAGE_TYPES.GUEST_REVIEW},
            {name: 'Cleaning', path: '/cleaners', type: PAGE_TYPES.CLEANER},
            {name: 'Repair', path: '/repair', type: PAGE_TYPES.REPAIR},
            {name: 'Management', path: '/management', type: PAGE_TYPES.MANAGEMENT},
            {name: 'Membership', path: '/membership', type: PAGE_TYPES.SUBSCRIPTION},
        ]
    }
}