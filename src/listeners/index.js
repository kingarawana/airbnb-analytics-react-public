import store from '../store'
import {
  fetchWatchLists, setCurrentPaymentMethod, setSelectedUserWatchList,
  watchListsFetchSuccess, fetchCurrentSubscription, setCurrentPlanId, fetchPaymentMethods
} from "../actions";
import dataService from "../services/dataService";
import PaymentMethod from "../models/PaymentMethod";
import {PAGE_TYPES} from "../classes/Page";

let listeners = [];

export default store => next => action => {
  listeners.forEach(f => {
    f(store.getState(), action);
  });
  next(action);
}

const addListener = f => {
  listeners.push(f);
};

const removeListener = f => {
  listeners = listeners.filter(l => l !== f);
};

function addOneTimeListener(actionType, fn){
  let listener = (state, action)=>{
    if(action.type === actionType){
      fn(state, action);
      removeListener(listener);
    }
  };
  addListener(listener);
}

addListener((state, action) => {
  switch(action.type){
    case 'SET_CURRENT_PAGE':
      if(action.currentPage === PAGE_TYPES.ACCOUNT){
        if(state.loggedInUser){
          store.dispatch(fetchPaymentMethods(state.loggedInUser.userId));
        }else{
          addOneTimeListener('SET_LOGGED_IN_USER', (state, action)=>{
            store.dispatch(fetchPaymentMethods(action.loggedInUser.userId));
          });
        }
      }
      break;
    case 'SET_LOGGED_IN_USER':
      if(action.loggedInUser){
        store.dispatch(fetchWatchLists(action.loggedInUser.email));
        store.dispatch(fetchCurrentSubscription(action.loggedInUser.userId));
        // i don't fetch the users payment methods until they go to the acct page.
      }else{
        store.dispatch(setCurrentPlanId(null));
      }
      break;
    case 'WATCH_LIST_CREATED':
      let wl = state.userWatchLists.map(v => v);
      wl.unshift(action.watchList);
      store.dispatch(watchListsFetchSuccess(wl));
      break;
    case 'SET_USER_WATCH_LISTS':
      if(action.userWatchLists && action.userWatchLists.length){
        store.dispatch(setSelectedUserWatchList(action.userWatchLists[0]));
      }
      break;
    case 'SET_PAYMENT_METHODS':
      let pms = PaymentMethod.sortAndConvertIfNeeded(action.paymentMethods);
      if(pms.length > 0){
        store.dispatch(setCurrentPaymentMethod(pms[0]));
      }
      break;
    case 'WATCH_LIST_DELETED':
      state.userWatchLists = state.userWatchLists.filter(l => l.id !== action.watchListId);
      if(state.selectedUserWatchList && action.watchListId === state.selectedUserWatchList.id){
        if(state.userWatchLists.length){
          store.dispatch(setSelectedUserWatchList(state.userWatchLists[0]));
        }else{
          store.dispatch(setSelectedUserWatchList(null));
        }
      }
      store.dispatch(watchListsFetchSuccess(state.userWatchLists));
      break;
    case 'SET_SELECTED_USER_WATCH_LIST':
      dataService.populateWatchListWithAppDataIfNeeded(action.selectedUserWatchList);
      break;
    case 'WATCH_LIST_UPDATED':
      // let updatedWatchList = action.watchList;
      // // this optimization is probably a bad idea, i should probably just refresh and pull directly from the API again.
      // let watchLists = state.userWatchLists.map(wl => {
      //   if(wl.id === updatedWatchList.id){
      //     wl = updatedWatchList;
      //   }
      //   return wl;
      // });
      // // emitter.showSpinner(true); // bad idea. i should affect the UI from a background service.
      // dataService.populateWatchListWithAppDataIfNeeded(watchList).then(()=>{
      //   store.dispatch(watchListsFetchSuccess(watchList));
      //   // emitter.showSpinner(false);
      // });
      let updatedWatchList = action.watchList;
      // this optimization is probably a bad idea, i should probably just refresh and pull directly from the API again.
      let watchLists = state.userWatchLists.map(wl => {
        if(wl.id === updatedWatchList.id){
          wl = updatedWatchList;
        }
        return wl;
      });
      store.dispatch(watchListsFetchSuccess(watchLists));
      break;
    default:
  }
});
