/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createUserRentalWatchList = /* GraphQL */ `
  mutation CreateUserRentalWatchList(
    $input: CreateUserRentalWatchListInput!
    $condition: ModelUserRentalWatchListConditionInput
  ) {
    createUserRentalWatchList(input: $input, condition: $condition) {
      id
      userEmail
      name
      rentals
      owner
    }
  }
`;
export const updateUserRentalWatchList = /* GraphQL */ `
  mutation UpdateUserRentalWatchList(
    $input: UpdateUserRentalWatchListInput!
    $condition: ModelUserRentalWatchListConditionInput
  ) {
    updateUserRentalWatchList(input: $input, condition: $condition) {
      id
      userEmail
      name
      rentals
      owner
    }
  }
`;
export const deleteUserRentalWatchList = /* GraphQL */ `
  mutation DeleteUserRentalWatchList(
    $input: DeleteUserRentalWatchListInput!
    $condition: ModelUserRentalWatchListConditionInput
  ) {
    deleteUserRentalWatchList(input: $input, condition: $condition) {
      id
      userEmail
      name
      rentals
      owner
    }
  }
`;
