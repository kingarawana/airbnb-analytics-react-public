/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getUserRentalWatchList = /* GraphQL */ `
  query GetUserRentalWatchList($id: ID!) {
    getUserRentalWatchList(id: $id) {
      id
      userEmail
      name
      rentals
      owner
    }
  }
`;
export const listUserRentalWatchLists = /* GraphQL */ `
  query ListUserRentalWatchLists(
    $filter: ModelUserRentalWatchListFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUserRentalWatchLists(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        userEmail
        name
        rentals
        owner
      }
      nextToken
    }
  }
`;
export const watchListByUserEmail = /* GraphQL */ `
  query WatchListByUserEmail(
    $userEmail: String
    $sortDirection: ModelSortDirection
    $filter: ModelUserRentalWatchListFilterInput
    $limit: Int
    $nextToken: String
  ) {
    watchListByUserEmail(
      userEmail: $userEmail
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        userEmail
        name
        rentals
        owner
      }
      nextToken
    }
  }
`;
