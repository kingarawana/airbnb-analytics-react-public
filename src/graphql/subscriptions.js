/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateUserRentalWatchList = /* GraphQL */ `
  subscription OnCreateUserRentalWatchList {
    onCreateUserRentalWatchList {
      id
      userEmail
      name
      rentals
      owner
    }
  }
`;
export const onUpdateUserRentalWatchList = /* GraphQL */ `
  subscription OnUpdateUserRentalWatchList($owner: String!) {
    onUpdateUserRentalWatchList(owner: $owner) {
      id
      userEmail
      name
      rentals
      owner
    }
  }
`;
export const onDeleteUserRentalWatchList = /* GraphQL */ `
  subscription OnDeleteUserRentalWatchList($owner: String!) {
    onDeleteUserRentalWatchList(owner: $owner) {
      id
      userEmail
      name
      rentals
      owner
    }
  }
`;
