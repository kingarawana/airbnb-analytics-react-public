import apiService from "../services/apiService";
import store from "../store";
import User from '../models/User'
import PaymentMethod from "../models/PaymentMethod";
import {setLoggedInUser, setCurrentPaymentMethod, setCurrentPlanId,
    setPaymentMethods} from "../actions";
import Auth from "@aws-amplify/auth";
import planService from "../services/planService";

const manager = {
    isUserLoggedIn: ()=>{
      return JSON.parse(localStorage.getItem('IS_LOGGED_IN'));
    },
    setLoggedInUserFromCognitoData: async cognitoUser => {
        console.log(cognitoUser)
        if(!cognitoUser.userId){
            let resp = await apiService.getUserByCognitoIdPromise(cognitoUser.username);
            let user = resp.data;
            if(!user){
                user = await manager.createNewUserAccount(cognitoUser);
            }
            console.log('the user', user)
            cognitoUser.userId = user._id;
            cognitoUser.paymentCustomerId = user.paymentCustomerId;
        }
        return store.dispatch(setLoggedInUser(cognitoUser));
    },
    initializeLoggedInUserPaymentData: async ()=>{
        if(store.getState().paymentMethods){
            return store.getState().paymentMethods;
        }else{
            let loggedInUser = store.getState().loggedInUser;
            let resp = await apiService.fetchUserPaymentMethods(loggedInUser.userId);
            let pms = PaymentMethod.sortAndConvertIfNeeded(resp.data || []);
            if(pms.length > 0){
                await store.dispatch(setCurrentPaymentMethod(pms[0]));
            }else{
                await store.dispatch(setCurrentPaymentMethod(null));
            }
            await store.dispatch(setPaymentMethods(pms));
            return pms;
        }
    },
    logoutUserPromise: ()=>{
        return store.dispatch(setLoggedInUser(null));
    },
    createNewUserAccount: async cognitoUser => {
        let user = new User(cognitoUser);
        let userData = {
            cognitoId: user.id,
            email: user.email,
            name: user.name,
        };
        let resp = await apiService.createNewUserPromise(userData);
        return resp.data;
    },
    /**
     *
     * @param userId internal id
     * @param paymentId internal PaymentMethod id
     * @param planId plan/subscription id that was created in Stripe.
     * @returns {Promise<void>}
     */
    startSubscriptionForExistingUser: async (planId, paymentId) => {
        /**
         * 1. figure out how to create a user programatically
         *      - will likely need to set a temp password
         * 2. then make call to add payment and start subscription
         */
        let user = store.getState().loggedInUser;
        apiService.createSubscriptionForExistingUser(user.userId, planId, paymentId);
        await store.dispatch(setCurrentPlanId(planId));
    },
    changeSubscriptionForExistingUser: async (newPlanId, prorationTime) => {
        let user = store.getState().loggedInUser;
        await apiService.changeSubscriptionForExistingUser(user.userId, newPlanId, prorationTime);
        await store.dispatch(setCurrentPlanId(newPlanId));
    },
    /**
     * Since a user can only have one subscription at the moment, the fn only takes the user id.
     * @param userId
     * @returns {Promise<void>}
     */
    cancelSubscriptionForUser: async userId => {
        await apiService.cancelSubscriptionForUser(userId);
        await store.dispatch(setCurrentPlanId(planService.getBasicFreePlanId()));
    },
    getCurrentSubscriptionForUser: async userId => {
        let resp = await apiService.getSubscriptionForUser(userId);
        if(resp.httpCode < 300){
            return resp.data
        }else{
            throw Error("Error getting user's subscription")
        }
    },
    doesUserExistPromise: async email => {
        let resp = await apiService.doesUserExistPromise(email);
        return resp.data;
    },
    /**
     *
     * TODO: switch this to get userId internally.
     * @param user instance of {User}
     * @param paymentMethod instance of {PaymentMethod}
     * @return instance of {PaymentMethod} w/ data from API.
     */
    createPaymentMethodForUserPromise: async (userId, paymentMethod, isNewDefault=false) => {
        let resp = await apiService.createPaymentMethodForUser(userId, paymentMethod, isNewDefault);
        console.log(' the respppppp', resp);
        if(resp.httpCode < 300){
            let pm = new PaymentMethod(resp.data);
            let pms = store.getState().paymentMethods;
            if(pms && pms.length > 0){
                pms = pms.map(p => p);
                if(isNewDefault){
                    pms[0].isDefault = false;
                    pm.isDefault = true;
                    pms.unshift(pm);
                    store.dispatch(setCurrentPaymentMethod(pm));
                }else{
                    pms.push(pm);
                }
                store.dispatch(setPaymentMethods(pms));
            }
            return pm;
        }else{
            throw Error(`Error creating payment method`);
        }
    },
    deletePaymentMethodPromise: async (userId, paymentMethodId)=>{
        return apiService.deletePaymentMethodForUser(userId, paymentMethodId);
    },
    /**
     * I'm breaking the pattern here, I will no longer pass userId, since a user is ever
     * only going to be updating it's own data, it makes no sense to pass in user id.
     * @param name
     * @param expireMmYy
     * @returns {Promise<void>}
     */
    updatePaymentMethodInfo: async (paymentMethod, updates) => {
        let userId = store.getState().loggedInUser.userId;
        await apiService.updatePaymentMethodInfoForUser(userId, paymentMethod._id, updates);
        let copy = paymentMethod.copy();
        copy.setChanges(updates);
        let pms = store.getState().paymentMethods.map(p => p._id === copy._id ? copy : p);
        await store.dispatch(setPaymentMethods(pms));
    },
    markAsDefaultPromise: async (userId, paymentMethodId) => {
        await apiService.makePaymentMethodDefault(userId, paymentMethodId);
        let pms = store.getState().paymentMethods.map(p => p);
        pms.forEach(pm => {
            if(pm._id === paymentMethodId){
                pm.lastTimeWasDefault = Date.now();
            }
        });
        await store.dispatch(setPaymentMethods(pms));
    },
    createPaymentMethodAndStartSubscription: async (userId, paymentMethod, planId) => {
        let createdPm = await manager.createPaymentMethodForUserPromise(userId, paymentMethod, true);
        console.log('xxx', 1, createdPm);
        await manager.startSubscriptionForExistingUser(planId, createdPm._id);
        await store.dispatch(setCurrentPaymentMethod(createdPm));
        await store.dispatch(setCurrentPlanId(planId));
    },
    /**
     * This will register a cognito user and create a user in our system
     *
     * @param username
     * @param password
     * @param name
     * @returns {Promise<*>} a cognito user with userId and paymentMethodId set. (Not an instance of User)
     */
    registerNewCognitoUser: async (username, password, name) => {
        // https://github.com/aws-amplify/amplify-js/blob/4644b4322ee260165dd756ca9faeb235445000e3/packages/amazon-cognito-identity-js/index.d.ts#L136-L139
        /**
            interface ISignUpResult {
                user: CognitoUser;
                userConfirmed: boolean;
                userSub: string;
            }
         * @type {ISignUpResult}
         */
        let result = await Auth.signUp({username, password, attributes:{name}});
        let cognitoUser = result.user;
        if(cognitoUser){
            // had to do this conversion b/c apparently signUp and signIn
            // returns two different objects
            console.log('the auth user', cognitoUser, result);
            let realCognitoUser = {
                username: result.userSub,
                attributes: {
                    sub: result.userSub,
                    email_verified: false,
                    name,
                    email: username
                }
            };
            console.log('the realCognitoUser', realCognitoUser);
            let user = await manager.createNewUserAccount(realCognitoUser);
            console.log('created user', user);
            // triggering a sign in
            realCognitoUser = await Auth.signIn(username, password);
            // i don't need to do this manually here because i have a listener for logins.
            // await manager.setLoggedInUserFromCognitoData(realCognitoUser);
            realCognitoUser.userId = user._id;
            console.log('before returning', realCognitoUser);
            return realCognitoUser;
        }
    },
    changeUserPasswordPromise: async (cognitoUser, oldPw, newPw)=>{
        try{
            let resp = await Auth.changePassword(cognitoUser, oldPw, newPw);
        }catch(err){
            throw err.message;
        }
    },
    /**
     * Changes the actual name, not the {username}
     * @param user
     * @param newName
     * @returns {Promise<void>}
     */
    changeUserFullNamePromise: async (user, newName)=>{
        try{
            await Auth.updateUserAttributes(user.cognitoUser, {name: newName});
            let resp = await apiService.updateInternalUser(user.userId, {name: newName});
            if(resp.httpStatus > 300){
                throw 'Error updating user';
            }
            // NOTE: need to call this to refresh the cached user.
            await Auth.currentAuthenticatedUser({ bypassCache: true });
            user.setName(newName);
            let updatedUser = new User(user.cognitoUser); // can't reuse obj in redux, otherwise it wont re-render
            return store.dispatch(setLoggedInUser(updatedUser));
        }catch(err){
            throw err;
        }
    }
};

export default manager;