const PORT = 3001;

export default {
    port: PORT,
    baseUrl: `http://localhost:${PORT}`,
    payment: {
        publishableKey: process.env.REACT_APP_ANB_STRIPE_PUBLISHABLE_KEY
    },
    logger: {
        logLevel: 'info' // comma separated list, debug,info,warn,error
    },
}