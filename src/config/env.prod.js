const PORT = 3001;

export default {
    payment: {
        publishableKey: process.env.ANB_STRIPE_PUBLISHABLE_KEY
    },
    logger: {
        logLevel: 'error' // comma separated list, debug,info,warn,error
    },
}