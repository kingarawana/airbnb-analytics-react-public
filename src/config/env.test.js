const PORT = 2999;

// this is only used during tests
export default {
    port: PORT,
    baseUrl: `http://localhost:${PORT}`,
    payment: {
        publishableKey: process.env.ANB_STRIPE_PUBLISHABLE_KEY
    },
    logger: {
        logLevel: 'error' // comma separated list, debug,info,warn,error
    },
};