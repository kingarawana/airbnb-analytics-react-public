const env = process.env.ENV || 'dev';
console.log('The ENV', env);
const config = require(`./env.${env}`).default;
module.exports = config;