const { execSync } = require('child_process');
const fs = require('fs');
const AUTH_PARAMS_FILE = 'amplify/backend/auth/airbnbanalytics7a52f096/parameters.json';
const readline = require('readline');

let env;
if(process.argv.length > 2){
    env = process.argv[2];
}else{
    throw Error('Please pass in which enviornment [dev|prod]');
}

let env_file = './.env';
if(env === 'prod'){
    env_file += '/production'
}

async function run(){
    let env_params = await getEnvVariablesPromise();
    let authParams = JSON.parse(fs.readFileSync(AUTH_PARAMS_FILE));
    let authMetaData = JSON.parse(authParams.oAuthMetadata);
    let authUrl = env_params['REACT_APP_REDIRECT_URL'];
    let priorAuthUrl = authMetaData.CallbackURLs;
    authMetaData.CallbackURLs = authUrl;
    authMetaData.LogoutURLs = authUrl;
    authParams.oAuthMetadata = JSON.stringify(authMetaData);
    fs.writeFileSync(AUTH_PARAMS_FILE, JSON.stringify(authParams, null, 4));
    execSync('amplify push -y', {stdio: 'inherit'});
    authMetaData.CallbackURLs = priorAuthUrl;
    authMetaData.LogoutURLs = priorAuthUrl;
    authParams.oAuthMetadata = JSON.stringify(authMetaData);
    fs.writeFileSync(AUTH_PARAMS_FILE, JSON.stringify(authParams, null, 4));
};

function getEnvVariablesPromise(){
    return new Promise(fulfill => {
        const readInterface = readline.createInterface({
            input: fs.createReadStream(env_file),
            console: false
        });
        let env_params = {};
        readInterface.on('line', line => {
            let parts = line.split('=');
            parts = parts.map(p => p.trim());
            env_params[parts[0]] = parts[1];
        });
        readInterface.on('close', ()=>{
            fulfill(env_params);
        })
    })
}

run();